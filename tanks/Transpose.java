import java.awt.*;
import java.awt.image.*;
import java.applet.*;
import java.lang.*;
public class Transpose extends Applet
{
       Graphics    sG;
       Graphics    bG;
       Image       bI;
       Image       matrices;
       Color       blue    = new Color(0, 0, 255);
       int         x = 0;
       int         y = 0;
       public void init()
       {
              bI = createImage(this.size().width, this.size().height);
              bG = bI.getGraphics();
              matrices = getImage(getCodeBase(), "transpose.gif");
              bG.drawImage(matrices, 0, 0, this);
              drawscreen();
       }  
       public void paint(Graphics g)
       {
              drawscreen();
              g.drawImage(bI, 0, 0, this);
              sG = getGraphics();
       }
       public void drawscreen()
       {
              bG.drawImage(matrices, 0, 0, this);
              check(x, y, 58, 6, 58, 6);
              check(x, y, 58, 28, 97, 6);
              check(x, y, 58, 78, 172, 6);
              check(x, y, 97, 6, 58, 28);
              check(x, y, 97, 28, 97, 28);
              check(x, y, 97, 78, 172, 28);
              check(x, y, 172, 6, 58, 78);
              check(x, y, 172, 28, 97, 78);
              check(x, y, 172, 78, 172, 78);
       }
       public void box(int ix, int iy)
       {
              bG.setColor(blue);
              bG.drawLine(ix, iy, ix + 30, iy);
              bG.drawLine(ix, iy + 20, ix + 30, iy + 20);
              bG.drawLine(ix, iy, ix, iy + 20);
              bG.drawLine(ix + 30, iy, ix + 30, iy + 20);
       }
       public boolean mouseDown(Event evt, int mx, int my)
       {
             x = mx;
             y = my;
             drawscreen();
             sG.drawImage(bI, 0, 0, this);
             return true;
       }
       public void check(int x, int y, int tx, int ty, int px, int py)
       {
            if ((x >= tx) && (x < tx + 31) && (y >= ty) && (y < ty + 21))
            {
                box(tx, ty);
                box(px + 228, py + 1);
            }
            if ((x >= px + 228) && (x <= px + 259) && (y >= py) && (y < py + 21))
            {
                box(tx, ty);
                box(px + 228, py + 1);
            }
       }
}
