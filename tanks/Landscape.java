import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
public class Landscape
{
	private int start;
	private int change;
	private int faktor;
	private int last;
	private int plus;
	private final int mapsize = 700;
	Random rnd = new Random ();
	public int [] map;
	public Color [] colors;
	Landscape ()
	{
		faktor = 1;
		map = new int [mapsize];
		colors = new Color [mapsize];
		generateLandscape ();
	}
	public void generateLandscape ()
	{
		plus = 1;
		start = Math.abs(300 + (rnd.nextInt() % 50));
		map [0] = start;
		int greenvalue = 200;
		int redvalue = Math.abs(rnd.nextInt() % 200);
		int bluevalue = Math.abs(rnd.nextInt() % 201);
		colors [0] = new Color (redvalue, greenvalue, bluevalue);
		for (int i = 1; i < mapsize; i ++)
		{
			last = map [i - 1];
			change = Math.abs(rnd.nextInt() % 10);
			if (change > 8)
			{
				faktor = - (faktor);
				plus = 1 + Math.abs(rnd.nextInt() % 2);
			}
			if (last > 350 || last < 120)
			{
				faktor = - (faktor);
			}
			if (greenvalue > 240)
			{
				greenvalue -= 10;
			}
			else if (greenvalue < 100)
			{
				greenvalue += 10;
			}
			map [i] = last + (faktor * plus);
			greenvalue = greenvalue + (-faktor * plus);
			colors [i] = new Color (redvalue, greenvalue, bluevalue);
		}
	}
	public void paintMap (Graphics g)
	{
		for (int index = 0; index < mapsize; index ++)
		{
			g.setColor (colors [index]);
			g.drawLine (index, map[index], index, 400);
		}
	}
}