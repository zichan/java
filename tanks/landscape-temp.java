
/**
 * Write a description of class Gravtest here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
public class Gravtest extends Frame
{
    Mycanvas can;
    public Gravtest()
    {
       can = new Mycanvas();
       setSize(300,300);
       setLayout(new BorderLayout());
       add(can,BorderLayout.CENTER);
       can.startAnim();
    }
    public static void main(String[] args)
    {
        Gravtest f = new Gravtest();
        f.setVisible(true);
    }
    
}
class Mycanvas extends Canvas implements Runnable
{
    double angle=150; //(this is angle of cannon relative to the sky), presumably it ranges between 0 and pi;
    double velocity=60; //(this is speed shell leaves cannon in some random units) ranges from 50 to 100;
    double deltaTime = .01; // you need a time scale.
    landscape ls = new landscape();
	private Image dbImage;
	private Graphics dbg;
    double xV = velocity*Math.cos(angle); // simple trig. We initialize it to the start velocity.
    double yV = velocity*Math.cos(angle); // this will start changing once time starts happening.
    double gravity = 20; // how rapidly velocity in the y directions diminsihes. 
    //(max height will be 100+80+60+40+20 = 300 units - changing gravity changes this.)
 
    double xShell = 50; 
    double yShell = 50 + 0; // the cannon is off the ground by about the height of the tank 
    Thread thr;
    boolean impact = false;
    private int start;			// Gibt die Starth�he der Landschaft vor
	private int change;			// Entscheidet, ob die Richtung der Landschaftsentwicklung ge�ndert wird
	private int faktor;			// Entscheidet �ber Addition oder Substraktion
	private int last;			// Speichert die letzte H�he der letzten gezeichneten Linie
	private int plus;			// Wieviel wird addiert oder subtraiert

	private final int mapsize = 700;	// Konstante f�r die Gr��e des Arrays

	// Variable zur Erzeugung einer Zufallszahl
	Random rnd = new Random ();

	// Array zum Speichern der H�henpunkte
	public int []map;

	// Array zum Speichern der Farbwerte der jeweiligen H�henpunkte
	public Color [] colors;
    public Mycanvas()
    {
        super();
        map = new int [mapsize];
		colors = new Color [mapsize];
		generateLandscape();
    }
    public void run()
    {
        while(!impact)
        {
            xShell += xV*deltaTime; // you move a litte in the x direction
            yShell += yV*deltaTime; // you move a little in the y direction
            yV += gravity*deltaTime; // gravity alters the y Velocity slightly.
            impact = testForImpact(xShell, yShell);
            repaint();
            try 
            { 
                thr.sleep(20);
            }
            catch (InterruptedException e){}
        }
    }
    public void startAnim()
    {
        thr = new Thread(this);
        thr.start();
    }
    public void update (Graphics g)
	{
		// Initialisierung des DoubleBuffers
		if (dbImage == null)
		{
			dbImage = createImage (this.getSize().width, this.getSize().height);
			dbg = dbImage.getGraphics ();
		}

		// Bildschirm im Hintergrund l�schen
		dbg.setColor (getBackground ());
		dbg.fillRect (0, 0, this.getSize().width, this.getSize().height);

		// Auf gel�schten Hintergrund Vordergrund zeichnen
		dbg.setColor (Color.black);
		paint (dbg);

		// Nun fertig gezeichnetes Bild Offscreen auf dem richtigen Bildschirm anzeigen
		g.drawImage (dbImage, 0, 0, this);
	}
    public void paint(Graphics g)
    {
        g.drawOval((int)xShell,(int)yShell,5,5);
        ls.paintMap(g);
    }
    public boolean testForImpact(double x,double y)
    {
        if(x>this.getSize().width)
        {
            if(y>this.getSize().height)
            {
                return false;
            }else return false;
        }else return false;
    }
    public void generateLandscape ()
	{
		// Initialisierung von plus
		plus = 1;

		// Initialisierung des Startwertes
		start = Math.abs(300 + (rnd.nextInt() % 50));

		// Speichern des Startwertes an der ersten Stelle des Arrays
		map [0] = start;

		// Initialisierung der Startfarbwerte
		int greenvalue = 200;
		int redvalue = Math.abs(rnd.nextInt() % 200);
		int bluevalue = Math.abs(rnd.nextInt() % 201);

		// Speichern des ersten Startwertes f�r das Farbenarray
		colors [0] = new Color (redvalue, greenvalue, bluevalue);

		// Loop zur Initialisierung aller anderen Arrayfelder
		for (int i = 1; i < mapsize; i ++)
		{
			// Speichern der letzten Arrayposition f�r H�he und Farbe
			last = map [i - 1];

			// Entscheidet, ob die eingeschlagene Richtung gewechselt wird
			change = Math.abs(rnd.nextInt() % 10);

			// Wenn change > 7 ist, dann �ndert sich die Richtung und m�glicherweise plus
			if (change > 8)
			{
				// �ndern der Richtung
				faktor = - (faktor);

				// Wieviel wird addiert bzw. substrahiert
				plus = 1 + Math.abs(rnd.nextInt() % 2);
			}

			// Wird ein bestimmter Wert unter- bzw. �berschritten, dann wird die Richtung ge�ndert
			if (last > 350 || last < 120)
			{
				// �ndern der Richtung
				faktor = - (faktor);
			}

			// H�lt die Farbwerte immer in einem bestimmten Rahmen
			if (greenvalue > 240)
			{
				// Wenn Farbwert zu gro� wird, erniedrigen des Wertes
				greenvalue -= 10;
			}
			else if (greenvalue < 100)
			{
				// Wenn Farbwert zu klein wird erh�hen des Farbwertes
				greenvalue += 10;
			}

			// Werte f�r das Feld an i - Stelle werden berechnet
			map [i] = last + (faktor * plus);

			/** Um die Farbewerte f�r zunehmende H�he heller werden zu lassen, wird der Faktor
			umgekehrt. Dies ist wegen dem umgekehrten Koordinatensystem von Java n�tig */
			greenvalue = greenvalue + (-faktor * plus);
			colors [i] = new Color (redvalue, greenvalue, bluevalue);
		}
	}
}
public class landscape
{
	private int start;
	private int change;
	private int faktor;
	private int last;
	private int plus;
	private final int mapsize = 700;
	Random rnd = new Random ();
	public int [] map;
	public Color [] colors;
	landscape ()
	{
		faktor = 1;
		map = new int [mapsize];
		colors = new Color [mapsize];
		generateLandscape ();
	}
	public void generateLandscape ()
	{
		plus = 1;
		start = Math.abs(300 + (rnd.nextInt() % 50));
		map [0] = start;
		int greenvalue = 200;
		int redvalue = Math.abs(rnd.nextInt() % 200);
		int bluevalue = Math.abs(rnd.nextInt() % 201);
		colors [0] = new Color (redvalue, greenvalue, bluevalue);
		for (int i = 1; i < mapsize; i ++)
		{
			last = map [i - 1];
			change = Math.abs(rnd.nextInt() % 10);
			if (change > 8)
			{
				faktor = - (faktor);
				plus = 1 + Math.abs(rnd.nextInt() % 2);
			}
			if (last > 350 || last < 120)
			{
				faktor = - (faktor);
			}
			if (greenvalue > 240)
			{
				greenvalue -= 10;
			}
			else if (greenvalue < 100)
			{
				greenvalue += 10;
			}
			map [i] = last + (faktor * plus);
			greenvalue = greenvalue + (-faktor * plus);
			colors [i] = new Color (redvalue, greenvalue, bluevalue);
		}
	}
	public void paintMap (Graphics g)
	{
		for (int index = 0; index < mapsize; index ++)
		{
			g.setColor (colors [index]);
			g.drawLine (index, map[index], index, 400);
		}
	}
}