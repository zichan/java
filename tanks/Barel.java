import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import java.net.*;
import javax.imageio.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
 public class Barel extends Frame
{
    MyCan can = new MyCan();
    public Barel()
    {
        setLayout(new BorderLayout());
        Button repaint = new Button("Repaint");
        repaint.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent a)
            {
                can.repaint();
            }
        });
        add(can,BorderLayout.CENTER);
        add(repaint,BorderLayout.SOUTH);
    }
    public static void main(String[] args) throws IOException 
    {
        Barel f = new Barel();
        f.setVisible(true);
    }
}
class MyCan extends Canvas
{
    int Angle=0;
    public MyCan()
    {
        super();
    }
    public BufferedImage tilt(double angle) 
    {
        BufferedImage image = Circle();
        double sin = Math.abs(Math.sin(angle)), cos = Math.abs(Math.cos(angle));
        int w = image.getWidth(), h = image.getHeight();
        int neww = (int)Math.floor(w*cos+h*sin), newh = (int)Math.floor(h*cos+w*sin);
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage result = gc.createCompatibleImage(neww, newh, Transparency.TRANSLUCENT);
        Graphics2D g = result.createGraphics();
        g.rotate(angle, 20, 20);
        g.drawRenderedImage(image, null);
        g.dispose();
        return result;
    }
    public BufferedImage Circle()
    {
        BufferedImage i = new BufferedImage(40,40,BufferedImage.TYPE_INT_ARGB);
        Graphics g = i.getGraphics();
        g.setColor(Color.blue);
        g.fillRect(20,18,20,4);
        return i;
    }
    public static GraphicsConfiguration getDefaultConfiguration() 
    {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }
    public void paint(Graphics g)
    {
        g.drawImage(tilt(Math.toRadians(Angle+10)),50,50,this);
        Angle+=10;
    }
}