package tankds;

/**
 * A BuddyBar is not a panel.  It creates the label, textfield, and scrollbar but doesn't add
 * them to any panel.  The point is that you can layout your controls any way you want.  Typically,
 * though, you want them left to right as label, textfield, and scrollbar.  This panel lays out
 * the contents of a buddybar just that way.  Just make your buddybar and, when it comes time
 * to add it to your panel, do this instead:
 * this.add(new BuddyBarPanel(bbar));
 * so the three pieces will look like a single panel component.
 */

import java.awt.*;
import java.awt.event.*;

public class BuddyBarPanel extends Panel
{
	BuddyBar mybar;

	public BuddyBarPanel(BuddyBar theBar)
	{
		mybar = theBar;

		// Use a layout called GridBagLayout.  It is the most powerful of the layout managers.
		// We will tell it to keep the label and textfield their default size
		// but expand the scrollbar as much as possible.
		this.setLayout(new GridBagLayout());
		// The constraints object tells the layout manager where to put the next component added.
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(3,3,3,3);
		c.fill = GridBagConstraints.NONE;
		c.gridx = 0; c.gridy = 0; c.gridwidth=1; c.gridheight=1;
		c.weightx = 0; c.weighty = 0;

		// The label goes in gridx=0 with no desire to fill horizontal or vertical.
		this.add(mybar.getLabel(),c);

		// The textfield goes in gridx=1 with no fill.
		c.gridx = 1;
		this.add(mybar.getTextField(),c);

		// Put the scrollbar in gridx=2 with horizontal fill and some horizontal weight.
		c.gridx = 2; c.weightx = 1.0;
		c.fill = GridBagConstraints.HORIZONTAL;
		this.add(mybar.getScrollbar(),c);
	}
}
