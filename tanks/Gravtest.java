import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
public class Gravtest extends Frame
{
    Mycanvas can;
    public Gravtest()
    {
       can = new Mycanvas();
       setSize(700,400);
       setLayout(new BorderLayout());
       add(can,BorderLayout.CENTER);
       can.startAnim();
    }
    public static void main(String[] args)
    {
        Gravtest f = new Gravtest();
        f.setVisible(true);
    }
}
class Mycanvas extends Canvas implements Runnable
{
    double angle=60;
    double rads =57.29577866f;
    double velocity=3;
    double deltaTime = .098;
    landscape ls = new landscape();
    private Image dbImage;
    private Graphics dbg;
    double xV;
    double yV;
    double gravity = .098;
    double xShell; 
    double yShell;
    Thread thr;
    JSlider Angle;
    JSlider Speed;
    boolean impact = false;
    boolean gameDone = false;
    boolean hitwall = false;
    public boolean efexcomp=true;
    int tankx,tanky;
    public Mycanvas()
    {
        super();
        angle /= rads;
    }
    double th;
    public void FirstPlayerShot()
    {
        xV = Speed.getValue()/10*Math.cos(Math.toRadians(Angle.getValue()));
        yV = Speed.getValue()/10*Math.sin(Math.toRadians(Angle.getValue()));
        while(!impact)
        {
            xShell += xV;
            yShell -= yV;
            yV -= gravity/2; // gravity alters the y Velocity slightly.
            impact = testForImpact(xShell, yShell);
            repaint();
            try 
            {
                thr.sleep(20);
            }
            catch (InterruptedException e){}
        }
        explosionAndLand();
    }
    public void explosionAndLand()
    {
        efexcomp=false;
        repaint();
        ls.Efexcomp=true;
        int height;
        efe=false;
        int radius=30;
        int w=0;
        while(!efexcomp&&hitwall==false)
        {
            for(int i=(int)xShell-radius+1;i<=(int)xShell+radius-1;i++)
            {
                if(i<xShell)
                {
                    if((int)Math.pow(radius,2)>(int)Math.pow(xShell-i,2))
                        th = Math.sqrt((int)Math.pow(radius,2)-(int)Math.pow(xShell-i,2));
                    if((int)Math.pow(radius,2)<Math.pow(xShell-i,2))
                        th = Math.sqrt((int)Math.pow(xShell-i,2)-(int)Math.pow(radius,2));
                }
                if(i>xShell)
                {
                    if((int)Math.pow(radius,2)>Math.pow(i-xShell,2))
                        th = Math.sqrt((int)Math.pow(radius,2)-(int)Math.pow(i-xShell,2));
                    if((int)Math.pow(radius,2)<Math.pow(i-xShell,2))
                        th = Math.sqrt((int)Math.pow(i-xShell,2)-Math.pow(radius,2));
                }
                if(i==xShell)
                    th=radius;
                if(ls.map[i]<yShell+th&&ls.map[i]>yShell-th)
                {
                    if((int)(yShell+th)>400)
                        ls.map[i]=400;
                    else
                        ls.map[i]=(int)(yShell+th);
                }
                if(ls.map[i]<yShell-th)
                {
                    ls.left[w][2]=i;
                    ls.left[w][1]=(int)(yShell-th);
                    ls.left[w][0]=ls.map[i];
                    if((int)(yShell+th)>400)
                    ls.map[i]=400;
                    else
                        ls.map[i]=(int)(yShell+th);
                    //ls.map[i]=(int)(ls.map[i]+(th*2));
                    w++;
                }
            }
            efexcomp=true;
            efe=true;
            repaint();
            ls.indexs=w;

        }
    }
    public void run()
    {
        Frame f = new Frame();
        f.setLayout(new GridLayout(5,2));
        f.add(new Label("Angle"));
        Angle = new JSlider(0,360,45);
        Angle.setMajorTickSpacing( 40 );
        Angle.setMinorTickSpacing( 10 );
        Angle.setPaintTicks(true);
        Angle.setPaintLabels(true);
        f.add(Angle);
        f.add(new Label("Paower"));
        Speed = new JSlider(0,100,20);
        Speed.setMajorTickSpacing( 40 );
        Speed.setMinorTickSpacing( 10 );
        Speed.setPaintTicks(true);
        Speed.setPaintLabels(true);
        f.add(Speed);
        Button shoot = new Button("Shoot");
        shoot.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent r)
            {
                FirstPlayerShot();
            }
        });
        f.setVisible(true);
        ls.tnk.TankImage();
        xShell=50;
        yShell=ls.map[50]-10;
        while(!gameDone)
        {
            xV = (Speed.getValue()/10)*Math.cos(Math.toRadians(Angle.getValue()));
            yV = (Speed.getValue()/10)*Math.sin(Math.toRadians(Angle.getValue()));
            while(!impact)
            {
                xShell += xV;
                yShell -= yV;
                yV -= gravity/2; // gravity alters the y Velocity slightly.
                impact = testForImpact(xShell, yShell);
                repaint();
                try 
                {
                    thr.sleep(20);
                }
                catch (InterruptedException e){}
            }
            efexcomp=false;
            repaint();
            ls.Efexcomp=true;
            int height;
            efe=false;
            int radius=30;
            int w=0;
            while(!efexcomp&&hitwall==false)
            {
                for(int i=(int)xShell-radius+1;i<=(int)xShell+radius-1;i++)
                {
                    if(i<xShell)
                    {
                        if((int)Math.pow(radius,2)>(int)Math.pow(xShell-i,2))
                            th = Math.sqrt((int)Math.pow(radius,2)-(int)Math.pow(xShell-i,2));
                        if((int)Math.pow(radius,2)<Math.pow(xShell-i,2))
                            th = Math.sqrt((int)Math.pow(xShell-i,2)-(int)Math.pow(radius,2));
                    }
                    if(i>xShell)
                    {
                        if((int)Math.pow(radius,2)>Math.pow(i-xShell,2))
                            th = Math.sqrt((int)Math.pow(radius,2)-(int)Math.pow(i-xShell,2));
                        if((int)Math.pow(radius,2)<Math.pow(i-xShell,2))
                            th = Math.sqrt((int)Math.pow(i-xShell,2)-Math.pow(radius,2));
                    }
                    if(i==xShell)
                        th=radius;
                    if(ls.map[i]<yShell+th&&ls.map[i]>yShell-th)
                    {
                        if((int)(yShell+th)>400)
                            ls.map[i]=400;
                        else
                            ls.map[i]=(int)(yShell+th);
                    }
                    if(ls.map[i]<yShell-th)
                    {
                        ls.left[w][2]=i;
                        ls.left[w][1]=(int)(yShell-th);
                        ls.left[w][0]=ls.map[i];
                        if((int)(yShell+th)>400)
                            ls.map[i]=400;
                        else
                            ls.map[i]=(int)(yShell+th);
                        //ls.map[i]=(int)(ls.map[i]+(th*2));
                        w++;
                    }
                }
                efexcomp=true;
                efe=true;
                repaint();
                ls.indexs=w;
            }
            if(w>0)
            {
                if(ls.th==null)
                    ls.dooo();
            }
            efexcomp=true;
            impact = false;
            hitwall=false;
            xShell = 50; 
            yShell = ls.map[50]-20;
            xV = velocity*Math.cos(angle);
            yV = velocity*Math.sin(angle);
        }
    }
    public void startAnim()
    {
        thr = new Thread(this);
        thr.start();
    }
    public void update (Graphics g)
    {
        if (dbImage == null)
        {
            dbImage = createImage (800, 800);
            dbg = dbImage.getGraphics ();
        }
        dbg.setColor (getBackground ());
        dbg.fillRect (0, 0, this.getSize().width, this.getSize().height);
        paint(dbg);
        dbg.setColor (Color.blue);
        dbg.drawImage(ls.tnk.i,40,ls.map[40]-12,this);
        g.drawImage (dbImage, 0, 0, this);
    }
    boolean efe=true;
    public void paint(Graphics g)
    {
        g.setColor(Color.black);
        g.drawOval((int)xShell,(int)yShell,5,5);
        ls.paintMap(g);int x=26/2;
        int r=50;
        int t=60;
        double bx,by;
        int angle=45;
        int barrel=6;
        by=(Math.sin(angle)*barrel)+r;
        bx=(Math.tan(angle)*barrel)+t;
        if(efe==false)
        {
            g.fillOval((int)xShell-10,(int)yShell-10,20,20);
        }
    }
    public boolean testForImpact(double x,double y)
    {
        if(x>699||y>400)
        {
            if(x>=700)
                hitwall=true;
            return true;
        }
        if(y>=ls.map[(int)x])
        {
            return true;
        }
        if(x<=0)
        {
            hitwall=true;
            return true;
        }
        else return false;
    }
}
class Tank
{
    int lastx,lasty;
    BufferedImage i = new BufferedImage(26,12,BufferedImage.TYPE_INT_ARGB);
    Graphics gr = i.getGraphics();
    int x=26/2;
    int y=6;
    double bx,by;
    int angle=45;
    int barrel=6;
    public Tank()
    {
        super();
        by=(Math.sin(angle)*barrel)+y;
        bx=(Math.tan(angle)*barrel)+x;
    }
    public void TankImage()
    {
        gr.setColor(Color.blue);
        gr.fillRoundRect(8,0,10,6,2,2);
        gr.fillRoundRect(0,6,26,6,2,2);
    }
}
class landscape implements Runnable
{
    private int start;
    private int change;
    private int faktor;
    private int last;
    private int plus;
    boolean landToMove;
    Thread th;
    private final int mapsize = 700;
    Integer [] map;
    int indexs;
    public int[][] left= new int[100][3];
    public Color [] colors;
    boolean done=true;
    boolean Efexcomp=false;
    Graphics g;
    Tank tnk = new Tank();
    Random rnd = new Random ();
    landscape()
    {
        faktor = 1;
        map = new Integer [mapsize];
        colors = new Color [mapsize];
        generateLandscape();
    }
    public void dooo()
    {   
        th= new Thread(this);
        th.start();
    }
    public void stop()
    {
        th=null;
    }
    public void run()
    {
        done=false;
        boolean thisLandDone[] = new boolean[70];
        for(int i=0;i<=indexs;i++)
        {
            thisLandDone[i]=false;
        }
        while(!done)
        {
            done=true;
            for(int i=0;i<=indexs;i++)
            {
                if(left[i][1]==map[left[i][2]])
                {
                    map[left[i][2]]=left[i][0];
                    thisLandDone[i]=true;
                }
                if(left[i][1]<map[left[i][2]])
                {
                    left[i][0]++;
                    left[i][1]++;
                    done=false;
                }
            }
            new Mycanvas().repaint();
            try
            {
                th.sleep(10);
            }
            catch(Exception t){}
        }
        left= new int[41][3];
    }
    public void genHillLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<(mapsize/2);i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==6)
            {
                if(deg+2<10)
                    deg+=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
            }
            else
            {
                if(deg+2<10)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genSlopeLand()
    {
        map[0]=Math.abs(100 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<250;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=250;i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
            }
            else
            {
                if(deg+2<8)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genbumpyLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==4||rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                if(map[i-1]>map[0]+10)
                    deg-=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0]-10)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/2);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                map[i]=map[i-1]+(deg/5);
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
                map[i]=map[i-1]+(deg/2);
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genVallyLand()
    {
        map[0]=Math.abs(100 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<(mapsize/2);i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
            }
            else
            {
                if(deg+2<8)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                map[i]=map[i-1]+(deg/5);
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
                map[i]=map[i-1]+(deg/2);
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genFlatLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int up;
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        for(int i=1;i<mapsize;i++)
        {
            up=(250+rnd.nextInt(150));
            if(up>map[i-1])
            {
                map[i]=map[i-1]+(1/(1+rnd.nextInt(6)));
            }
            if(up<=map[i-1])
            {
                map[i]=map[i-1]-(1/(1+rnd.nextInt(6)));
            }
            if (greenvalue > 240)
            {
                greenvalue -= 10;
            }
            else if (greenvalue < 100)
            {
                greenvalue += 10;
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void generateLandscape ()
    {
        plus = 1;
        start = Math.abs(300 + (rnd.nextInt() % 50));
        map [0] = start;
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        for (int i = 1; i < mapsize; i ++)
        {
            last = map [i - 1];
            change = Math.abs(rnd.nextInt() % 10);
            if (change > 8)
            {
                faktor = - (faktor);
                plus = 1/(1 + Math.abs(rnd.nextInt() % 2));
            }
            if (last > 350 || last < 120)
            {
                faktor = - (faktor);
            }
            if (greenvalue > 240)
            {
                greenvalue -= 10;
            }
            else if (greenvalue < 100)
            {
                greenvalue += 10;
            }
            map [i] = last + (faktor * plus);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void paintMap (Graphics g)
    {
        for (int index = 0; index < mapsize; index ++)
        {
            g.setColor (colors [index]);
            g.drawLine (index, map[index], index, 400);
        }
        if(!done)
        {
            for(int i=0;i<=indexs;i++)
            {
                g.setColor(colors[left[i][2]]);
                g.drawLine(left[i][2],left[i][0],left[i][2],left[i][1]);
            }
        }
    }
}