package fds;
import java.awt.*;
import java.awt.event.*;
import java.util.Random;
import java.awt.image.*;
import javax.swing.event.*;
import javax.swing.*;
import javax.swing.event.*;
import java.util.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JWindow;

public class AnimJava extends Frame implements ActionListener
{
    Button start, stop;
    Panel controls;
    xCanvas can = new xCanvas();
    public static void main(String[] args)
    {
        AnimJava f = new AnimJava();
        f.setVisible(true);
    }
    public AnimJava()
    {
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){
                             System.exit(0);}});
                             setBounds(60, 60, 400, 400);
        start = new Button("Start");
        start.addActionListener(this);
        stop = new Button("Stop");
        stop.addActionListener(this);
        controls = new Panel();
        can.setBackground(Color.black);
        controls.setLayout(new GridLayout(1,2));
        setLayout ( new BorderLayout());
        controls.add(start);
        controls.add(stop);
        add(controls, BorderLayout.NORTH);
        add(can,BorderLayout.CENTER);
    }
    public void actionPerformed (ActionEvent e)
    {   
        if (e.getSource() == start)
        {
            can.startAnim();
        }
        if (e.getSource() == stop)
        {
            can.stopAnim();
        }
    }
}
class xCanvas extends Canvas implements Runnable
{
	private Image dbImage;
	private Graphics dbg;
    Time tim = new Time();
    Thread disp=null;
    int x[][] = new int[6][21];
    int y[][] = new int[6][21];
    int stx[][] = new int[6][21];
    int sty[][] = new int[6][21];
    int X,Y;
    int[][][][] num1 = {
    {{{40},{10}},{{30},{10}},{{20},{10}},{{10},{10}},{{10},{20}},{{10},{30}},{{10},{40}},{{10},{50}},{{10},{60}},{{10},{70}},{{20},{70}},{{30},{70}},{{40},{70}},{{40},{60}},{{40},{50}},{{40},{40}},{{40},{30}},{{40},{20}},{{40},{20}},{{40},{20}}},
    {{{40},{10}},{{40},{20}},{{40},{30}},{{40},{40}},{{40},{50}},{{40},{60}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}}},
    {{{10},{10}},{{20},{10}},{{30},{10}},{{40},{10}},{{40},{20}},{{40},{30}},{{40},{40}},{{30},{40}},{{20},{40}},{{10},{40}},{{10},{50}},{{10},{60}},{{10},{70}},{{20},{70}},{{30},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}}},
    {{{10},{10}},{{20},{10}},{{30},{10}},{{40},{10}},{{40},{20}},{{40},{30}},{{40},{40}},{{30},{40}},{{20},{40}},{{10},{40}},{{40},{50}},{{40},{60}},{{40},{70}},{{30},{70}},{{20},{70}},{{10},{70}},{{10},{70}},{{10},{70}},{{10},{70}},{{10},{70}}},
    {{{10},{10}},{{10},{20}},{{10},{30}},{{10},{40}},{{20},{40}},{{30},{40}},{{40},{40}},{{40},{30}},{{40},{20}},{{40},{10}},{{40},{50}},{{40},{60}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}}},
    {{{40},{10}},{{30},{10}},{{20},{10}},{{10},{10}},{{10},{20}},{{10},{30}},{{10},{40}},{{20},{40}},{{30},{40}},{{40},{40}},{{40},{50}},{{40},{60}},{{40},{70}},{{30},{70}},{{20},{70}},{{10},{70}},{{10},{70}},{{10},{70}},{{10},{70}},{{10},{70}}},
    {{{40},{10}},{{30},{10}},{{20},{10}},{{10},{10}},{{10},{20}},{{10},{30}},{{10},{40}},{{10},{50}},{{10},{60}},{{10},{70}},{{20},{70}},{{30},{70}},{{40},{70}},{{40},{60}},{{40},{50}},{{40},{40}},{{30},{40}},{{20},{40}},{{20},{40}},{{20},{40}}},
    {{{10},{10}},{{20},{10}},{{30},{10}},{{40},{10}},{{40},{20}},{{40},{30}},{{40},{40}},{{40},{50}},{{40},{60}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}}},
    {{{40},{10}},{{30},{10}},{{20},{10}},{{10},{10}},{{10},{20}},{{10},{30}},{{10},{40}},{{20},{40}},{{30},{40}},{{40},{40}},{{40},{50}},{{40},{60}},{{40},{70}},{{30},{70}},{{20},{70}},{{10},{70}},{{10},{60}},{{10},{50}},{{40},{30}},{{40},{20}}},
    {{{10},{10}},{{20},{10}},{{30},{10}},{{40},{10}},{{40},{20}},{{40},{30}},{{40},{40}},{{30},{40}},{{20},{40}},{{10},{40}},{{10},{30}},{{10},{20}},{{40},{50}},{{40},{60}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}},{{40},{70}}}};
    public xCanvas()
    {
        super();
    }
    public void startAnim()
    {
        if (disp == null)
        {
            disp = new Thread(this);
            tim.start();
            disp.start();
        }
    }
    public void stopAnim()
    { 
        if (disp != null)
        {
            disp = null;
            tim.time=null;
        }
    }
    public void frew()
    {
        for(int a=0;a<=5;a++)
        {
            for(int b=0;b<=19;b++)
            {
                stx[0][b]=num1[tim.number[0]][b][0][0];
                sty[0][b]=num1[tim.number[0]][b][1][0];
                stx[1][b]=num1[tim.number[1]][b][0][0]+50;
                sty[1][b]=num1[tim.number[1]][b][1][0];
                stx[2][b]=num1[tim.number[2]][b][0][0]+150;
                sty[2][b]=num1[tim.number[2]][b][1][0];
                stx[3][b]=num1[tim.number[3]][b][0][0]+200;
                sty[3][b]=num1[tim.number[3]][b][1][0];
                stx[4][b]=num1[tim.number[4]][b][0][0]+300;
                sty[4][b]=num1[tim.number[4]][b][1][0];
                stx[5][b]=num1[tim.number[5]][b][0][0]+350;
                sty[5][b]=num1[tim.number[5]][b][1][0];
                if (x[a][b]<stx[a][b])
                {
                    X=stx[a][b]/x[a][b];
                    x[a][b]=x[a][b]+X;
                }
                if(x[a][b]>stx[a][b])
                {
                    X=x[a][b]/stx[a][b];
                    x[a][b]=x[a][b]-X;
                }
                if (y[a][b]<sty[a][b])
                {
                    Y=sty[a][b]/y[a][b];
                    y[a][b]=y[a][b]+Y;
                }
                if(y[a][b]>sty[a][b])
                {
                    Y=y[a][b]/sty[a][b];
                    y[a][b]=y[a][b]-Y;
                }
            }
        }
    }
    public void run()
    {
            for(int b=0;b<=19;b++)
            {
                x[0][b]= num1[tim.number[0]][b][0][0];
                x[1][b]=num1[tim.number[1]][b][0][0];
                x[2][b]=num1[tim.number[2]][b][0][0];
                x[3][b]=num1[tim.number[3]][b][0][0];
                x[4][b]=num1[tim.number[4]][b][0][0];
                x[5][b]=num1[tim.number[5]][b][0][0];
                y[0][b]=num1[tim.number[0]][b][1][0];
                y[1][b]=num1[tim.number[1]][b][1][0];
                y[2][b]=num1[tim.number[2]][b][1][0];
                y[3][b]=num1[tim.number[3]][b][1][0];
                y[4][b]=num1[tim.number[4]][b][1][0];
                y[5][b]=num1[tim.number[5]][b][1][0];
            }
        while(disp!=null)
        {
            frew();
            repaint();
            try 
            { 
                disp.sleep(15);
            }
            catch (InterruptedException e){}
        }
    }
    public void paint(Graphics g)
    {
        Graphics2D g2 = (Graphics2D)g;
        g2.setColor(Color.green);
        for (int a=0;a<=5;a++)
        {
            for(int b=0;b<=19;b++)
            {
                g2.fillOval(x[a][b],y[a][b],10,10);
            }
        }
    }
    public void update (Graphics g)
	{
		// Initialisierung des DoubleBuffers
		if (dbImage == null)
		{
			dbImage = createImage (600, 600);
			dbg = dbImage.getGraphics ();
		}

		// Bildschirm im Hintergrund l�schen
		dbg.setColor (getBackground ());
		dbg.fillRect (0, 0, this.getSize().width, this.getSize().height);

		// Auf gel�schten Hintergrund Vordergrund zeichnen
		dbg.setColor (Color.black);
		paint (dbg);

		// Nun fertig gezeichnetes Bild Offscreen auf dem richtigen Bildschirm anzeigen
		g.drawImage (dbImage, 0, 0, this);
	}
}
class Time implements Runnable
{
    Thread time = null;
    int number[] = new int[6];
    public Time()
    {
        super();
    }
    public void seperate(int dig,int a)
    {
        if(a>=0&&a<10)
        {
            number[dig]=0;
            number[dig+1]=a;
        }
        if(a==10)
        {
            number[dig]=1;
            number[dig+1]=0;
        }
        if(a>10&&a<20)
        {
            number[dig]=1;
            number[dig+1]=a-10;
        }
        if(a==20)
        {
            number[dig]=2;
            number[dig+1]=0;
        }
        if(a>20&&a<30)
        {
            number[dig]=2;
            number[dig+1]=a-20;
        }
        if(a==30)
        {
            number[dig]=3;
            number[dig+1]=0;
        }
        if(a>30&&a<40)
        {
            number[dig]=3;
            number[dig+1]=a-30;
        }
        if(a==40)
        {
            number[dig]=4;
            number[dig+1]=0;
        }
        if(a>40&&a<50)
        {
            number[dig]=4;
            number[dig+1]=a-40;
        }
        if(a==50)
        {
            number[dig]=5;
            number[dig+1]=0;
        }
        if(a>50&&a<60)
        {
            number[dig]=5;
            number[dig+1]=a-50;
        }
        if(a==60)
        {
            number[dig]=0;
            number[dig+1]=0;
        }
    }
    public void start()
    {
        if(time==null)
        {
            time = new Thread(this);
            time.start();
        }
    }
    public void run()
    {
        while(true)
        {
            Calendar now = Calendar.getInstance();
            seperate(0,now.get(Calendar.HOUR));
            seperate(2,now.get(Calendar.MINUTE));
            seperate(4,now.get(Calendar.SECOND));
            try
            {
                time.sleep(100);
            }
            catch(Exception e){}
        }
    }
}