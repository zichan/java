import java.awt.*;
import java.applet.*;
import java.awt.image.*;
import java.util.Random;
//a flicker free animation with images!
//the GIF snail with transparent color moves over the JPEG grass
public class Project11 extends Applet implements Runnable
{
     Thread runner;
     BufferedImage bi;
     Graphics2D big;
     

    Random t = new Random();
    int x[]= new int[12];
    int y[] = new int[12];
    int X,Y;
    int stx[]= new int[12];
    int sty[] = new int[12];
    int[][][] num1 = {{{20},{20}},{{30},{10}},{{30},{20}},{{30},{30}},{{30},{40}},{{30},{50}},{{30},{60}},{{30},{70}},{{10},{70}},{{20},{70}},{{40},{70}}};
     public void init()
     {
         start();
     }
     public void start()
     {
         if (runner == null)
         {
             runner = new Thread (this);
             runner.start();
         }
     }
     public void stop()
     {
         if (runner != null)
         {
             runner.stop();
             runner = null;
         }
     }
     public void run()
     {
        for(int i=1;i<=11;i++)
        {
            stx[i]=num1[i-1][0][0];
            sty[i]=num1[i-1][1][0];
            x[i] = t.nextInt(400); //hard coding opening coordinates of rectangle
            y[i] = t.nextInt(400);
        }
         while(true)
         {
            for(int i=1;i<=11;i++)
            {
                if (x[i]<stx[i])
                {
                    X=stx[i]/x[i];
                    x[i]=x[i]+X;
                }
                if(x[i]>stx[i])
                {
                    X=x[i]/stx[i];
                    x[i]=x[i]-X;
                }
                if (y[i]<sty[i])
                {
                    Y=sty[i]/y[i];
                    y[i]=y[i]+Y;
                }
                if(y[i]>sty[i])
                {
                    Y=y[i]/sty[i];
                    y[i]=y[i]-Y;
                }
            }
             repaint();
             try 
            { 
                runner.sleep(10);
            }
            catch (InterruptedException e){}
         }
     }
     //is needed to avoid erasing the background by Java
     public void updat(Graphics g)
     {
         for(int i=1;i<=11;i++)
         {
            g.fillOval(x[i],y[i],20,20);
        }
     }
     public void paint(Graphics g)
     {
         g.setColor(Color.red);
         updat(g);
     }
}