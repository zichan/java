import java.awt.*;


import java.applet.*;


import java.lang.*;


//


// The BallAnim applet uses XOR mode to draw a rectangle


// and a moving ball. It implements the Runnable interface


// because it is performing animation.


public class BallAnim extends Applet implements Runnable


{


   Thread animThread;


   int ballX = 0;     // X coordinate of ball


   int ballDirection = 0;   // 0 if going left-to-right, 1 otherwise


// Start is called when the applet first cranks up. It creates a thread for


// doing animation and starts up the thread.


   public void start()


   {


     if (animThread == null)


     {


        animThread = new Thread(this);


        animThread.start();


     }


   }


// Stop is called when the applet is terminated. It halts the animation


// thread and gets rid of it.


   public void stop()


   {


     animThread.stop();


     animThread = null;


   }


// The run method is the main loop of the applet. It moves the ball, then


// sleeps for 1/10th of a second and then moves the ball again.


   public void run()


   {


     Thread.currentThread().setPriority(Thread.NORM_PRIORITY);


     while (true)


     {


        moveBall();


        try {


          Thread.sleep(100);   // sleep 0.1 seconds


        } catch (Exception sleepProblem) {


// This applet ignores any exceptions if it has a problem sleeping.


// Maybe it should take Sominex


        }


     }


   }


   private void moveBall()


   {


// If moving the ball left-to-right, add 1 to the x coord


     if (ballDirection == 0)


     {


        ballX++;


// Make the ball head back the other way once the x coord hits 100


        if (ballX > 100)


        {


          ballDirection = 1;


          ballX = 100;


        }


     }


     else


     {


// If moving the ball right-to-left, subtract 1 from the x coord


        ballX--;


// Make the ball head back the other way once the x coord hits 0


        if (ballX <= 0)


        {


          ballDirection = 0;


          ballX = 0;


        }


     }


     repaint();


   }


   public void paint(Graphics g)


   {


     g.setXORMode(getBackground());


     g.fillRect(40, 10, 40, 40);


     g.fillOval(ballX, 0, 30, 30);


   }


}
