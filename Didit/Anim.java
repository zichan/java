import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.util.Random;
public class Anim extends Applet implements ActionListener
{
    Button start, stop;
    canv can;
    Panel controls;
    public void init()
    {
        start = new Button("Start");
        start.addActionListener(this);
        stop = new Button("Stop");
        stop.addActionListener(this);
        controls = new Panel();
        controls.setLayout ( new GridLayout(1,2));
        can = new canv();
        can.setBackground(Color.black);
        this.setLayout ( new BorderLayout());
        controls.add(start);
        controls.add(stop);
        add(controls, BorderLayout.NORTH);
        add(can, BorderLayout.CENTER);
    }
    public void actionPerformed (ActionEvent e)
    {   
        if (e.getSource() == start)
            can.startAnim();
        if (e.getSource() == stop)
            can.stopAnim();
    }
}
class canv extends Canvas implements Runnable
{
    Random t = new Random();
    Thread runner = null;
    int w = 20; 
    int incr = 4;
    int x1,y1,x,y;
    int X,X1,Y,Y1;
    int stx,stx1,sty,sty1;
    int[][][] num1 = {{{20},{20}},{{30},{10}},{{30},{20}},{{30},{30}},{{30},{40}},{{30},{50}},{{30},{60}},{{30},{70}},{{10},{70}},{{20},{70}},{{40},{70}}};
    public canv()
    {
        super();
    }
    public void startAnim()
    {
        if (runner == null)
        {
            runner = new Thread(this);
            runner.start();
        }
    }
    public void stopAnim()
    { 
        if (runner != null)
        {
            runner = null;
        }
    }
    public int getx(int e)
    {
        return num1[e][0][0];
    }
    public int gety(int e)
    {
        return num1[e][1][0];
    }
    public void run()
    {
            stx=num1[0][0][0];
            sty=num1[1][0][0];
            x = t.nextInt(400); //hard coding opening coordinates of rectangle
            y = t.nextInt(400);
            stx1=num1[0][1][0];
            sty1=num1[1][1][0];
            x1 = t.nextInt(400); //hard coding opening coordinates of rectangle
            y1 = t.nextInt(400);
            while(x!=stx||y!=sty||x1!=stx1||y1!=sty1)
            {
                if (x1<stx1)
                {
                    X1=stx1/x1;
                    x1=x1+X1;
                }
                if(x1>stx1)
                {
                    X1=x1/stx1;
                    x1=x1-X1;
                }
                if (y1<sty1)
                {
                    Y1=sty1/y1;
                    y1=y1+Y1;
                }
                if(y1>sty1)
                {
                    Y1=y1/sty1;
                    y1=y1-Y1;
                }
                if (x<stx)
                {
                    X=stx/x;
                    x=x+X;
                }
                if(x>stx)
                {
                    X=x/stx;
                    x=x-X;
                }
                if (y<sty)
                {
                    Y=sty/y;
                    y=y+Y;
                }
                if(y>sty)
                {
                    Y=y/sty;
                    y=y-Y;
                }
                repaint();
                try 
                { 
                    runner.sleep(10);
                }
                catch (InterruptedException e){}
            }
    }
    public void paint(Graphics g)
    {   
        g.setColor(Color.red);
        g.fillOval(x,y,20,20);
        g.fillOval(x1,y1,20,20);
    }
}