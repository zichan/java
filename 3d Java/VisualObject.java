public class VisualObject extends Shape3D{
private Geometry voGeometry;
private Appearance voAppearance;
// create Shape3D with geometry and appearance
// the geometry is created in method createGeometry
// the appearance is created in method createAppearance
public VisualObject() {
voGeometry = createGeometry();
voAppearance = createAppearance();
this.setGeometry(voGeometry);
this.setAppearance(voAppearance);
}
private Geometry createGeometry() {
// code to create default geometry of visual object
}
private Appearance createAppearance () {
// code to create default appearance of visual object
}
} // end of class VisualObject