package iuyt;
import java.awt.event.*;
import java.awt.AWTEvent;
import javax.media.j3d.*;
import java.util.Enumeration;
import javax.vecmath.*;
// Mover behavior class - used to allow viewer to move using arrow keys
class MoverBehavior extends Behavior
{
    WakeupOnAWTEvent w1 = new WakeupOnAWTEvent(KeyEvent.KEY_PRESSED);
    WakeupCriterion[] w2 = {w1};
    WakeupCondition w = new WakeupOr(w2);
    TransformGroup viewTransformGroup;
    double rotation = 0.0;		// holds current rotation radians
    
    public void initialize() {
	// Establish initial wakeup criteria
	wakeupOn(w);
    }
    /**
     *  Override Behavior's stimulus method to handle the event.
     */
    public void processStimulus(Enumeration criteria) {
	WakeupOnAWTEvent ev;
	WakeupCriterion genericEvt;
	AWTEvent[] events;
	while (criteria.hasMoreElements()) {
	    genericEvt = (WakeupCriterion) criteria.nextElement();
	    if (genericEvt instanceof WakeupOnAWTEvent) {
		ev = (WakeupOnAWTEvent) genericEvt;
		events = ev.getAWTEvent();
		processManualEvent(events);
	    }
	}
	// Set wakeup criteria for next time
	wakeupOn(w);
    }
    /**
     *  Process a keyboard event to move or rotate the viewer.
     */
    void processManualEvent(AWTEvent[] events) {
	for (int i = 0; i < events.length; ++i) {
	    if (events[i] instanceof KeyEvent) {
		KeyEvent event = (KeyEvent)events[i];
		if (event.getKeyCode() == KeyEvent.VK_EQUALS) {
		    continue;
		}
		Transform3D t = new Transform3D();
		viewTransformGroup.getTransform(t);
		Vector3f viewDir = new Vector3f(0f, 0f, -1f);
		Vector3f translation = new Vector3f();
		t.get(translation);
		t.transform(viewDir);
		if (event.getKeyCode() == KeyEvent.VK_UP) {
		    translation.x += viewDir.x;
		    translation.y += viewDir.y;
		    translation.z += viewDir.z;
		}
		else if (event.getKeyCode() == KeyEvent.VK_DOWN) {
		    translation.x -= viewDir.x;
		    translation.y -= viewDir.y;
		    translation.z -= viewDir.z;
		}
		else if (event.getKeyCode() == KeyEvent.VK_RIGHT) {
		    rotation += -.1;
		}
		else if (event.getKeyCode() == KeyEvent.VK_LEFT) {
		    rotation += .1;
		}
		t.rotY(rotation);
		t.setTranslation(translation);
		viewTransformGroup.setTransform(t);
	    }
	}
    }
    /**
     *  Constructor 
     */
    public MoverBehavior(TransformGroup trans) {
	viewTransformGroup = trans;
	Bounds bound = new BoundingSphere(new Point3d(0.0,0.0,0.0),10000.0);
	this.setSchedulingBounds(bound);
    }
}