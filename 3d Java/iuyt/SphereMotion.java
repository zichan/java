package iuyt;
import com.sun.j3d.utils.geometry.Sphere;
import com.sun.j3d.utils.universe.*;
import javax.media.j3d.*;
import javax.vecmath.*;
import java.awt.GraphicsConfiguration;
import java.awt.*;
import com.sun.j3d.utils.behaviors.mouse.*;
import com.sun.j3d.utils.behaviors.vp.*;
import com.sun.j3d.utils.image.TextureLoader;
public class SphereMotion extends javax.swing.JFrame
{
    private SimpleUniverse univ = null;
    private BranchGroup scene = null;
    // Constants for type of light to use
    private static final int DIRECTIONAL_LIGHT = 0;
    private static final int POINT_LIGHT = 1;
    private static final int SPOT_LIGHT = 2;
    // Flag indicates type of lights: directional, point, or spot
    // lights.  This flag is set based on command line argument
    private static int lightType = POINT_LIGHT;
    boolean absolute = false;
    public BranchGroup createSceneGraph() 
    {
        Color3f eColor    = new Color3f(0.0f, 0.0f, 0.0f);
        Color3f sColor    = new Color3f(1.0f, 1.0f, 1.0f);
        Color3f objColor  = new Color3f(Color.gray);
        Color3f lColor1   = new Color3f(Color.yellow);
        Color3f lColor2   = new Color3f(Color.white);
        Color3f lColor3   = new Color3f(Color.red);
        Color3f alColor   = new Color3f(0.2f, 0.2f, 0.2f);
        Color3f bgColor   = new Color3f(0.05f, 0.05f, 0.2f);
        Color3f SunColor   = new Color3f(Color.yellow);
        Color3f EarthColorL  = new Color3f(new Color(60,71,241));
        Color3f EarthColorD  = new Color3f(new Color(10,16,139));
        Color3f MarsColor = new Color3f(new Color(213,22,13));
        Color3f MoonColor    = new Color3f(Color.white);
        Transform3D t;
        // Create the root of the branch graph
        BranchGroup objRoot = new BranchGroup();
        // Create a Transformgroup to scale all objects so they
        // appear in the scene.
        TransformGroup Solar = new TransformGroup();
        if(absolute == true)
        {
            Solar.addChild(makePlanet(.057,MarsColor,MarsColor,87660,.0019f));
            Solar.addChild(makePlanet(.1085,MarsColor,MarsColor,226455,.0047f));
            Solar.addChild(makePlanet(.1475,EarthColorL,EarthColorD,365000,.005f,true,.016,MoonColor,MoonColor,29500,.0013f));
            Solar.addChild(makePlanet(.226,MarsColor,MarsColor,686200,.0026f));
            Solar.addChild(makePlanet(.789,MarsColor,MarsColor,4383000,.055f));
            Solar.addChild(makePlanet(1.43,MarsColor,MarsColor,10767500,.046f));
            Solar.addChild(makePlanet(2.85,MarsColor,MarsColor,30660000,.02f));
            Solar.addChild(makePlanet(4.5,MarsColor,MarsColor,60225000,.019f));
            Solar.addChild(makePlanet(26,MarsColor,MarsColor,90520000,.9f));
        }
        if(absolute == false)
        {
            Solar.addChild(makePlanet(.057,MarsColor,MarsColor,87660,.0019f));
            Solar.addChild(makePlanet(.1085,MarsColor,MarsColor,226455,.0047f));
            Solar.addChild(makePlanet(.1475,EarthColorL,EarthColorD,365000,.005f,true,.016,MoonColor,MoonColor,29500,.0013f));
            Solar.addChild(makePlanet(.226,MarsColor,MarsColor,686200,.0026f));
            Solar.addChild(makePlanet(.789,MarsColor,MarsColor,4383000,.055f));
            Solar.addChild(makePlanet(1,MarsColor,MarsColor,10767500,.046f));
            Solar.addChild(makePlanet(1.2,MarsColor,MarsColor,30660000,.02f));
            Solar.addChild(makePlanet(1.4,MarsColor,MarsColor,60225000,.019f));
            Solar.addChild(makePlanet(1.6,MarsColor,MarsColor,90520000,.0009f));
        }
        objRoot.addChild(Solar);
        // Create a bounds for the background and lights
        BoundingSphere bounds = new BoundingSphere(new Point3d(0,0,0), 100.0);
        // Set up the background
        Background bg = new Background(bgColor);
        bg.setApplicationBounds(bounds);
        Solar.addChild(bg);
        // Create a Sphere object, generate one copy of the sphere,
        // and add it into the scene graph.
        ColoringAttributes Sun = new ColoringAttributes();
        Sun.setColor(SunColor);
        Appearance sun = new Appearance();
        sun.setColoringAttributes(Sun);
        Solar.addChild( new Sphere(.05f, Sphere.GENERATE_NORMALS,180,sun));
        Light SunLight = null;
        Point3f lPoint  = new Point3f(0.0f, 0.0f, 0.0f);
        Point3f atten = new Point3f(1.0f, 0.0f, 0.0f);
        SunLight = new PointLight(SunColor, lPoint, atten);
        // Set the influencing bounds
        SunLight.setInfluencingBounds(bounds);
        Solar.addChild(SunLight);
        objRoot.compile();
        return objRoot;
    }
    public TransformGroup makePlanet(double distance,Color3f lightedColor,Color3f darkColor,int speed,float diameter,boolean rotateAroundPlanet,double Distance,Color3f satColorLight, Color3f satColorDark,int Speed,float Diameter)
    {
        Transform3D t = new Transform3D();
        t.setTranslation(new Vector3d(2*distance,0,0));
        TransformGroup Satalite = new TransformGroup(t);
        TransformGroup satalite = new TransformGroup();
        satalite.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        satalite.addChild(Satalite);
        t.setTranslation(new Vector3d(2*Distance,0,0));
        TransformGroup Moon = new TransformGroup(t);
        TransformGroup moon = new TransformGroup();
        moon.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        moon.addChild(Moon);
        Satalite.addChild(moon);
        Moon.addChild(createNewPlanet(Diameter,satColorLight,satColorDark));
        Satalite.addChild(objRotate(Speed,moon));
        Satalite.addChild(createNewPlanet(diameter,lightedColor,darkColor));
        satalite.addChild(objRotate(speed,satalite));
        return satalite;
    }
    public TransformGroup makePlanet(double distance,Color3f lightedColor,Color3f darkColor,int speed,float diameter)
    {
        Transform3D t = new Transform3D();
        t.setTranslation(new Vector3d(2*distance,0,0));
        TransformGroup Satalite = new TransformGroup(t);
        TransformGroup satalite = new TransformGroup();
        satalite.setCapability(TransformGroup.ALLOW_TRANSFORM_WRITE);
        satalite.addChild(Satalite);
        Satalite.addChild(createNewPlanet(diameter,lightedColor,darkColor));
        satalite.addChild(objRotate(speed,satalite));
        return satalite;
    }
    public Sphere createNewPlanet(float diameter,Color3f lightedColor,Color3f darkColor)
    {
        Material Ma = new Material(lightedColor, darkColor,lightedColor,darkColor, 0);
        Appearance ma = new Appearance();
        ma.setMaterial(Ma);
        Ma.setLightingEnable(true);
        Sphere Sph = new Sphere(diameter,Sphere.GENERATE_NORMALS,140,ma);
        return Sph;
    }
    public RotationInterpolator objRotate(int speed, TransformGroup Rotgroup)
    {
        
        Transform3D yAxis = new Transform3D();
        Alpha rotorAlpha = new Alpha(-1, Alpha.INCREASING_ENABLE,0, 0,speed, 0, 0, 0, 0, 0);
        RotationInterpolator rotator = new RotationInterpolator(rotorAlpha,Rotgroup,yAxis,0.0f, (float) Math.PI*2.0f);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0,0,0.0), 100.0);
        rotator.setSchedulingBounds(bounds);
        return rotator;
    }
    private Canvas3D createUniverse()
    {
        // Get the preferred graphics configuration for the default screen
        GraphicsConfiguration config =SimpleUniverse.getPreferredConfiguration();
        // Create a Canvas3D using the preferred configuration
        Canvas3D c = new Canvas3D(config);
        // Create simple universe with view branch
        univ = new SimpleUniverse(c);
        ViewingPlatform viewingPlatform = univ.getViewingPlatform();

        // add orbit behavior to the ViewingPlatform
        OrbitBehavior orbit = new OrbitBehavior(c, OrbitBehavior.REVERSE_ALL);
        BoundingSphere bounds = new BoundingSphere(new Point3d(0.0, 0.0, 0.0), 100.0);
        orbit.setSchedulingBounds(bounds);
        viewingPlatform.setViewPlatformBehavior(orbit);
    
        // This will move the ViewPlatform back a bit so the
        // objects in the scene can be viewed.
        univ.getViewingPlatform().setNominalViewingTransform();
        // Ensure at least 5 msec per frame (i.e., < 200Hz)
        univ.getViewer().getView().setMinimumFrameCycleTime(5);
        return c;
    }
    /**
     * Creates new form SphereMotion
     */
    public SphereMotion(final String[] args) 
    {
        // Parse the Input Arguments
        String usage = "Usage: java SphereMotion [-point | -spot | -dir]";
        for (int i = 0; i < args.length; i++)
        {
            if (args[i].startsWith("-")) 
            {
                if (args[i].equals("-point"))
                {
                    System.out.println("Using point lights");
                    lightType = POINT_LIGHT;
                }
                else if (args[i].equals("-spot"))
                {
                    System.out.println("Using spot lights");
                    lightType = SPOT_LIGHT;
                }
                else if (args[i].equals("-dir")) 
                {
                    System.out.println("Using directional lights");
                    lightType = DIRECTIONAL_LIGHT;
                }
                else 
                {
                    System.out.println(usage);
                    System.exit(0);
                }
            }
            else 
            {
                System.out.println(usage);
                System.exit(0);
            }
        }              
        // Initialize the GUI components
        initComponents();
        // Create Canvas3D and SimpleUniverse; add canvas to drawing panel
        Canvas3D c = createUniverse();
        drawingPanel.add(c, java.awt.BorderLayout.CENTER);
        // Create the content branch and add it to the universe
        scene = createSceneGraph();
        univ.addBranchGraph(scene);
    }
    // ---------------------------------------------------------------
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc=" Generated Code ">//GEN-BEGIN:initComponents
    private void initComponents() 
    {
        drawingPanel = new javax.swing.JPanel();
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SphereMotion");
        drawingPanel.setLayout(new java.awt.BorderLayout());
        drawingPanel.setPreferredSize(new java.awt.Dimension(700, 700));
        getContentPane().add(drawingPanel, java.awt.BorderLayout.CENTER);
        pack();
    }// </editor-fold>//GEN-END:initComponents
    /**
     * @param args the command line arguments
     */
    public static void main(final String args[]) 
    {
        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run() 
            {
                SphereMotion sphereMotion = new SphereMotion(args);
                sphereMotion.setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel drawingPanel;
    // End of variables declaration//GEN-END:variables
}