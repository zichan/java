import javax.swing.JInternalFrame;
import java.awt.event.*;
import java.awt.*;

/* Used by InternalFrameDemo.java. */
public class EInternalFrame extends JInternalFrame
{
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;
    public EInternalFrame(String name)
    {
        super(name, true, true, true,true);
        setSize(300,300);
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
    }
}