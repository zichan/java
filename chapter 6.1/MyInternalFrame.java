import javax.swing.JInternalFrame;

import java.awt.event.*;
import java.awt.*;

/* Used by InternalFrameDemo.java. */
public class MyInternalFrame extends JInternalFrame
{
    static int openFrameCount = 0;
    static final int xOffset = 30, yOffset = 30;

    public MyInternalFrame(String name)
    {
        super(name, true, true, true,true);

        //...Create the GUI and put it in the window...

        //...Then set the window size or call pack...
        setSize(300,300);
        //Set the window's location.
        setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
    }
}