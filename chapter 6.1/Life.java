import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
/**
 * Jacob Cummings
 */
public class Life extends JFrame
{
	/** the menu bar of this frame */
	private JMenuBar menu = new JMenuBar();

	/** the menu for this frame */
	private JMenu options = new JMenu("Options");

	/** the option item for a new game */
	private JMenuItem newGame = new JMenuItem("New Game");

	/** the option item to pause a game */
	private JMenuItem pause = new JMenuItem("Pause Game");

	/** the option item to display the about box*/
	private JMenuItem about = new JMenuItem("About");

	/** the option item to quit */
	private JMenuItem exit = new JMenuItem("Exit");

	/** the frame object */
    private static Life life;

	/** the drawing canvas, which draws the graphics */
	private Canvas canvas;

	/**
	 * Runs the game of life
	 */
	public static void main(String[] args)
	{
		life = new Life();
	}
	
	/** 
	 * Constructs a game of life
	 */
	public Life ()
	{
		super("The game of Life");
		canvas = new Canvas();		
		getContentPane().add(canvas);
		
		// set up the menu
		options.add(newGame);
		options.add(pause);
		options.add(about);
		options.add(exit);
		menu.add(options);
        setJMenuBar(menu);
        
        // set up the new game listeners
        newGame.addActionListener(new ActionListener() {
        	public void  actionPerformed(ActionEvent e)  {
        		canvas.newGame();
        	}
        });

 		// set up the pause listener
		pause.addActionListener(new ActionListener() {
			public void  actionPerformed(ActionEvent e)  {
        		canvas.pauseGame();
			}
		});

		// set up the about message
		about.addActionListener(new ActionListener() {
			public void  actionPerformed(ActionEvent e)  {
				JOptionPane.showMessageDialog(null,"The game of Life by\n" +
				"   Ben Weber \n   bgweber@calpoly.edu","About", 
				JOptionPane.INFORMATION_MESSAGE); 
			}
		});

		// set up the exit button
		exit.addActionListener(new ActionListener() {
			public void  actionPerformed(ActionEvent e)  {
        		System.exit(0);	
			}
		});        
        
        // display the frame
        setSize(500,500);
        setVisible(true);
	}
}