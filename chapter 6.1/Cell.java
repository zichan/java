/**
 * Jacob Cummings
 */
public class Cell
{
	/** tells if the cell is alive or dead */
	private boolean alive;
	
	/** tells if the status of the cell should change in the next generation */
	private boolean change;

    /**
     * Creates a cell
     * 
     * @param living - true if the cell is living, false if dead
     */
	public Cell(boolean living)
	{
		alive = living;
		change = false;
	}
	
	/** 
	 * Updates the life of the cell
	 */
	public void update()
	{
		if (change) 
			alive = !alive;
		
		change = false;
	}
	
	/**
	 * Returns if the cell is alive
	 * 
	 * @return - true if alive
	 */
	public boolean isAlive()
	{
		return alive;
	}
	 
	/**
	 * 	Tells the cell to change in the next generation
	 */
	public void change()
	{
   		change = true;
	}
}