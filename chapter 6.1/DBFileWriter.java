
import java.io.*;
import java.util.*;
public class DBFileWriter
{
    //string to store the input text file name
    String dbInputFileName;    
    //start constructor
    public DBFileWriter()
    {     
        System.out.println("Enter the file name along with its path>java        <Application_name> <your_filename_path>");
        //Reader object to  read file name from console         
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try
        {
            // checks for empty filename or file other than .txt files
            if((dbInputFileName=br.readLine())!="" && dbInputFileName.endsWith(".txt"))
            {
                DBFileInput();
            }
            else
            {
                 System.out.println("Enter a valid text file name with its fullpath.");
                 // System.exit(0);
            }
        }
        catch(IOException ioe)
        {
            System.out.println("Sorry,we encountered I/O exception ");
        }
    }           //end constructor
    public static void main(String[] args)
    {
           new DBFileWriter(); 
    }
    public void DBFileInput()
    {
        String  dbInputFileName = this.dbInputFileName;
        File fileObject =new File(dbInputFileName); 

        // stores the total records without any blank lines (if any)
        int NO_OF_RECORD_LINES=0;
                    
        //stores the total line count in the file including any blank lines(if any)                         
        int TOTAL_NO_OF_LINES = 0; 

        //string array to store individual record
                        
        String[] recordStoreArray=null; 

        BufferedReader bf1,bf2;

        try
        {
            //opens a stream and attach it to the input file to count no. of records    
            bf1 =new BufferedReader(new InputStreamReader(new FileInputStream(fileObject)));

            // just to check whether the read record is empty or not    
            String holder; 
               
            for(  ;((holder= bf1.readLine())!= null);NO_OF_RECORD_LINES++,TOTAL_NO_OF_LINES++)
            {

                if(holder.equals(""))
                {
                    //If the read record is blank reduce the count by 1 
                    NO_OF_RECORD_LINES--;
                }

            }
            // TOTAL_NO_OF_LINES gives the lowest and highest count within which the records lay
            System.out.println("Total number of lines is "+TOTAL_NO_OF_LINES);
            System.out.println("number of record lines is "+NO_OF_RECORD_LINES);            
    
            //creating the space for the array to store the records.
                                 
            recordStoreArray = new String[TOTAL_NO_OF_LINES];   

            //close the stream after counting all the records
            bf1.close(); 
            
            //actual purpose of bf2 is to read the records 
            bf2=new BufferedReader(new InputStreamReader(new FileInputStream(fileObject)));

            for(int index=0;index < recordStoreArray.length;index++)
            {    
                // actual population into array    
                recordStoreArray[index]=bf2.readLine();
            }
            //close the stream
            bf2.close();    
        
            //call to method which writes to output .txt file           
            DBFileOutput(recordStoreArray);
        }
        catch(FileNotFoundException fnfexp)
        {
            System.out.println("Sorry,file cannot be found ");
        }
        catch(IOException ioexp)
        {
            ioexp.printStackTrace();
        }
        finally
        {
            recordStoreArray=null;      
        }
    }
    public void DBFileOutput(String[] storedRecordArray)
    {
        String[] recordStoreArray = storedRecordArray;
        for(int i=0;i<(recordStoreArray.length);i++)
        {
            if(!(recordStoreArray[i].equals("")))
            {
                System.out.println(recordStoreArray[i]);
            }
        }
        // The first part of insert command                                   
        String preFix="insert into table <tablename> values('"; 
        // The last part of the insert command                   
        String postFix="');";
        //The middle part of the insert command
        String middleString=""; 
        for( int recordCount=0;recordCount < recordStoreArray.length;recordCount++)
        {
            //while the record is not empty
            if(!(recordStoreArray[recordCount].equals("")))
            {
                // StringTokenizer is used to split a large sentence in to words (here columns)   
                StringTokenizer tokenizerObject=new StringTokenizer(recordStoreArray[recordCount]);
                while(tokenizerObject.hasMoreTokens())
                {
                    //space b/w words is replaced with ','
                    middleString=middleString+tokenizerObject.nextToken()+"','";
                }
                System.out.println(middleString);
                //The below sentence remove ',' present after the last word which we don't need    
                middleString = middleString.substring(0,(middleString.length()-3));
                //final construct of the insert command                
                middleString = preFix+middleString+postFix;
                //change the record string into a insert command string                            
                recordStoreArray[recordCount]=middleString;
                tokenizerObject = null;
                middleString = "";
            }   
        }
        for(int i=0;i<(recordStoreArray.length);i++)
        {
            if(!(recordStoreArray[i].equals("")))
            {              
                System.out.println(recordStoreArray[i]);
            }
        }
        try
        {
            //open the output stream and attach it to output file to write into it
            BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new     FileOutputStream("output.txt",true)));  
            for(int i=0;i<(recordStoreArray.length);i++)
            {   
                if(!(recordStoreArray[i].equals("")))
                { 
                    bw.write(recordStoreArray[i],0,recordStoreArray[i].length());
                    bw.newLine();
                }
            }
            //close the writer stream
            bw.close();
        }
        catch(FileNotFoundException fnfexp)
        {
            fnfexp.printStackTrace();
        }
        catch(IOException ioexp)
        {
            ioexp.printStackTrace();
        }
    }
}

