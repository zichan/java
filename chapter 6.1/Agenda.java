/*
 * ##### Well, I don't like the numbers 2 and 7 lol #####
 * 
 * Just kidding ... during PROJECT TIME i did some changes
 * in the code ... i was tired to change every CONTROL NAME ...
 * So the B2 and B7 disappeared ... ehehehehehe lol
 * 
 * Ow ... another thing ... I'm from Brasil ... some Variables are in Portuguese ...
 * just variables ... lol
 * 
 * Any comments, jokes, anything:
 * E-mail: rafaloos@uol.com.br
 * Home Page : www.geocities.com/agronomorafael
 * 
 * 
 * */
import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.event.*;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

public class Agenda extends JFrame implements ListSelectionListener, ActionListener{

// Variables declariation
	private JButton B1,B3,B4,B5,B6,B8; //Bye Bye B2 and B7 ... rest in peace lol
	private TextArea TA1;
	private JList List1;
	private JTextField TF1;
	private Vector names;
	private DefaultListModel listModel;

public Agenda(){

	// JFrame Properties
	setTitle("Phone Book");
	setSize(700,400);
	setResizable(false);
	setLocation(300,200);

	//Instanciating Objects
	B1 = new JButton("New");
	B3 = new JButton("Save");
	B4 = new JButton("Load List");
	B5 = new JButton("Add");
	B6 = new JButton("Edit");
	B8 = new JButton("Delete");
	TA1 = new TextArea(8,20);
	TF1 = new JTextField();
	names = new Vector();

	// List
	listModel = new DefaultListModel();
	List1 = new JList(listModel);
	List1.addListSelectionListener(this);
	JScrollPane PainelLista = new JScrollPane(List1);

	// Setting the ContentPane Layout
	getContentPane().setLayout(new GridLayout(1,2));

	// Building Left Panels
	JPanel botoesEsquerda = new JPanel();
	botoesEsquerda.setLayout(new FlowLayout());
	botoesEsquerda.add(B1);
	botoesEsquerda.add(B3);

	JPanel painelEsquerda = new JPanel();
	painelEsquerda.setLayout(new BorderLayout());
	painelEsquerda.add("Center", TA1);
	painelEsquerda.add("South",botoesEsquerda);

	// Building Right Panels

	JPanel botoesDireita = new JPanel();
	botoesDireita.setLayout(new FlowLayout());
	botoesDireita.add(B4);
	botoesDireita.add(B5);
	botoesDireita.add(B6);
	botoesDireita.add(B8);

	JPanel painelDireita = new JPanel();
	painelDireita.setLayout(new BorderLayout());
	painelDireita.add("North", TF1);
	painelDireita.add("Center", PainelLista);
	painelDireita.add("South", botoesDireita);

	// Adding Panels to ContentPane
	getContentPane().add(painelEsquerda);
	getContentPane().add(painelDireita);

	//Listeners
	B1.addActionListener(this);
	B3.addActionListener(this);
	B4.addActionListener(this);
	B5.addActionListener(this);
	B6.addActionListener(this);
	B8.addActionListener(this);


	
}

	public static void main(String[] args) {
		JFrame window = new Agenda();
		window.show();
	}


	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == B1){TF1.setText("Novo"); open();TF1.setText((String)List1.getSelectedValue());}
		if (e.getSource() == B3){save();}
		if (e.getSource() == B4){Load(e);}
		if (e.getSource() == B5){Add(e);Saving(e);}
		if (e.getSource() == B6){Edit(e);Saving(e);}
		if (e.getSource() == B8){Delete(e);Saving(e);}
	}

	private void Load(ActionEvent evt) {
	try {
		names.clear();
		RandomAccessFile a = new RandomAccessFile("./names.txt","rw");
		String n;
		while ((n = a.readLine()) != null)
			names.add(n);
			List1.setListData(names);
			a.close();
	} catch (Exception e) {}

	}

	private void Saving(ActionEvent evt) {
		try {
			FileWriter out = new FileWriter("./names.txt");
			for (int i = 0; i < names.size(); i++)
				out.write((String)names.elementAt(i) + "\n");
				out.close();
			} catch (Exception e) {}
		}

	private void Delete(ActionEvent evt) {
			
		int indice = List1.getSelectedIndex();
		if (indice > -1) {
			names.removeElementAt(indice);
			List1.setListData(names);
			List1.setSelectedIndex(names.size()-1);
			}
			else {
				JOptionPane.showMessageDialog(null,"Please, select a name");
			}
		}

	private void Edit(ActionEvent evt) {
		int indice = List1.getSelectedIndex();
		if (indice > -1) {
			names.setElementAt(TF1.getText(),List1.getSelectedIndex());
			List1.setListData(names);
		}
		else {
			JOptionPane.showMessageDialog(null,"Please, select a name");
		}
	}

	private void Add(ActionEvent evt) {
		names.add(TF1.getText());
		List1.setListData(names);
		List1.setSelectedIndex(names.size()-1);
	}

	public void valueChanged(ListSelectionEvent arg0) {
		TF1.setText( (String)List1.getSelectedValue());
		open();
	}

	public void open(){
		String filename;
		try{
			filename = "./"+ TF1.getText() +".txt";
			FileReader in = new FileReader(filename);
			String S="";
			int i = in.read();
			while (i!=-1){
				S = S +(char)i;
				i = in.read();
			}
			   TA1.setText(S);
			   in.close();
			}
			catch(java.io.IOException exc){
				TA1.setText("Error Opening the file!");
			}
		}

	public void save(){
		String filename;
		try{
			filename = "./" + TF1.getText() +".txt";
			FileWriter out = new FileWriter(filename);
			out.write(TA1.getText());
			out.close();
		}
		catch(java.io.IOException exc){
			  TA1.setText("Error Saving file");
		}
		}
	
}


