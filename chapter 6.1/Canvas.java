import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;
/**
 * Jacob Cummings
 */
public class Canvas extends JPanel implements Runnable 
{
	/** the speed of the game */
	private int delay = 100;
	
	/** the colony of cells */
	private Colony colony;
	
	/** tells if the game should create new generations */
	private boolean running;
	
	/** tells if the thread should continue running */
	private boolean gameRunning;
	
	/** The thread that updates the graphics */
	private Thread thread;
	
    /** 
     * Constructs a canvas with a random colony
     */	
	public Canvas()
	{
		colony = new Colony();	
		running = true;
        gameRunning = true;
		thread = new Thread(this);
		thread.start();
	}

    /**
     * starts a new game
     */	
	public void newGame()
	{
		colony = new Colony();	
		repaint();
	}
	
	/**
	 * pauses the game
	 */
	public void pauseGame()
	{
		running = !running;
		repaint();
	}
	
	/**
	 * Updates the graphics
	 */
	public void run()
	{
		// loop forever
		while (gameRunning)
		{						
			try 
			{ 			
				// redraw and wait
				repaint();
				Thread.sleep(delay);
				
				// update the colony if the game is running
				if (running) 
					colony.newGeneration(); 
			}
			catch (Exception e) { }
		}
	}	
	
	/**
	 * Repaints the canvas
	 * 
	 * @param g - the graphics context
	 */
	public void paint(Graphics g)
	{
		super.paintComponent(g);

		// the current pixel location of the cell
		double x = 0;
		double y = 0;
		
		// the size of the canvas
		double width = getWidth();
		double height = getHeight();
		
		// the size of each cell
		double dx = width/colony.getWidth();
		double dy = height/colony.getHeight();

		if (dx < 1 ) dx = 1;
		if (dy < 1 ) dy = 1;

		// clear the screen
		g.setColor(Color.white);
		g.fillRect(0,0,(int)width,(int)height);		

        // draw the cells
		g.setColor(Color.black);
		for (int j=0; j<colony.getHeight(); j++)
		{
			for (int i=0; i<colony.getWidth(); i++)
			{
				if (colony.getCellStatus(i,j)) 
				{
					g.fillRect((int)x,(int)y,(int)(dx),(int)(dy));				
				}
				// shift the x position to draw
				x += dx;
			}			

			// shift the y position to draw
			x = 0;
			y += dy;
		}
	}
}