
/**
 * Write a description of class listsort here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class SortedListModel(ListModel model, SortOrder sortOrder, 
                         Comparator comp) {
    unsortedModel = model;
    unsortedModel.addListDataListener(new ListDataListener() {
     public void intervalAdded(ListDataEvent e) {
        unsortedIntervalAdded(e);
      }
 
      public void intervalRemoved(ListDataEvent e) {
       unsortedIntervalRemoved(e);
     }

     public void contentsChanged(ListDataEvent e) {
       unsortedContentsChanged(e);
     }
          
   });
   this.sortOrder = sortOrder;
  if (comp != null) {
     comparator = comp;
   } else {
     comparator = Collator.getInstance();
   }
     
   // Get base model info.
   int size = model.getSize();
   sortedModel = new ArrayList<SortedListEntry>(size);
   for (int x = 0; x < size; ++x) {
     SortedListEntry entry = new SortedListEntry(x);
     int insertionPoint = findInsertionPoint(entry);
     sortedModel.add(insertionPoint, entry);
   }
 }
