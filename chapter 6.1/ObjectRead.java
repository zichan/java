
/**
 * Write a description of class ObjectRead here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.*;
import java.beans.*;
import java.io.Serializable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.Serializable;
public class ObjectRead
{
    static Object load() throws IOException, ClassNotFoundException
    {
        ObjectInputStream in = // The class for de-serialization
        new ObjectInputStream(new FileInputStream("names.nam"));
        return in.readObject(); // This method deserializes an object graph
    }
    public static class Test 
    {
    public static void main(String[] args) throws IOException,
        ClassNotFoundException {
      // Create a simple object graph
      DataStructure ds = new DataStructure();
      ds.data = new int[] {2,4,6,8};
      ds.other = new DataStructure();
      ds.other.data = new int[] { 9, 8, 7 };

      // Display the original object graph
      System.out.println("Original data structure: " + ds);

      // Output it to a file
      File f = new File("datastructure.ser");
      System.out.println("Storing to a file...");
      Serializer.store(ds, f);

      // Read it back from the file, and display it again
      ds = (DataStructure) Serializer.load(f);
      System.out.println("Read from the file: " + ds);
    }
  }
}
class DataStructure implements Serializable
{
    String message;

    int[] data;

    DataStructure other;

    public String toString()
    {
      String s = message;
      for (int i = 0; i < data.length; i++)
        s += " " + data[i];
      if (other != null)
        s += "\n\t" + other.toString();
      return s;
    }
  }
