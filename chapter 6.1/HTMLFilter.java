import java.io.File;
/**
Class to filter files for .html and .htm only


@author Jon Sharpe
*/
public class HTMLFilter extends
javax.swing.filechooser.FileFilter
{
  public boolean accept(File f)
  {
    if (f.isDirectory())
      return true;
    String extension = getExtension(f);
    if ((extension.equals("html")) || (extension.equals("htm")))
       return true;
    return false;
  }
  public String getDescription()
  {
      return "HTML files";
  }
  private String getExtension(File f)
  {
    String s = f.getName();
    int i = s.lastIndexOf('.');
    if (i > 0 &&  i < s.length() - 1)
      return s.substring(i+1).toLowerCase();
    return "";
  }
}