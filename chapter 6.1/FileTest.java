import java.awt.*;
import java.io.*;
import java.util.*;

class FileFrame extends Frame
{
   private Label l1, l2, l3, l4, l5, l6, l7, l8, l9, l0;

   public FileFrame()
   {
      setTitle("File Test");
      Panel p = new Panel();
      p.setLayout(new GridLayout(10, 1));
      p.add(l1 = new Label("First"));
      p.add(l2 = new Label("Second"));
      p.add(l3 = new Label("Third"));
      p.add(l4 = new Label("Fourth"));
      p.add(l5 = new Label("Fifth"));
      p.add(l6 = new Label("Sixth"));
      p.add(l7 = new Label("Seventh"));
      p.add(l8 = new Label("Eighth"));
      p.add(l9 = new Label("Ninth"));
      p.add(l0 = new Label("Tenth"));
      add("Center", p);
   }

   public boolean handleEvent(Event evt)
   {
      
      try
      {
         PrintStream os = new PrintStream(new FileOutputStream("file.dat"));
         Record rec = new Record("John Franco", "118 Fleming", "Cinci, Ohio");
         rec.toStream(os);
         l2.setText(rec.name() + " " + rec.street() + " " + rec.city());
         rec = new Record("Peter Rabbit", "Chicago Zoo", "Chicago, Illinois");
         rec.toStream(os);
         l3.setText(rec.name() + " " + rec.street() + " " + rec.city());
         rec = new Record("John Q. Student", "Sawyer Hall", "Firehouse, NY");
         rec.toStream(os);
         l4.setText(rec.name() + " " + rec.street() + " " + rec.city());
         rec = new Record("New York Times", "Date and Time Hall", "NYC");
         rec.toStream(os);
         l5.setText(rec.name() + " " + rec.street() + " " + rec.city());
         repaint();
      }
      catch(IOException e)
      {
         System.out.print("Error: " + e);
         System.exit(1);
      }

      try
      {
         DataInputStream is = new DataInputStream(new FileInputStream("file.dat"));
         Record rr = new Record ("", "", "");
         try
         {
            rr.splitLine(is.readLine());
            rr.print();
            l7.setText(rr.name() + " " + rr.street() + " " + rr.city());
            rr.splitLine(is.readLine());
            rr.print();
            l8.setText(rr.name() + " " + rr.street() + " " + rr.city());
            rr.splitLine(is.readLine());
            rr.print();
            l9.setText(rr.name() + " " + rr.street() + " " + rr.city());
            rr.splitLine(is.readLine());
            rr.print();
            l0.setText(rr.name() + " " + rr.street() + " " + rr.city());
            repaint();
         }
         catch (java.lang.NullPointerException e) {}
      }
      catch(IOException e)
      {
         System.out.print("Error: " + e);
         System.exit(1);
      }
      
      return true;
   }
}
class Record
{
   private String name;
   private String street;
   private String city;

   public Record(String n, String s, String c)
   {
      name = n;
      street = s;
      city = c;
   }
   public void print()
   {
      System.out.println(name + "--" + street + "--" + city);
   }
   public String name ()
   {
       return name; 
   }
   public String street ()
   { 
       return street;
   }
   public String city ()
   {
       return city;
   }
   public void toStream(PrintStream os) throws IOException
   {
      os.print(name);
      os.print("|");
      os.print(street);
      os.print("|");
      os.print(city);
      os.print("|\n");
   }
   public void splitLine(String s) throws IOException
   {
      StringTokenizer t = new StringTokenizer(s, "|");
      name = t.nextToken();
      street = t.nextToken();
      city = t.nextToken();
   }
}
public class FileTest extends java.applet.Applet implements Runnable
{
   Thread runner;
   FileFrame f;
   public void init ()
   {
      runner = new Thread (this);
      runner.start();
   }
   public void run ()
   {
      f = new FileFrame();
      f.resize(300, 200);
      f.show();
   }
   public void stop ()
   {
      f.dispose();
   }
}