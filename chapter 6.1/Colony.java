import java.util.ArrayList;
/**
 * Jacob Cummings
 */
public class Colony
{
	/** the cells that are in this colony */
	private ArrayList cells;
	
	/** the width of the colony, in cells */
	private int colonyWidth = 50;	

	/** the height of the colony, in cells */
	private int colonyHeight = 50;	

    /**
     * Creates a colony of cells
     */	
	public Colony()
	{
		cells = new ArrayList();
		spawnLife();
	}
	
	/**
	 * Creates the array list of cells, and sets about 1/4 of them alive.
	 */
	public void spawnLife()
	{		
		for (int i=0; i<(colonyWidth*colonyHeight); i++) 
		{
			Cell cell = new Cell(Math.random() < 0.25);
			cells.add(cell);
		}
	}
	
	/**
	 * Updates the generation of the colony. Cells that are alive and have two
	 * or three neighbors stay live, otherwise they die.  Dead cells with 
	 * exactly three cells come to life, otherwise they remain dead.
	 */
	public void newGeneration() 
	{
		// determine the new status of the cells
		for (int j=0; j<colonyHeight; j++)
		{
			for (int i=0; i<colonyWidth; i++)
			{
				determineFate(i,j);
			}					
		}		
		
		// update the cells
		for (int i=0; i<cells.size(); i++)
			((Cell)cells.get(i)).update();
	}
	
	/**
	 * Determines if the cell at the specified location will live
	 * 
	 * @param x - the x location of the cell
	 * @param y - the y location of the cell
	 */
	private void determineFate(int x, int y)
	{
		// the number of neighbors
		int neighbors = 0;
		
		// check the eight neighbors of the cell
		if (getCellStatus(x-1,y+1)) neighbors++;
		if (getCellStatus(x,y+1)) neighbors++;
		if (getCellStatus(x+1,y+1)) neighbors++;
		if (getCellStatus(x+1,y)) neighbors++;
		if (getCellStatus(x+1,y-1)) neighbors++;
		if (getCellStatus(x,y-1)) neighbors++;
		if (getCellStatus(x-1,y-1)) neighbors++;
		if (getCellStatus(x-1,y)) neighbors++;
	
		// set the fate of the cell
		Cell cell = getCell(x,y);

        // determine if the cell should stay dead
		if (cell.isAlive()) 
		{
			if (!(neighbors == 2 || neighbors == 3))
				cell.change();	
		}
		// determine if the cell should come to life
		else
		{
			if (neighbors == 3)
			   cell.change();
		}
	}

	/**
	 * Gets the cell at the specified location
	 * 
	 * @param x - the x location of the cell
	 * @param y - the y location of the cell
	 * @return - the true is the exists and is alive
	 */
	private Cell getCell(int x, int y)
	{
		if (x >= 0 && x<colonyWidth)
		{
			if (y >= 0 && y<colonyHeight)
			{
				return (Cell)cells.get(x + y*colonyWidth);
			}			
		}
		
		return null;		
	}

    /**
     * Gets if the cell at the specified location is alive
     * 
     * @param x - the x location of the cell
     * @param y - the y location of the cell
     * @return - the true is the exists and is alive
     */
	public boolean getCellStatus(int x, int y)
	{
		if (getCell(x,y) != null) 
			return getCell(x,y).isAlive();
		
		return false;		
	}
	
	/**
	 * Gets the width of the colony
	 *
	 * @return - colony width
	 */
	public int getWidth()
	{
		return colonyWidth;
	}

	/**
	 * Gets the height of the colony
	 *
	 * @return - colony height
	 */
	public int getHeight()
	{
		return colonyHeight;
	}	
}