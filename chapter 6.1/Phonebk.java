
/**
 * Write a description of class Phonebk here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.*;
import java.awt.*;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.beans.*;
public class Phonebk extends JFrame implements ActionListener, Serializable
{
    InputStream InNameList;
    FileOutputStream OutNameList;
    JDesktopPane Desktop;
    JButton Open,Edit;
    JInternalFrame selectNames;
    String[] Names;
    JTextField InputFields[];
    public void getList() throws Exception
    {
        
        
        
        Open = new JButton();
        Edit = new JButton("Edit");
        Desktop = new JDesktopPane();
        Open.setActionCommand("Open");
        Edit.setActionCommand("Edit");
        selectNames = new JInternalFrame("Contacts",false,false,false,false);
        selectNames.getContentPane().setLayout(new BorderLayout());
        selectNames.setVisible(true);
        selectNames.setSize(130,300);
        setSize(600,600);
        selectNames.setLocation(1,1);
        setContentPane(Desktop);
        Desktop.setOpaque(true);
        Desktop.add(selectNames);
        JList NameList = new JList(Names)
        {
            public int getScrollableUnitIncrement(Rectangle visibleRect,int orientation,int direction)
            {
                int row;
                if (orientation == SwingConstants.VERTICAL && direction < 0 && (row = getFirstVisibleIndex()) != -1)
                {
                    Rectangle r = getCellBounds(row, row);
                    if ((r.y == visibleRect.y) && (row != 0))
                    {
                        Point loc = r.getLocation();
                        loc.y--;
                        int prevIndex = locationToIndex(loc);
                        Rectangle prevR = getCellBounds(prevIndex, prevIndex);
                        if (prevR == null || prevR.y >= r.y)
                        {
                            return 0;
                        }
                        return prevR.height;
                    }
                }
                return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
            }
        };
        NameList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    Open.doClick();
                }
            }
        });
        selectNames.getContentPane().add(NameList);
        selectNames.add(Open);
    }
    public Phonebk()
    {
        try
        {
            getList();
        }
        catch(Exception C){}
    }
    private static void createAndShowGUI()
    {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(false);
        //Create and set up the window.
        Phonebk frame = new Phonebk();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Display the window.
        frame.setDefaultLookAndFeelDecorated(false);
        frame.setVisible(true);
    }
    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                createAndShowGUI();
            }
        });
    }
    public void actionPerformed(ActionEvent e)
    {
    }
}