import java.io.*;
import java.util.*;
public class MyFirstFileWritingApp
{
    // Main method
    public static void main (String[] args)
    {
        // Stream to write file
        FileOutputStream fout;      
        String[] work = {"qwrhjth","hthdsa"};
        try
        {
            // Open an output stream
            fout = new FileOutputStream ("MyFile.txt");
            // Print a line of text
            new PrintStream(fout).println (work);

            // Close our output stream
            fout.close();       
        }
        // Catches any error conditions
        catch (IOException e)
        {
            System.err.println ("Unable to write to file");
            System.exit(-1);
        }
    }   
}