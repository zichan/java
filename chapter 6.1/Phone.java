
/**
 * Write a description of class Phone here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.*;
import java.util.*;
import java.util.StringTokenizer.*;
import java.lang.Object.*;
import javax.swing.*;

import java.awt.*;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;



public class phone extends JFrame
{
    DefaultListModel Contacts = new DefaultListModel();
    String f = "Jacob/contacts.dat";
    String dir = "Jacob";
    JDesktopPane Desktop;
    JInternalFrame selectNames;
    JList NameList;
    public Phone()
    {
        try
        {
            Start();
        }
        catch(Exception c){}
    }
    public void GetUser() 
    {
        DataInputStream dis;
        try 
        {
            File f = new File("Jacob/contacts.dat"); 
            FileInputStream fis = new FileInputStream(f);  
            BufferedInputStream bis = new BufferedInputStream(fis);  
            dis = new DataInputStream(bis);
            String Names = dis.readLine();
            StringTokenizer dt = new StringTokenizer(Names,":");
            while (dt.hasMoreTokens())
            {
                String nhg=dt.nextToken();
                Contacts.addElement(nhg);
            }
            dis.close();
        }
        catch(Exception e)
        {
            System.out.println("Problem"+e.getMessage());
        }
    }
    public void createCont()
    {
        Label fn,ln,adr,st,zip,hp,wp,cp;
        fn = new Label("First Name");
        ln = new Label("Last Name");
        adr = new Label("Address");
        st = new Label("State");
        zip = new Label("Zip Code");
        hp = new Label("Home #");
        wp = new Label("Work #");
        cp = new Label("Cell #");
        TextField inp[];
        for(int i;i<=2;i++)
        {
            inp[i] = new TextField(10);
        }
        inp[3] = new TextField(20);
        inp[4] = new TextField(5);
        inp[5] = new TextField(10);
        inp[6] = new TextField(10);
        inp[7] = new TextField(10);
        JButton save = new JButton("Save");
        save.setActionCommand("Save");
        JButton cancel = new JButton("Cancel");
        cancel.setActionCommand("Cancel");
        JInternalFrame NContact = new JInternalFrame(false,true,false,true);
        NContact.getContentPane().setLayout(new BorderLayout());
        Panel Bpanel = new Panel();
        Bpanel.getContentPane().setLayout(new FlowLayout());
    }
    public void Start() throws IOException
    {
        GetUser();
        Desktop = new JDesktopPane();
        selectNames = new JInternalFrame("Contacts",false,false,false,false);
        selectNames.getContentPane().setLayout(new BorderLayout());
        selectNames.setVisible(true);
        selectNames.setSize(130,300);
        setSize(600,600);
        selectNames.setLocation(1,1);
        setContentPane(Desktop);
        Desktop.setOpaque(true);
        Desktop.add(selectNames);
        NameList = new JList(Contacts)
        {
            public int getScrollableUnitIncrement(Rectangle visibleRect,int orientation,int direction)
            {
                int row;
                if (orientation == SwingConstants.VERTICAL && direction < 0 && (row = getFirstVisibleIndex()) != -1)
                {
                    Rectangle r = getCellBounds(row, row);
                    if ((r.y == visibleRect.y) && (row != 0))
                    {
                        Point loc = r.getLocation();
                        loc.y--;
                        int prevIndex = locationToIndex(loc);
                        Rectangle prevR = getCellBounds(prevIndex, prevIndex);
                        if (prevR == null || prevR.y >= r.y)
                        {
                            return 0;
                        }
                        return prevR.height;
                    }
                }
                return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
            }
        };
        NameList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                }
            }
        });
        selectNames.getContentPane().add(NameList, BorderLayout.NORTH);
    }
    private static void createAndShowGUI()
    {
        //Make sure we have nice window decorations.
        JFrame.setDefaultLookAndFeelDecorated(false);
        //Create and set up the window.
        Phone frame = new Phone();
        frame.setSize(600,600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //Display the window.
        frame.setDefaultLookAndFeelDecorated(false);
        frame.setVisible(true);
    }
    public static void main(String[] args)
    {
        javax.swing.SwingUtilities.invokeLater(new Runnable()
        {
            public void run()
            {
                createAndShowGUI();
            }
        });
    }
}
