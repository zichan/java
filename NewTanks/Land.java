import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
class Land implements Runnable
{
    private int start;
    private int change;
    private int faktor;
    private int last;
    private int plus;
    boolean landToMove;
    Thread th;
    GameCanvas can;
    private final int mapsize = 700;
    static Integer [] map;
    int indexs;
    public int[][] left= new int[100][3];
    public Color [] colors;
    boolean done=true;
    boolean Efexcomp=false;
    Graphics g;
    Random rnd = new Random ();
    Land(GameCanvas b)
    {
        faktor = 1;
        map = new Integer [mapsize];
        colors = new Color [mapsize];
        generateLandscape();
        can=b;
    }
    public void dooo()
    {   
        th= new Thread(this);
        th.start();
    }
    public void stop()
    {
        th=null;
    }
    public void run()
    {
        done=false;
        boolean thisLandDone[] = new boolean[70];
        while(!can.gameDone)
        {
            for(int i=0;i<=indexs;i++)
            {
                thisLandDone[i]=false;
            }
            while(!done)
            {
                done=true;
                for(int i=0;i<=indexs-1;i++)
                {
                    if(thisLandDone[i]==false)
                    {
                        if(left[i][1]==map[left[i][2]])
                        {
                            map[left[i][2]]=left[i][0];
                            thisLandDone[i]=true;
                        }
                        if(left[i][1]<map[left[i][2]])
                        {
                            left[i][0]++;
                            left[i][1]++;
                            this.done=false;
                        }
                    }
                }
                can.repaint();
                try
                {
                    th.sleep(10);
                }
                catch(Exception t){}
            }
            try
            {
                th.sleep(10);
            }
            catch(Exception t){}
        }
        while(done=true)
        {
            try
            {
                th.wait();
            }
            catch(Exception ujg){}
        }
    }
    public void genHillLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<(mapsize/2);i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==6)
            {
                if(deg+2<10)
                    deg+=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
            }
            else
            {
                if(deg+2<10)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genSlopeLand()
    {
        map[0]=Math.abs(100 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<250;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=250;i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
            }
            else
            {
                if(deg+2<8)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genbumpyLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==4||rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                if(map[i-1]>map[0]+10)
                    deg-=2;
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0]-10)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/2);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                map[i]=map[i-1]+(deg/5);
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
                map[i]=map[i-1]+(deg/2);
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genVallyLand()
    {
        map[0]=Math.abs(100 + (rnd.nextInt() % 50));
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        int deg=2;
        for(int i=1;i<(mapsize/2);i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg-2>-5)
                    deg-=2;
            }
            else
            {
                if(deg+2<8)
                    deg+=2;
            }
            map[i]=map[i-1]+(deg/5);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
        for(int i=(mapsize/2);i<mapsize;i++)
        {
            int rndm = (1+rnd.nextInt(6));
            if(rndm==5||rndm==6)
            {
                if(deg+2<8)
                    deg+=2;
                map[i]=map[i-1]+(deg/5);
            }
            else
            {
                if(deg-2>-5)
                    deg-=2;
                if(map[i-1]<map[0])
                    deg+=2;
                map[i]=map[i-1]+(deg/2);
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void genFlatLand()
    {
        map[0]=Math.abs(300 + (rnd.nextInt() % 50));
        int up;
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        for(int i=1;i<mapsize;i++)
        {
            up=(250+rnd.nextInt(150));
            if(up>map[i-1])
            {
                map[i]=map[i-1]+(1/(1+rnd.nextInt(6)));
            }
            if(up<=map[i-1])
            {
                map[i]=map[i-1]-(1/(1+rnd.nextInt(6)));
            }
            if (greenvalue > 240)
            {
                greenvalue -= 10;
            }
            else if (greenvalue < 100)
            {
                greenvalue += 10;
            }
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void generateLandscape ()
    {
        plus = 1;
        start = Math.abs(300 + (rnd.nextInt() % 50));
        map [0] = start;
        int greenvalue = 200;
        int redvalue = Math.abs(rnd.nextInt() % 200);
        int bluevalue = Math.abs(rnd.nextInt() % 201);
        colors [0] = new Color (redvalue, greenvalue, bluevalue);
        for (int i = 1; i < mapsize; i ++)
        {
            last = map [i - 1];
            change = Math.abs(rnd.nextInt() % 10);
            if (change > 8)
            {
                faktor = - (faktor);
                plus = 1/(1 + Math.abs(rnd.nextInt() % 2));
            }
            if (last > 350 || last < 120)
            {
                faktor = - (faktor);
            }
            if (greenvalue > 240)
            {
                greenvalue -= 10;
            }
            else if (greenvalue < 100)
            {
                greenvalue += 10;
            }
            map [i] = last + (faktor * plus);
            greenvalue = greenvalue + (-faktor * plus);
            colors [i] = new Color (redvalue, greenvalue, bluevalue);
        }
    }
    public void paintMap (Graphics g)
    {
        for (int index = 0; index < mapsize; index ++)
        {
            g.setColor (colors [index]);
            g.drawLine (index, map[index], index, 400);
        }
        if(!done)
        {
            for(int i=0;i<=indexs;i++)
            {
                g.setColor(colors[left[i][2]]);
                g.drawLine(left[i][2],left[i][0],left[i][2],left[i][1]);
            }
        }
    }
}