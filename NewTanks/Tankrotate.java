import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
public class Tankrotate extends Frame
{
    TANK tnk = new TANK();
    public Tankrotate()
    {
        setSize(400,400);
        setLayout(new BorderLayout());
        add(tnk,BorderLayout.CENTER);
        tnk.th.start();
    }
    public static void main(String[] args)
    {
        Tankrotate f = new Tankrotate();
        f.setVisible(true);
    }
}
class TANK extends Canvas implements Runnable
{
    Thread th = new Thread(this);
    double angle=0;
    double mangle;
    Image all;
    Graphics gr;
    int btrans,trans=0;
    boolean gameon=true;
    public void run()
    {
        mangle=1;
        while (gameon)
        {
            mangle=.75;
            angle=0;
            btrans=13;
            trans=0;
            boolean recoil=false;
            boolean recoildone=false;
            while(!recoildone)
            {
                if(!recoil)
                {
                    btrans-=2;
                    trans+=btrans;
                }
                if(trans>=25)
                {
                    recoil=true;
                    btrans=0;
                }
                if(recoil)
                {
                    if(btrans>-1&&trans!=0)
                    {
                        btrans--;
                        trans+=btrans;
                    }
                    if(btrans==-1&&trans>0)
                    {
                        trans+=btrans;
                    }
                    angle+=mangle;
                    if(mangle>=-.6&&angle !=0)
                    {
                        mangle-=.1;
                    }
                    if(angle<=0)
                    {
                        mangle=0;
                        angle=0;
                        recoildone=true;
                    }
                }
                repaint();
                try 
                {
                    th.sleep(75);
                }
                catch (InterruptedException e){}
            }
        }
    }
    public BufferedImage Top()
    {
        BufferedImage i = new BufferedImage(300,100,BufferedImage.TYPE_INT_ARGB);
        Graphics g = i.getGraphics();
        g.setColor(Color.blue);
        g.fillRoundRect(0,55,300,45,20,20);
        return i;
    }
    public BufferedImage Bottom()
    {
        BufferedImage botom = new BufferedImage(300,100,BufferedImage.TYPE_INT_ARGB);
        Graphics g = botom.getGraphics();
        g.setColor(Color.red);
        g.fillRoundRect(20,0,260,40,30,30);
        return botom;
    }
    public BufferedImage top()
    {
        BufferedImage c = Top();
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage top = gc.createCompatibleImage(300, 200, Transparency.TRANSLUCENT);
        Graphics2D tg = top.createGraphics();
        tg.rotate(Math.toRadians(angle), 225,150);
        tg.drawRenderedImage(barel(),null);
        tg.drawRenderedImage(c,null);
        return top;
    }
    public BufferedImage bottom()
    {
        BufferedImage botom=Bottom();
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage Botom = gc.createCompatibleImage(300, 100, Transparency.TRANSLUCENT);
        Graphics2D bg = Botom.createGraphics();
        bg.drawRenderedImage(botom,null);
        return Botom;
    }
    public BufferedImage Barrel()
    {
        BufferedImage barel = new BufferedImage(50,50,BufferedImage.TYPE_INT_ARGB);
        Graphics g = barel.getGraphics();
        g.setColor(Color.black);
        g.fillRect(25,22,25,6);
        return barel;
    }
    public BufferedImage barel()
    {
        BufferedImage botom=Barrel();
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage barel = gc.createCompatibleImage(100,100, Transparency.TRANSLUCENT);
        Graphics2D bg = barel.createGraphics();
        bg.translate(trans,0);
        bg.drawRenderedImage(botom,null);
        return barel;
    }
    public BufferedImage all()
    {
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage all = gc.createCompatibleImage(300, 200, Transparency.TRANSLUCENT);
        Graphics2D bg = all.createGraphics();
        bg.drawImage(Bottom(),0,75,null);
        bg.drawRenderedImage(top(),null);
        return all;
    }
    public void update(Graphics g)
    {
        if(all==null)
        {
            all=createImage(400,400);
            gr=all.getGraphics();
        }
        gr.setColor(getBackground());
        gr.fillRect(0,0,this.getSize().width,this.getSize().height);
        gr.drawImage(all(),0,0,this);
        g.drawImage(all,100,100,this);
    }
    public static GraphicsConfiguration getDefaultConfiguration()
     {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }
}