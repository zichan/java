import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
class Tank
{
    Land lnd;
    int lastx,lasty;
    int x=26/2;
    int y=6;
    int tankx,tanky;
    double bx,by;
    double angle=45;
    double Tankangle;
    int barrel=6;
    int player=0;
    int xcoord=tankx;
    static int PLAYER_ONE=0;
    static int PLAYER_TWO=1;
    double Angle=0,mangle=-.9;
    GameCanvas can;
    boolean recoildone=true;
    Barrel bar;
    public Tank(int a,Land b,GameCanvas c,Barrel bare)
    {
        super();
        lnd = b;
        by=(Math.sin(angle)*barrel)+y;
        bx=(Math.tan(angle)*barrel)+x;
        if(a==0)
        {
            tankx=50;
            tanky=lnd.map[50]-12;
        }
        if(a==1)
        {
            tankx=600;
            tanky=lnd.map[600]-12;
        }
        can=c;
        bar=bare;
    }
    int highy;
    public void conformToLand()
    {
        int highestpoint=tanky;
        for(int a=tankx;a<=tankx+26;a++)
        {
            if(lnd.map[a]<highestpoint+34)
            {
                highestpoint=lnd.map[a]-34;
                xcoord=a;
            }
        }
        tanky=highestpoint;
    }
    public void conformtoLand()
    {
        int highestpoint=400;
        for(int a=tankx;a<=tankx+26;a++)
        {
            if(lnd.map[a]<highestpoint+34)
            {
                highestpoint=lnd.map[a]-34;
                xcoord=a;
            }
        }
        tanky=highestpoint;
    }
    double xpos;
    double ypos;
    public void run()
    {
        while(true)
        {
            
        }
    }
    public void setCorrectPosition()
    {
        int highestpoint=tanky;
        for(int a=tankx;a<=tankx+26;a++)
        {
            if(lnd.map[a]<highestpoint+34)
            {
                highestpoint=lnd.map[a]-34;
                xcoord=a;
            }
        }
        tanky=highestpoint;
        System.out.println(tanky);
        if(xcoord<tankx+12||xcoord==tankx)
        {
            double highestangle=0;
            int farthestpoint=tanky;
            for(int a=xcoord;a<=tankx+26;a++)
            {
                double xaxis=a-xcoord;
                double yaxis=lnd.map[a]-lnd.map[xcoord];
                double Angle=(xaxis!=0&&yaxis!=0)?Math.toDegrees(Math.atan(xaxis/yaxis)):0;
                if(Angle-90<highestangle)
                    highestangle=90-Angle;
            }
            Tankangle=highestangle;
        }
        if(xcoord>tankx+12||xcoord==tankx+26)
        {
        }
    }
    public BufferedImage TankImage()
    {
        BufferedImage image = bar.update();
        double sin = Math.abs(Math.sin(Math.toRadians(angle))), cos = Math.abs(Math.cos(Math.toRadians(angle)));
        int w = image.getWidth(), h = image.getHeight();
        int neww = (int)Math.floor(w*cos+h*sin), newh = (int)Math.floor(h*cos+w*sin);
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage result = gc.createCompatibleImage(neww, newh, Transparency.TRANSLUCENT);
        Graphics2D g = result.createGraphics();
        g.rotate(Math.toRadians(angle), 20, 20);
        if(!recoil&&!recoildone)
        {
            btrans+=.15;
            trans+=btrans;
        }
        if(trans<=-2.5)
        {
            recoil=true;
            btrans=-1;
        }
        if(recoil&&!recoildone)
        {
            if(trans<0)
                trans+=.11;
        }
        g.translate(trans,0);
        g.drawRenderedImage(image, null);
        g.dispose();
        return result;
    }
    public BufferedImage AllTank()
    {
        BufferedImage i = new BufferedImage(40,34,BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = i.createGraphics();
        g.drawImage(botTank(),5,26,null);
        g.drawImage(topTank(),0,0,null);
        return i;
    }
    public BufferedImage TaNk()
    {
        BufferedImage image=AllTank();
        double sin = Math.abs(Math.sin(Math.toRadians(Tankangle))), cos = Math.abs(Math.cos(Math.toRadians(Tankangle)));
        int w = image.getWidth(), h = image.getHeight();
        int neww = (int)Math.floor(w*cos+h*sin), newh = (int)Math.floor(h*cos+w*sin);
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage result = gc.createCompatibleImage(40, 80, Transparency.TRANSLUCENT);
        Graphics2D g = result.createGraphics();
        //g.rotate(Math.toRadians(Tankangle),xcoord-tankx,12);
        g.drawRenderedImage(image,null);
        g.dispose();
        return result;
    }
    boolean first=true;
    boolean recoil=false;
    double trans=0,btrans=-1;
    public BufferedImage topTank()
    {
        GraphicsConfiguration gc = getDefaultConfiguration();
        BufferedImage result = gc.createCompatibleImage(40,40, Transparency.TRANSLUCENT);
        Graphics2D g = result.createGraphics();
        if(recoil&&!recoildone)
        {
            if(angle>360-90)
            {
                if(first==true)
                {
                    mangle=-.90;
                    first=false;
                }
                Angle+=mangle;
                if(mangle<=.6&&Angle!=0)
                {
                    mangle+=.05;
                }
                if(Angle>=0)
                {
                    mangle=-.90;
                    Angle=0;
                    trans=0;
                    recoildone=true;
                    first=true;
                    recoil=false;
                }
            }
            if(angle<360-90)
            {
                if(first==true)
                {
                    mangle=.90;
                    first=false;
                }
                Angle+=mangle;
                if(mangle>=-.6&&Angle!=0)
                {
                    mangle-=.05;
                }
                if(Angle<=0)
                {
                    mangle=.90;
                    Angle=0;
                    trans=0;
                    recoildone=true;
                    first=true;
                    recoil=false;
                }
            }
        }
        g.rotate(Math.toRadians(Angle),20,50);
        g.drawImage(TankImage(),0,0,null);
        g.drawImage(TopTank(),0,18,null);
        return result;
    }
    public BufferedImage TopTank()
    {
        BufferedImage i = new BufferedImage(40,12,BufferedImage.TYPE_INT_ARGB);
        Graphics g = i.getGraphics();
        g.setColor(new Color(58,118,62));
        g.fillRoundRect(12,0,16,5,2,2);
        g.fillRoundRect(0,5,40,7,2,2);
        return i;
    }
    public BufferedImage botTank()
    {
        BufferedImage i = new BufferedImage(30,8,BufferedImage.TYPE_INT_ARGB);
        Graphics g = i.getGraphics();
        g.setColor(Color.black);
        g.fillRoundRect(0,0,30,8,4,4);
        return i;
    }
    public BufferedImage Barel()
    {
        BufferedImage i = new BufferedImage(40,40,BufferedImage.TYPE_INT_ARGB);
        Graphics g = i.getGraphics();
        g.setColor(Color.blue);
        g.fillRect(20,18,20,4);
        return i;
    }
     public static GraphicsConfiguration getDefaultConfiguration()
     {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }
}