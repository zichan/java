import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
class Projectile implements Runnable
{
    BufferedImage i = new BufferedImage(40,34,BufferedImage.TYPE_INT_ARGB);
    Land ls;
    Tank tnk;
    double[] coord=new double[2];
    double xV,yV;
    double gravity = .098;
    Thread thr = new Thread(this);
    boolean impact=false;
    GameCanvas can;
    int e;
    public Projectile(Land l,Tank t,int Speed,int Angle,GameCanvas c,int w)
    {
        ls=l;
        tnk=t;
        xV = (Speed/10)*Math.cos(Math.toRadians(Angle));
        yV = (Speed/10)*Math.sin(Math.toRadians(Angle));
        coord[0] = (Math.cos(Math.toRadians(Angle))*20)+tnk.tankx+20;
        coord[1] = (tnk.tanky+20)-(Math.sin(Math.toRadians(Angle))*20);
        can=c;
        e=w;
    }
    public void run()
    {
        can.impact=false;
        while(!impact)
        {
            coord[0] += xV;
            coord[1] -= yV;
            yV -= gravity/2;
            impact = testForImpact(coord[0], coord[1]);
            can.repaint();
            try
            {
                thr.sleep(20);
            }
            catch (InterruptedException e){}
        }
        impact=true;
        if(e==1)
        {
            can.proj0=null;
        }
        if(e==2)
        {
            can.proj1=null;
            System.out.println("jhitkoe");
        }
    }
    public void proj()
    {
        Graphics2D g = i.createGraphics();
        g.setColor(Color.black);
        g.fillOval(0,0,5,5);
    }
    public boolean testForImpact(double x,double y)
    {
        if(x>699||y>400) return true;
        if(y>=ls.map[(int)x]) return true;
        if(x<=0) return true;
        else return false;
    }
}