
/**
 * Write a description of class Barrel here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
public class Barrel implements Runnable
{
    Thread th = new Thread(this);
    double angle=0;
    double mangle;
    BufferedImage Barrel[]=new BufferedImage[7];
    BufferedImage all;
    Graphics2D[] g= new Graphics2D[7];
    Graphics2D gr;
    int btrans,trans=0;
    boolean gameon=true;
    boolean firstTwoDone=false;
        boolean recoil=false;
    int[][] barrelpositions={{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    public void run()
    {
        gameon=true;
        firstTwoDone=false;
        recoil=false;
        while (gameon)
        {
            if(!firstTwoDone)
            {
                barrelpositions[4][0]+=1;
                barrelpositions[4][1]-=1;
                barrelpositions[5][0]+=1;
                barrelpositions[5][1]+=1;
                if(barrelpositions[4][0]>=barrelpositions[6][0]+10&&barrelpositions[4][1]<=barrelpositions[6][1]-10)
                {
                    firstTwoDone=true;
                    barrelpositions[0][0]=barrelpositions[4][0];
                    barrelpositions[0][1]=barrelpositions[4][1];
                    barrelpositions[1][0]=barrelpositions[4][0];
                    barrelpositions[1][1]=barrelpositions[4][1];
                    barrelpositions[2][0]=barrelpositions[5][0];
                    barrelpositions[2][1]=barrelpositions[5][1];
                    barrelpositions[3][0]=barrelpositions[5][0];
                    barrelpositions[3][1]=barrelpositions[5][1];
                }
            }
            if(firstTwoDone&&barrelpositions[3][1]!=barrelpositions[5][1]+5)
            {
                for(int i=0;i<=3;i++)
                {
                    barrelpositions[i][0]+=1;
                    if(i==0||i==2)
                    {
                        barrelpositions[i][1]-=1;
                    }
                    if(i==1||i==3)
                    {
                        barrelpositions[i][1]+=1;
                    }
                }
            }
            if(barrelpositions[3][1]==barrelpositions[5][1]+5)
                gameon=false;
            update();
            try
            {
                th.sleep(20);
            }
            catch(Exception e){}
        }
        boolean lasts=false;
        while(!recoil)
        {
            if(!lasts)
            {
                for(int i=0;i<=3;i++)
                {
                    barrelpositions[i][0]-=1;
                    if(i==0||i==2)
                    {
                        barrelpositions[i][1]+=1;
                    }
                    if(i==1||i==3)
                    {
                        barrelpositions[i][1]-=1;
                    }
                }
            }
            if(barrelpositions[3][1]==barrelpositions[5][1])
                lasts=true;
            if(lasts)
            {
                barrelpositions[4][0]--;
                barrelpositions[4][1]++;
                barrelpositions[5][0]--;
                barrelpositions[5][1]--;
                barrelpositions[0][0]=barrelpositions[4][0];
                barrelpositions[0][1]=barrelpositions[4][1];
                barrelpositions[1][0]=barrelpositions[4][0];
                barrelpositions[1][1]=barrelpositions[4][1];
                barrelpositions[2][0]=barrelpositions[5][0];
                barrelpositions[2][1]=barrelpositions[5][1];
                barrelpositions[3][0]=barrelpositions[5][0];
                barrelpositions[3][1]=barrelpositions[5][1];
            }
            if(barrelpositions[4][1]==barrelpositions[6][1])
                recoil=true;
            try
            {
                th.sleep(20);
            }
            catch(Exception e){}
        }
        th=null;
    }
    public BufferedImage Top(int i)
    {
        GraphicsConfiguration gc = getDefaultConfiguration();
        Barrel[i] = gc.createCompatibleImage(80,80,Transparency.TRANSLUCENT);
        g[i] = Barrel[i].createGraphics();
        g[i].translate(barrelpositions[i][0],barrelpositions[i][1]);
        g[i].setColor(Color.blue);
        g[i].fillRect(20,18,20,4);
        return Barrel[i];
    }
    public BufferedImage update()
    {
        GraphicsConfiguration gc = getDefaultConfiguration();
        all=gc.createCompatibleImage(80,80,Transparency.TRANSLUCENT);
        gr=all.createGraphics();
        for(int i=0;i<=6;i++)
        {
            gr.drawRenderedImage(Top(i),null);
        }
        return all;
    }
    public static GraphicsConfiguration getDefaultConfiguration()
     {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }
}