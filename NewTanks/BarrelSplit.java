import java.awt.*;
import java.awt.event.*;
import java.lang.Math;
import java.lang.Integer;
import java.util.*;
import java.awt.image.*;
import java.util.*;
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.io.*;
import javax.imageio.*;
import javax.swing.*;
public class BarrelSplit extends Frame
{
    BaRREL tnk = new BaRREL();
    public BarrelSplit()
    {
        setSize(400,400);
        setLayout(new BorderLayout());
        add(tnk,BorderLayout.CENTER);
        tnk.th.start();
    }
    public static void main(String[] args)
    {
        BarrelSplit f = new BarrelSplit();
        f.setVisible(true);
    }
}
class BaRREL extends Canvas implements Runnable
{
    Thread th = new Thread(this);
    double angle=0;
    double mangle;
    BufferedImage Barrel[]=new BufferedImage[7];
    Image all;
    Graphics2D[] g= new Graphics2D[7];
    Graphics gr;
    int btrans,trans=0;
    boolean gameon=true;
    boolean firstTwoDone=false;
    int[][] barrelpositions={{0,0},{0,0},{0,0},{0,0},{0,0},{0,0},{0,0}};
    public void run()
    {
        while (gameon)
        {
            if(!firstTwoDone)
            {
                barrelpositions[4][0]+=1;
                barrelpositions[4][1]-=1;
                barrelpositions[5][0]+=1;
                barrelpositions[5][1]+=1;
                if(barrelpositions[4][0]>=barrelpositions[6][0]+30&&barrelpositions[4][1]<=barrelpositions[6][1]-30)
                {
                    firstTwoDone=true;
                    barrelpositions[0][0]=barrelpositions[4][0];
                    barrelpositions[0][1]=barrelpositions[4][1];
                    barrelpositions[1][0]=barrelpositions[4][0];
                    barrelpositions[1][1]=barrelpositions[4][1];
                    barrelpositions[2][0]=barrelpositions[5][0];
                    barrelpositions[2][1]=barrelpositions[5][1];
                    barrelpositions[3][0]=barrelpositions[5][0];
                    barrelpositions[3][1]=barrelpositions[5][1];
                }
            }
            if(firstTwoDone&&barrelpositions[3][1]!=barrelpositions[5][1]+15)
            {
                for(int i=0;i<=3;i++)
                {
                    barrelpositions[i][0]+=1;
                    if(i==0||i==2)
                    {
                        barrelpositions[i][1]-=1;
                    }
                    if(i==1||i==3)
                    {
                        barrelpositions[i][1]+=1;
                    }
                }
            }
            if(barrelpositions[3][1]==barrelpositions[5][1]+15)
                gameon=false;
            repaint();
            try
            {
                th.sleep(20);
            }
            catch(Exception e){}
        }
        boolean recoil=false;
        boolean lasts=false;
        while(!recoil)
        {
            if(!lasts)
            {
                for(int i=0;i<=3;i++)
                {
                    barrelpositions[i][0]-=1;
                    if(i==0||i==2)
                    {
                        barrelpositions[i][1]+=1;
                    }
                    if(i==1||i==3)
                    {
                        barrelpositions[i][1]-=1;
                    }
                }
            }
            if(barrelpositions[3][1]==barrelpositions[5][1])
                lasts=true;
            if(lasts)
            {
                barrelpositions[4][0]--;
                barrelpositions[4][1]++;
                barrelpositions[5][0]--;
                barrelpositions[5][1]--;
                barrelpositions[0][0]=barrelpositions[4][0];
                barrelpositions[0][1]=barrelpositions[4][1];
                barrelpositions[1][0]=barrelpositions[4][0];
                barrelpositions[1][1]=barrelpositions[4][1];
                barrelpositions[2][0]=barrelpositions[5][0];
                barrelpositions[2][1]=barrelpositions[5][1];
                barrelpositions[3][0]=barrelpositions[5][0];
                barrelpositions[3][1]=barrelpositions[5][1];
            }
            if(barrelpositions[4][1]==barrelpositions[6][1])
                recoil=true;
            repaint();
            try
            {
                th.sleep(20);
            }
            catch(Exception e){}
        }
    }
    public BufferedImage Top(int i)
    {
        GraphicsConfiguration gc = getDefaultConfiguration();
        Barrel[i] = gc.createCompatibleImage(300,300,Transparency.TRANSLUCENT);
        g[i] = Barrel[i].createGraphics();
        g[i].translate(barrelpositions[i][0],barrelpositions[i][1]);
        g[i].setColor(Color.blue);
        g[i].fillRect(140,140,50,10);
        return Barrel[i];
    }
    public void update(Graphics g)
    {
        if(all==null)
        {
            all=createImage(300,300);
            gr=all.getGraphics();
        }
        gr.setColor(getBackground());
        gr.fillRect(0,0,this.getSize().width,this.getSize().height);
        for(int i=0;i<=6;i++)
        {
            gr.drawImage(Top(i),0,0,this);
        }
        g.drawImage(all,0,0,this);
    }
    public static GraphicsConfiguration getDefaultConfiguration()
     {
        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice gd = ge.getDefaultScreenDevice();
        return gd.getDefaultConfiguration();
    }
}