import java.awt.*;
import java.io.FilenameFilter;
import java.io.File;

import javax.swing.*;
import java.awt.Component;
import java.io.*;
import java.util.Vector;
import java.util.LinkedList;

public class UseFileDialog
{
    
     public String loadFile(Frame f, String title, String defDir, String fileType)
     {
         FileDialog fd = new FileDialog(f, title, FileDialog.LOAD);
         fd.setDirectory(defDir);
         fd.setLocation(50, 50);
         fd.setFilenameFilter(new HTMLFilter("name"));
         fd.show();
         return fd.getFile();
     }
     public String saveFile(Frame f, String title, String defDir, String fileType)
     {
         FileDialog fd = new FileDialog(f, title,    FileDialog.SAVE);
         fd.setFile(fileType);
         fd.setDirectory(defDir);
         fd.setLocation(50, 50);
         fd.show();
         return fd.getFile();
     }

     public static void main(String s[])
     {
         UseFileDialog ufd = new UseFileDialog();
         System.out.println
         ("Loading : " + ufd.loadFile(new Frame(), "Open...", ".\\", "*.java"));
         System.out.println
         ("Saving : " + ufd.saveFile(new Frame(), "Save...", ".\\", "*.java"));
         System.exit(0);
     }
}
class HTMLFilter implements FilenameFilter
{
    String ext;
    public HTMLFilter(String ext)
    {
        this.ext="."+ext;
    }
    public boolean accept(File dir, String s)
    {
        if (s.endsWith(ext))
        return true;
        return false;
    }
}
