
/**
 * Write a description of class Adr here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.io.*;
import java.awt.*;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JFrame;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
public class Adr extends JFrame implements ActionListener, Serializable
{
    String dir;
    FileInputStream in;
    FileOutputStream ou;
    JDesktopPane desktop;
    JButton open,edit;
    JList list;
    JTextField field[];
    JLabel label[];
    static Object[] names = {"jacob","Jordan"};
    public static String fileName;
    static int openFrameCount = 0;
    int xOffset = 30, yOffset = 30;
    
    public Object getNames() throws Exception
    {
        Object Names;
        in = new FileInputStream("dat/names.nam");
        ObjectInputStream na = new ObjectInputStream(in);
        Names = na.readObject();
        in.close();
        return names;
    }
    public Adr()
    {
        open = new JButton("Open");
        edit = new JButton("Edit");
        JLabel n = new JLabel("nenmd");
        desktop = new JDesktopPane();
        open.setActionCommand("Open");
        edit.setActionCommand("Edit");
        JInternalFrame frame = new JInternalFrame("name", false,false,false,false);
        frame.getContentPane().setLayout(new BorderLayout());
        frame.setVisible(true);
        frame.setSize(130,300);
        setSize(600,600);
        //Set the window's location.
        frame.setLocation(xOffset*openFrameCount, yOffset*openFrameCount);
        setContentPane(desktop);
        desktop.setOpaque(true);
        desktop.add(frame);
        open.addActionListener(this);
        edit.addActionListener(this);
        list = new JList(names)
        {
            //Subclass JList to workaround bug 4832765, which can cause the
            //scroll pane to not let the user easily scroll up to the beginning
            //of the list.  An alternative would be to set the unitIncrement
            //of the JScrollBar to a fixed value. You wouldn't get the nice
            //aligned scrolling, but it should work.
            public int getScrollableUnitIncrement(Rectangle visibleRect,int orientation,int direction)
            {
                int row;
                if (orientation == SwingConstants.VERTICAL && direction < 0 && (row = getFirstVisibleIndex()) != -1)
                {
                    Rectangle r = getCellBounds(row, row);
                    if ((r.y == visibleRect.y) && (row != 0))
                    {
                        Point loc = r.getLocation();
                        loc.y--;
                        int prevIndex = locationToIndex(loc);
                        Rectangle prevR = getCellBounds(prevIndex, prevIndex);
                        if (prevR == null || prevR.y >= r.y)
                        {
                            return 0;
                        }
                        return prevR.height;
                    }
                }
                return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
            }
        };
        list.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    open.doClick(); //emulate button click
                }
            }
        });
        Panel button = new Panel();
        button.setLayout(new BorderLayout());
        button.add(open, BorderLayout.WEST);
        button.add(edit, BorderLayout.EAST);
        frame.getContentPane().add(button,BorderLayout.SOUTH);
        frame.getContentPane().add(list,BorderLayout.NORTH);
    }
    
    public static void main(String[] args)
    {
        Adr f = new Adr();
        f.setVisible(true);
    }
    protected void quit()
    {
        try
        {
            ou = new FileOutputStream(dir);
            ObjectOutputStream them = new ObjectOutputStream(ou);
            them.writeObject(names);
        }
        catch(IOException t)
        {
        }
        System.exit(0);
    }
    public JLabel create_Labels(int i)
    {
        label[1] = new JLabel("First Name");
        label[2] = new JLabel("Last Name");
        label[3] = new JLabel("Address");
        label[4] = new JLabel("Zip Code");
        return label[i];
    }
    public JTextField create_Frame(int i)
    {
        field[i] = new JTextField();
        return field[i];
    }
    public void save_Info(PrintStream os)
    {
        os.print(names);
        System.exit(0);
    }
    protected void createframe()
    {
        EInternalFrame frame = new EInternalFrame((String) list.getSelectedValue());
        Label n = new Label("Mine");
        frame.add(create_Frame(1));
        try
        {
            DataInputStream is = new DataInputStream(new FileInputStream("dir/"+list.getSelectedValue()+".nam"));
            Record th = new Record("","","");
            try
            {
                th.splitLine(is.readLine());
                label[1].setText(th.FName());
            }
            catch (java.lang.NullPointerException p) {}
        }
        catch(IOException w){}
        frame.setVisible(true); //necessary as of 1.3
        desktop.add(frame);
    }
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource()==open)
        {
            createframe();
        }
    }
}
class Record
{
   private String name;
   private String street;
   private String fname;

   public Record(String n, String s, String c)
   {
      name = n;
      street = s;
      fname = c;
   }
   public String name ()
   {
       return name; 
   }
   public String street ()
   { 
       return street;
   }
   public String FName ()
   {
       return fname;
   }
   public void toStream(PrintStream os) throws IOException
   {
      os.print(name);
      os.print("|");
      os.print(street);
      os.print("|");
      os.print(fname);
      os.print("|\n");
   }
   public void splitLine(String s) throws IOException
   {
      StringTokenizer t = new StringTokenizer(s, "|");
      name = t.nextToken();
      street = t.nextToken();
      fname = t.nextToken();
   }
}