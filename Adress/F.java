import java.awt.Button.*;
import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.TextField;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;
public class F extends Frame implements ActionListener
{
    Button load, save, quit;
    final TextField dir, file;
    public F()
    {
        setLayout(new GridLayout(5,1));
        add(load = new Button("Load..."));
        add(save = new Button("Save..."));
        add(dir = new TextField(50));
        dir.setEditable(true);
        add(file = new TextField(50));
        file.setEditable(true);
        add(quit = new Button("Quit"));

        load.addActionListener(this);
        save.addActionListener(this);
        quit.addActionListener(new ActionListener()
        {
            public void actionPerformed (ActionEvent e)
            {
                System.exit(0);
            }
        });
    }
    public void actionPerformed(ActionEvent e)
    {
        if (e.getSource()==save)
        {
            
            try
            {
                FileDialog d = new FileDialog(F.this, "Save...", FileDialog.SAVE);
                d.pack();
                d.show();
                File f = new File(d.getDirectory() + File.separator + d.getFile());
                String dit = f.getPath();
                PrintStream os = new PrintStream(new FileOutputStream(dit));
                record rec = new record(dir.getText(), file.getText(), "Cinci, Ohio");
                rec.toStream(os);
                repaint();
                d.dispose();
            }
            catch(IOException r)
            {
                dir.setText("Error in saving file");
            }

        }
        if (e.getSource()==load)
        {
            FileDialog d = new FileDialog(F.this, "Load...", FileDialog.LOAD);
            d.pack();
            d.show();
            File f = new File(d.getDirectory() + File.separator + d.getFile());
            String dit = f.getPath();
            try
            {
                DataInputStream is = new DataInputStream(new FileInputStream(dit));
                record th = new record("","","");
                try
                {
                    th.splitLine(is.readLine());
                    dir.setText(th.name());
                    file.setText(th.street());
                }
                catch (java.lang.NullPointerException p) {}
            }
            catch(IOException w)
            {
                dir.setText("Error in reading the file");
            }
        }
    }
    public static void main(String[] args)
    {
        F f = new F();
        f.setVisible(true);
    }
}
class record
{
   private String name;
   private String street;
   private String city;

   public record(String n, String s, String c)
   {
      name = n;
      street = s;
      city = c;
   }
   public void print()
   {
      System.out.println(name + "--" + street + "--" + city);
   }

   public String name ()
   {
       return name; 
   }
   public String street ()
   { 
       return street;
   }
   public String city ()
   {
       return city;
   }
   public void toStream(PrintStream os) throws IOException
   {
      os.print(name);
      os.print("|");
      os.print(street);
      os.print("|");
      os.print(city);
      os.print("|\n");
   }
   public void splitLine(String s) throws IOException
   {
      StringTokenizer t = new StringTokenizer(s, "|");

      name = t.nextToken();
      street = t.nextToken();
      city = t.nextToken();
   }
   
}