
/**
 * Write a description of class hj here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public abstract interface java.io.ObjectInput extends  java.io.Externalizable
{
  // Methods
  public abstract int available();
  public abstract void close();
  public abstract int read();
  public abstract int read(byte[] b);
  public abstract int read(byte[] b, int off, int len);
  public abstract Object readObject();
  public abstract long skip(long n);
  public abstract Object readObject() throws ClassNotFoundException, IOException;
}

