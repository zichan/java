import java.io.*;

public class MyFirstFileReadingApp
{
	public static void main (String args[])
	{
		FileInputStream fin;
		try
		{
		    fin = new FileInputStream ("myfile.txt");
		    System.out.println(new DataInputStream(fin).readLine());
		    fin.close();		
		}
		catch (IOException e)
		{
			System.err.println ("Unable to read from file");
			System.exit(-1);
		}
	}	
}
