import java.io.*;

public class MyFirstFileWritingApp
{
	// Main method
	public static void main (String args[])
	{
		// Stream to write file
		FileOutputStream fout;		

		try
		{
		    // Open an output stream
		    fout = new FileOutputStream ("myfile.txt");

		    // Print a line of text
		    new PrintStream(fout).println ("hello world!");

		    // Close our output stream
		    fout.close();		
		}
		// Catches any error conditions
		catch (IOException e)
		{
			System.err.println ("Unable to write to file");
			System.exit(-1);
		}
	}	
}