import java.util.StringTokenizer;
   import java.awt.event.* ;
   import java.awt.* ;
   import javax.swing.*;
   class AddressBook extends JFrame implements ActionListener{
      public SortableVector entryList;
      Entry current;
      JLabel display;
      int pos = 0;
      boolean empty = false;
      boolean newEntry = false;
      JTextArea noteArea;
      JTextField lastName, firstName, homePhone, mobilePhone, faxPhone,addressField, suburbField, postCode, stateField, countryField ;
      JButton previousB, nextB;
   
      AddressBook() throws Exception{
         super("Address Book");
         getContentPane().setLayout(new BorderLayout(0, 0));
         setLocation(100,0) ;  
         setBackground(Color.lightGray) ;
         entryList = DataAccess.load(); // try to read from the data file
         if (entryList == null){
            empty = true;
            entryList = new SortableVector();
         	// initialise the entryList, it is empty
         }
         addWindowListener(
                             new WindowAdapter() {
                                public void windowClosing(WindowEvent e) {
                                   exitEvent();
                                }});
         //start of menu
         JMenuBar menuBar = new JMenuBar();
         setJMenuBar(menuBar);
         JMenu fileMenu = new JMenu("File");
         fileMenu.setMnemonic(KeyEvent.VK_F);
         JMenu entrysMenu = new JMenu("Entrys");
         entrysMenu.setMnemonic(KeyEvent.VK_E);
         JMenu sortbyMenu = new JMenu("Sort by");
         sortbyMenu.setMnemonic(KeyEvent.VK_S);
         JMenu helpMenu = new JMenu("Help");
         helpMenu.setMnemonic(KeyEvent.VK_H);
         menuBar.add(fileMenu);
         menuBar.add(entrysMenu);
         menuBar.add(sortbyMenu);
         menuBar.add(helpMenu);
         JMenuItem saveM = new JMenuItem("Save");
         saveM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
         saveM.addActionListener(this);
         fileMenu.add(saveM);
         JMenuItem refreshM = new JMenuItem("Refresh");
         refreshM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
         refreshM.addActionListener(this);
         fileMenu.add(refreshM);
         JMenuItem exitM = new JMenuItem("Exit");
         exitM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK));
         exitM.addActionListener(this);
         fileMenu.add(exitM);
         JMenuItem newM = new JMenuItem("New");
         newM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
         newM.addActionListener(this);
         entrysMenu.add(newM);
         JMenuItem previousM = new JMenuItem("Previous");
         previousM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_KP_LEFT , ActionEvent.CTRL_MASK));
         previousM.addActionListener(this);
         entrysMenu.add(previousM);
         JMenuItem nextM = new JMenuItem("Next");
         nextM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_KP_RIGHT, ActionEvent.CTRL_MASK));
         nextM.addActionListener(this);
         entrysMenu.add(nextM);
         JMenuItem deleteM = new JMenuItem("Delete");
         deleteM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, ActionEvent.CTRL_MASK));
         deleteM.addActionListener(this);
         entrysMenu.add(deleteM);
         JMenuItem sortFNameM = new JMenuItem("First name");
         sortFNameM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
         sortFNameM.addActionListener(this);
         sortbyMenu.add(sortFNameM);
         JMenuItem sortLNameM = new JMenuItem("Last name");
         sortLNameM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
         sortLNameM.addActionListener(this);
         sortbyMenu.add(sortLNameM);
      
         JMenuItem aboutM = new JMenuItem("About");
         aboutM.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
         aboutM.addActionListener(this);
         helpMenu.add(aboutM);
      	//end of menu
      
      
      
         JPanel top = new JPanel(new GridLayout(10,2));
         top.add(new JLabel("First Name"));
         firstName = new JTextField();
         top.add(firstName);
         top.add(new JLabel("Last Name"));
         lastName = new JTextField();
         top.add(lastName);
         top.add(new JLabel("Home Phone"));
         homePhone = new JTextField();
         top.add(homePhone);
         top.add(new JLabel("Mobile Phone"));
         mobilePhone = new JTextField();
         top.add(mobilePhone);
         top.add(new JLabel("Fax Number"));
         faxPhone = new JTextField();
         top.add(faxPhone);
         top.add(new JLabel("Address"));
         addressField = new JTextField();
         top.add(addressField);
         top.add(new JLabel("Suburb"));
         suburbField = new JTextField();
         top.add(suburbField);
         top.add(new JLabel("Post Code"));
         postCode = new JTextField();
         top.add(postCode);
         top.add(new JLabel("State"));
         stateField = new JTextField("SA");
         top.add(stateField);
         top.add(new JLabel("Country"));
         countryField = new JTextField("Australia");
         top.add(countryField);
         getContentPane().add(top, BorderLayout.NORTH);
      
         JPanel middle = new JPanel(new BorderLayout());
         getContentPane().add(middle, BorderLayout.CENTER);
         JPanel noteJPanel = new JPanel();
         middle.add(noteJPanel, BorderLayout.NORTH);
         display = new JLabel("", JLabel.CENTER);
         middle.add(display, BorderLayout.CENTER);
         noteJPanel.add(new JLabel("Notes"));
         noteArea = new JTextArea(Entry.maxNotes,45);
         noteArea.setFont(new Font("Serif", Font.PLAIN, 16));
         noteArea.setLineWrap(true);
         noteArea.setWrapStyleWord(true);
      
         noteJPanel.add(noteArea);
         JPanel buttons = new JPanel(new GridLayout(1,6));
         JButton newB = new JButton("New");
         newB.addActionListener(this);
         buttons.add(newB);
        // JButton saveB = new JButton("Save");
         //saveB.addActionListener(this);
         //buttons.add(saveB);
         //JButton refreshB = new JButton("Refresh");
         //refreshB.addActionListener(this);
         //buttons.add(refreshB);
         previousB = new JButton("Previous");
         previousB.addActionListener(this);
         buttons.add(previousB);
         nextB = new JButton("Next");
         nextB.addActionListener(this);
         buttons.add(nextB);
         JButton deleteB = new JButton("Delete");
         deleteB.addActionListener(this);
         buttons.add(deleteB);
         //JButton exitB = new JButton("Exit");
        // exitB.addActionListener(this);
         //buttons.add(exitB);
         getContentPane().add(buttons, BorderLayout.SOUTH);
         setEntry(pos); //set the screen to the first entry
         pack();
         setVisible(true);
      }
      void setEntry(int num){
         if(empty){
            addNewEntry();
            empty = false;
         }
         else{
         
         // start of enabling buttons
            if(num < 1){
               previousB.setEnabled(false);
            }
            else
               previousB.setEnabled(true);
         
            if(num >= (entryList.size()-1)){
               nextB.setEnabled(false);
            }
            else
               nextB.setEnabled(true);
         	// end of enabling buttons
         
            current = (Entry) entryList.elementAt(num);
            display.setText((num + 1) + " of " + entryList.size());
            pos = num;
         //set the fields to what the entry contains
         
            firstName.setText(current.fName);
            lastName.setText(current.lName);
            homePhone.setText(current.hPhone);
            mobilePhone.setText(current.mPhone);
            faxPhone.setText(current.fPhone);
            addressField.setText(current.address);
            suburbField.setText(current.suburb);
            postCode.setText(current.pCode);
            stateField .setText(current.state);
            countryField.setText(current.country);
            noteArea.setText("");
            for(int i=0; i < Entry.maxNotes; i++){
               if(current.notes[i] != ""){
                  noteArea.append(current.notes[i]);
               	// don't print new line after last line of notes
                  if(i < (Entry.maxNotes-1)){
                     noteArea.append("\n");
                  }
               }
            }
         }
      }
      void addNewEntry(){
         if(previousB.isEnabled()){
            previousB.setEnabled(false);
         }
         if(!nextB.isEnabled()){
            nextB.setEnabled(true);
         }
         newEntry = true;
         current = new Entry();
         display.setText("This is a new entry, click save to add, next for another new one");
         firstName.setText("");
         lastName.setText("");
         homePhone.setText("");
         mobilePhone.setText("");
         faxPhone.setText("");
         addressField.setText("");
         suburbField.setText("");
         postCode.setText("");
         stateField .setText("SA");
         countryField.setText("Australia");
         noteArea.setText("");
      }
      void saveToFile(){
         saveToMem();
         try{
            DataAccess.save(entryList);}
            catch(Exception e){
               showErrorMessage("Error saving data to " + DataAccess.file);
            }
      }
      void saveNewEntry(){
         entryList.addElement(current);
         pos = entryList.size()-1;
         setEntry(pos);
         newEntry = false;
      }
      void saveToMem(){
         int counter=0;
         current.fName = firstName.getText();
         current.lName = lastName.getText();
         current.hPhone = homePhone.getText();
         current.mPhone = mobilePhone.getText();
         current.fPhone = faxPhone.getText();
         current.address = addressField.getText();
         current.suburb = suburbField.getText();
         current.pCode = postCode.getText();
         current.state = stateField.getText();
         current.country = countryField.getText();
         StringTokenizer tok = new StringTokenizer(noteArea.getText(), "\n");
         for(int i=0; i < Entry.maxNotes; i++){
            current.notes[i] = "";
         }
         try{
            while(tok.hasMoreTokens()){
               current.notes[counter++] = tok.nextToken();
            }
         }
            catch(ArrayIndexOutOfBoundsException e){
               showErrorMessage("You cannot have more than "+ Entry.maxNotes +" lines of notes.");
            }
         if(newEntry){
            saveNewEntry();
         }
         else
            entryList.setElementAt(current, pos);
      }
      void showErrorMessage(String error){
         JOptionPane.showMessageDialog(this, error, "An errror has occurred", JOptionPane.ERROR_MESSAGE);
      }
      void newEvent(){
         saveToMem();
         addNewEntry();
      }
      void saveEvent(){
         saveToFile();
      }
      void refreshEvent(){
         if(! empty){
            try{
               entryList = DataAccess.load();}
               catch(Exception e){
                  showErrorMessage("Error accessing data from " + DataAccess.file);
               }
            if(entryList == null){
               entryList = new SortableVector();
               showErrorMessage("You cannot refresh from an empty data file");
            }
            else{
               if(newEntry){
                  newEntry = false;
               }
               try{	
                  setEntry(pos);
                // if the entry does not exist in the refreshed data, set it to thte last one
               }
                  catch(ArrayIndexOutOfBoundsException e){
                     setEntry(entryList.size()-1);
                  }
            }
         }
      }
      void previousEvent(){
         saveToMem();
         int newPos = pos -1;
         if(newPos >= 0 && newPos < entryList.size()){
            setEntry(newPos);
         }
      }
      void nextEvent(){
         if(newEntry){
            saveToMem();
            addNewEntry();
         }
         else{
            saveToMem();
            int newPos = pos +1;
            if(newPos >= 0 && newPos < entryList.size()){
               setEntry(newPos);
            }
         }
      }
      void deleteEvent(){
         if(newEntry){
            if(entryList.size() > 0 && pos < entryList.size()){
               newEntry = false;
               setEntry(pos);
            }
            else{
               addNewEntry();
            }
         }
         else{
            if(entryList.size() == 1){
               entryList.removeElementAt(0);
               addNewEntry();
            }
            else{
               entryList.removeElementAt(pos);
               if(pos == 0)
                  setEntry(0);
               else
                  setEntry(pos-1);
            }
         }
      }
      void exitEvent(){
         saveToFile();
         System.exit(0);
      }
      void sortFNameEvent(){
         saveToMem();
         entryList.sortByFirstName();
         setEntry(pos);
      }
      void sortLNameEvent(){
         saveToMem();
         entryList.sortByLastName();
         setEntry(pos);
      }
      void aboutEvent(){
         JOptionPane.showMessageDialog(this, "Created in 2003 by Andrew Murphy\n Email: murp0118@flinders.edu.au", "About AddressBook",JOptionPane.INFORMATION_MESSAGE);
      }
      public void actionPerformed (ActionEvent event) {
         String command = event.getActionCommand();
         if(command.equals("New")){
            newEvent();
         }
         else if(command.equals("Save")){
            saveEvent();
         }
         else if(command.equals("Refresh")){
            refreshEvent();
         }
         else if(command.equals("Previous")){
            previousEvent();
         }
         else if(command.equals("Next")){
            nextEvent();
         }
         else if(command.equals("Delete")){
            deleteEvent();
         }
         else if (command.equals("Exit")){
            exitEvent();
         }
         else if (command.equals("First name")){
            sortFNameEvent();
         }
         else if (command.equals("Last name")){
            sortLNameEvent();
         }
         else if (command.equals("About")){
            aboutEvent();
         }
      }
   
   
      public static void main(String[] args)throws Exception{
         AddressBook ab = new AddressBook();
      }
   }
