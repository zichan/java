
   import java.util.StringTokenizer;
   import java.io.*;        
   class DataAccess
   {
      static String file = "data.dat";
      public static SortableVector load()throws IOException
      {
         boolean empty = true;
         String line;
         SortableVector entryList = new SortableVector();
         try
     {
            BufferedReader input = new BufferedReader(new FileReader(file));
            while((line = input.readLine()) !=null && line.length() != 0)
        {
               empty = false;
               int counter = 0;            
               Entry entry = new Entry();
               DataTokenizer dt = new DataTokenizer(line);
               entry.fName = dt.nextToken();
               entry.lName = dt.nextToken();
               entry.hPhone = dt.nextToken();
               entry.mPhone = dt.nextToken();
               entry.fPhone = dt.nextToken();
               entry.address = dt.nextToken();
               entry.suburb = dt.nextToken();
               entry.pCode = dt.nextToken();
               entry.state = dt.nextToken();
               entry.country = dt.nextToken();
               entry.CID = dt.nextToken();
               input.readLine();
               while(!(line = input.readLine()).equals("$$"))
               {
                   entry.notes[counter++] = line;
               }
               entryList.addElement(entry);
            }
            input.close();
         }
         catch(FileNotFoundException e)
         {
            new File(file).createNewFile();
            return null;
         }
         if(empty) return null;
         return entryList;
      }
   
      public static void save(SortableVector entryList)throws IOException{
         FileWriter fw = new FileWriter(file);
         BufferedWriter bw = new BufferedWriter(fw);
         PrintWriter outFile = new PrintWriter(bw);
         for(int i=0; i < entryList.size(); i++)
     {
            Entry entry = (Entry) entryList.elementAt(i);
            outFile.print("*" + entry.fName);
            outFile.print("~*");
            outFile.print(entry.lName);
            outFile.print("~*");
            outFile.print(entry.hPhone);
            outFile.print("~*");
            outFile.print(entry.mPhone);
            outFile.print("~*");
            outFile.print(entry.fPhone);
            outFile.print("~*");
            outFile.print(entry.address);
            outFile.print("~*");
            outFile.print(entry.suburb);
            outFile.print("~*");
            outFile.print(entry.pCode);
            outFile.print("~*");
            outFile.print(entry.state);
            outFile.print("~*");
            outFile.print(entry.country);
            outFile.print("~*");
            outFile.print(entry.CID);
            outFile.println();
            outFile.println("$$");
            for(int j=0; j < Entry.maxNotes; j++)
        {
               if(entry.notes[j] != "")
           {
                  outFile.println(entry.notes[j]);
               }
            }
            outFile.println("$$");
         }
         outFile.close();
      }
   }