import java.util.Vector;
class SortableVector extends Vector
{
// just an extension of the Vector with bubble sort algorithms implemented to sort the entrys		
   public void sortByFirstName(){
      Entry first;
      Entry second; 
      boolean swapped = true ;
      for (int j = 1 ; j < size()  && swapped ; j++)  {
         swapped = false ;
         for (int i = 0 ; i < size() - j ; i++) {
            first = (Entry)elementAt(i);
            second = (Entry)elementAt(i+1);
            if (first.fName.compareToIgnoreCase(second.fName) > 0 ) { 
               // if the first one is larger than the second, swap
               setElementAt(second,i) ;
               setElementAt(first, i+1) ; 
               swapped=true ;
            }
         }
      }
   }
   public void sortByLastName()
   {
      Entry first;
      Entry second; 
      boolean swapped = true ;
      for (int j = 1 ; j < size()  && swapped ; j++)
      {
         swapped = false ;
         for (int i = 0 ; i < size() - j ; i++)
         {
            first = (Entry)elementAt(i);
            second = (Entry)elementAt(i+1);
            if (first.lName.compareToIgnoreCase(second.lName) > 0 ) 
            { 
               // if the first one is larger than the second, swap
               setElementAt(second,i) ;
               setElementAt(first, i+1) ; 
               swapped=true ;
            }
         }
      }
   }
}
