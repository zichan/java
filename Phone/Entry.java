class Entry
{
   public String fName, lName, hPhone, mPhone, fPhone, address, suburb, pCode, state, country, CID;
   public String notes[];
   static int maxNotes = 10;
    // the maximum allowable amount of lines of notes, this will effect all other classes
   Entry()
   {
      notes = new String[maxNotes];
      for(int i =0; i < maxNotes; i++)
      {
         notes[i] = "";
      }
      fName = "";
      lName = "";
      hPhone = "";
      mPhone = "";
      fPhone = "";
      address = "";
      suburb = "";
      pCode = "";
      state = "";
      country = "";
      CID = "";
   }
}