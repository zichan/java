import javax.swing.*;
public class EditableTextFieldTest 
{
    public static void main(String args[]) 
    {
        JTextField tf1 = new JTextField("Frame", 20);
        JTextField tf2 = new JTextField("Owned Window", 20);
        JTextField tf3 = new JTextField("Free Window", 20);
        JFrame f = new JFrame();
        f.getContentPane().add( tf1 );
        f.pack();
        f.setLocation(-100,-100);
        f.setVisible(true);
        JWindow wO = new JWindow(f);
        wO.getContentPane().add( tf2 );
        wO.pack();
        wO.setLocation(100,100);
        wO.setVisible(true);
        JWindow wF = new JWindow();
        wF.getContentPane().add( tf3 );
        wF.pack();
        wF.setLocation(150,150);
        wF.setVisible(true);
    }
}
