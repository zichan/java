import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
 
public class ListRightClick extends JFrame
{
	public ListRightClick()
	{
		String[] data = { "zero", "one", "two", "three", "four", "five" };
		JList list = new JList( data );
		list.addMouseListener( new MouseAdapter()
		{
			public void mousePressed(MouseEvent e)
			{
				if ( SwingUtilities.isRightMouseButton(e) )
				{
 					try
 					{
 						Robot robot = new java.awt.Robot();
 						robot.mousePress(InputEvent.BUTTON1_MASK);
 						robot.mouseRelease(InputEvent.BUTTON1_MASK);
 					}
 					catch (AWTException ae) { System.out.println(ae); }
				}
			}
 
			public void mouseReleased(MouseEvent e)
			{
				if ( SwingUtilities.isRightMouseButton(e) )
				{
					JList list = (JList)e.getSource();
					System.out.println(list.getSelectedValue() + " selected");
				}
			}
		});
 
		getContentPane().add( new JScrollPane(list) );
	}
 
	public static void main(String[] args)
	{
		ListRightClick frame = new ListRightClick();
		frame.setDefaultCloseOperation( EXIT_ON_CLOSE );
		frame.setSize(400, 100);
		frame.setLocationRelativeTo( null );
		frame.setVisible( true );
	}
}

