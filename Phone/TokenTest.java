import java.util.*;
import java.io.*;
import java.util.StringTokenizer.*;
public class TokenTest {  

     public static void main (String[] args) { 
        TokenTest tt = new TokenTest(); 
        tt.dbTest(); 
     } 
  

     void dbTest() {  

        DataInputStream dis = null; 
        String dbRecord = null; 

        try {  

           File f = new File("customer.txt"); 
           FileInputStream fis = new FileInputStream(f);  
           BufferedInputStream bis = new BufferedInputStream(fis);  
           dis = new DataInputStream(bis); 

           // read the first record of the database 
           while ( (dbRecord = dis.readLine()) != null) { 

              StringTokenizer st = new StringTokenizer(dbRecord, ":"); 
              String fname = st.nextToken(); 
              String lname = st.nextToken(); 
              String city  = st.nextToken(); 
              String state = st.nextToken(); 

              System.out.println("First Name:  " + fname); 
              System.out.println("Last Name:   " + lname); 
              System.out.println("City:        " + city); 
              System.out.println("State:       " + state + "\n"); 
           } 

        } catch (IOException e) {  
           // catch io errors from FileInputStream or readLine()  
           System.out.println("Uh oh, got an IOException error: " + e.getMessage());  

        } finally {  
           // if the file opened okay, make sure we close it  
           if (dis != null) { 
              try { 
                 dis.close(); 
              } catch (IOException e) { 
                 System.out.println("IOException error trying to close the file: " + 
                                    e.getMessage());  
              } 

           } // end if 

        } // end finally 

     } // end dbTest 

  } // end class 

