{ { "Java2D  -  text on gradient", "1000" },
                 { new GpE(GpE.SDH, blue, black, 0, 20),
                   new GpE(GpE.SIH, blue, black, 21, 40),
                   new GpE(GpE.SDH, blue, black, 41, 50),
                   new GpE(GpE.INC | GpE.NF, red, yellow, 0, 50),
                   new TxE("Java2D", f2, TxE.NOP, null, 0, 50) }},
                   static class GpE implements Part 
                   {

            static final int INC = 1;             // increasing
            static final int DEC = 2;             // decreasing
            static final int CNT = 4;             // center
            static final int WID = 8;             // width 
            static final int WI  = WID | INC;             
            static final int WD  = WID | DEC;            
            static final int HEI = 16;            // height
            static final int HI  = HEI | INC;            
            static final int HD  = HEI | DEC;            
            static final int SPL = 32 | CNT;      // split 
            static final int SIW = SPL | INC | WID;
            static final int SDW = SPL | DEC | WID;
            static final int SIH = SPL | INC | HEI;
            static final int SDH = SPL | DEC | HEI;
            static final int BUR = 64 | CNT;     // burst 
            static final int BURI = BUR | INC;    
            static final int BURD = BUR | DEC;   
            static final int NF = 128;           // no fill
            private Color c1, c2;
            private int beginning, ending;
            private float incr, index;
            private Vector rect = new Vector();
            private Vector grad = new Vector();
            private int type;


            public GpE(int type, Color c1, Color c2, int beg, int end)
            {
                this.type = type;
                this.c1 = c1;
                this.c2 = c2;
                this.beginning = beg;
                this.ending = end;
            }


            public void reset(int w, int h)
            {
                incr = 1.0f / (ending - beginning);
                if ((type & CNT) != 0)
                {
                    incr /= 2.3f;
                }
                if ((type & CNT) != 0 && (type & INC) != 0)
                {
                    index = 0.5f;
                }
                else if ((type & DEC) != 0)
                {
                    index = 1.0f;
                    incr = -incr;
                }
                else
                {
                    index = 0.0f;
                }
                index += incr;
            }

       
            public void step(int w, int h)
            {
                rect.clear();
                grad.clear();

                if ((type & WID) != 0)
                {
                    float w2 = 0, x1 = 0, x2 = 0;
                    if ((type & SPL) != 0)
                    {
                        w2 = w * 0.5f;
                        x1 = w * (1.0f - index);
                        x2 = w * index;
                    } 
                    else
                    {
                        w2 = w * index;
                        x1 = x2 = w2;
                    }
                    rect.addElement(new Rectangle2D.Float(0, 0, w2, h));
                    rect.addElement(new Rectangle2D.Float(w2, 0, w-w2, h));
                    grad.addElement(new GradientPaint(0,0,c1,x1,0,c2));
                    grad.addElement(new GradientPaint(x2,0,c2,w,0,c1));
                } else if ((type & HEI) != 0)
                {
                    float h2 = 0, y1 = 0, y2 = 0;
                    if ((type & SPL) != 0)
                    {
                        h2 = h * 0.5f;
                        y1 = h * (1.0f - index);
                        y2 = h * index;
                    } else {
                        h2 = h * index;
                        y1 = y2 = h2;
                    }
                    rect.addElement(new Rectangle2D.Float(0, 0, w, h2));
                    rect.addElement(new Rectangle2D.Float(0, h2, w, h-h2));
                    grad.addElement(new GradientPaint(0,0,c1,0,y1,c2));
                    grad.addElement(new GradientPaint(0,y2,c2,0,h,c1));
                }
                else if ((type & BUR) != 0)
                {

                    float w2 = w/2;
                    float h2 = h/2;

                    rect.addElement(new Rectangle2D.Float(0, 0, w2, h2));
                    rect.addElement(new Rectangle2D.Float(w2, 0, w2, h2));
                    rect.addElement(new Rectangle2D.Float(0, h2, w2, h2));
                    rect.addElement(new Rectangle2D.Float(w2, h2, w2, h2));

                    float x1 = w * (1.0f - index);
                    float x2 = w * index;
                    float y1 = h * (1.0f - index);
                    float y2 = h * index;

                    grad.addElement(new GradientPaint(0,0,c1,x1,y1,c2));
                    grad.addElement(new GradientPaint(w,0,c1,x2,y1,c2));
                    grad.addElement(new GradientPaint(0,h,c1,x1,y2,c2));
                    grad.addElement(new GradientPaint(w,h,c1,x2,y2,c2));
                }
                else if ((type & NF) != 0)
                {
                    float x = w * index;
                    float y = h * index;
                    grad.addElement(new GradientPaint(0,0,c1,0,y,c2));
                }

                if ((type & INC) != 0 || (type & DEC) != 0)
                {
                    index += incr;
                }
            }