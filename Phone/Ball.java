
/**
 *
 * Ball.java
 * @author	Mark G. Tacchi (mtacchi@next.com) 
 * @version	1.0
 * Feb 23/1996
 *
 * A bouncy ball.
 *
 */

import java.lang.Math;
import java.awt.Image;
import com.next.gt.*;

public class Ball extends Actor {
  int			bounceCount;
  Image			theImage;
  int			MAX_VELOLCITY_X= 200;
  int			MAX_VELOLCITY_Y= 1200;
  
Ball(Gamelet theOwner) {
 
  owner= theOwner;
  x= (Math.random()*owner.size().width);
  y= (Math.random()*owner.size().height);
      velocity_x= (double)((int)owner.randBetween(0.5,1.5)*2 - 1) * Math.random()*MAX_VELOLCITY_X;
  velocity_y= Math.random()*MAX_VELOLCITY_Y;
  bounceCount= 0;

  theImage= owner.getImage(owner.getCodeBase(), "images/pingPong.gif");

  setImage (theImage, 4, 16);
  currentFrame= (int) owner.randBetween(0., (double)numFrames);
  wrapAround= false;
  
} /*Ball()*/



/**
 * Handle left rotation.
 */
public void calculateNewPosition() {
  super.calculateNewPosition();
  //
  // check for out of bounds and rebound
  //
  if (x > (owner.size().width - width)) {
      velocity_x= - velocity_x;
	  x= owner.size().width - width;
  }
  else if (x < 0) {
    velocity_x= - velocity_x;
	x= 0;
  }
  if (y > (owner.size().height - height)) {
    velocity_y= - velocity_y * 0.75;
	y= owner.size().height - height;
	if(bounceCount++ > 8) {		// give it a kick of energy after a while
	  bounceCount= 0;
	  velocity_y= -Math.random() * MAX_VELOLCITY_Y;
	} /*endif*/
  
  }
  else if (y < 0) {
    velocity_y= - velocity_y * 0.75;
	y= 0;
  }
  
  velocity_y+= 5; 

} /*calculateNewPosition*/


} /*Ball*/
