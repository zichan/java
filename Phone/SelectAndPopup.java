import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;
public class SelectAndPopup implements MouseListener
{
    private JPopupMenu popup;
    public SelectAndPopup(JPopupMenu popup)
    {
        this.popup = popup;
    }
    public void mousePressed(MouseEvent e)
    {
        if ( SwingUtilities.isRightMouseButton(e) )
        {
            try
            {
                Robot robot = new java.awt.Robot();
                robot.mousePress(InputEvent.BUTTON1_MASK);
                robot.mouseRelease(InputEvent.BUTTON1_MASK);
            }
            catch (AWTException ae) {}
        }
    }
    public void mouseReleased(MouseEvent e)
    {
        if (e.isPopupTrigger())
        {
            popup.show(e.getComponent(), e.getX(), e.getY());
        }
    }
    public void mouseClicked(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    public void mouseExited(MouseEvent e) {}
    public static void main(String[] args)
    {
        JPopupMenu popup = new JPopupMenu();
        JMenuItem i = new JMenuItem("Do Something1");
        i.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent a)
            {
                System.out.println("THR");
            }
        });
        popup.add(i );
        popup.add( new JMenuItem("Do Something2") );
        popup.add( new JMenuItem("Do Something3") );
        String[] data = { "zero", "one", "two", "three", "four", "five" };
        JList list = new JList( data );
        list.addMouseListener( new SelectAndPopup( popup ) );
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
        frame.getContentPane().add( new JScrollPane(list) );
        frame.setSize(400, 100);
        frame.setLocationRelativeTo( null );
        frame.setVisible( true );
    }
}