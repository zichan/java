/**
 * A Phonebook application
 * 
 * Jacob Cummings
 * 1.1
 */
import java.awt.event.KeyEvent;
import java.io.*;
import java.util.*;
import java.util.StringTokenizer.*;
import java.lang.Object.*;
import javax.swing.*;
import javax.swing.JFileChooser;
import javax.swing.JInternalFrame;
import javax.swing.JDesktopPane;
import javax.swing.JButton.*;
import javax.swing.JFrame;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import javax.swing.JPanel;
import java.awt.*;
import java.awt.event.*;
public class Phone extends JFrame implements ActionListener
{
    DefaultListModel Contacts = new DefaultListModel();
    String user;
    JDesktopPane Desktop;
    JInternalFrame selectNames, selcont, NContact;
    JList NameList;
    JButton mew, del, open, edit, save ,cancel;
    JLabel label[] = new JLabel[11];
    JTextField inp[];
    TextField enter;
    int locationx,locationy,sizex,sizey=0;
    JComboBox ST;
    Color DESKTOP = new Color(23,23,23);
    boolean OPENUSER;
    String EncrPass;
    static Phone fram;
    String[] States = {"ALABAMA","ALASKA","ARIZONA","ARKANSAS","CALIFORNIA", "COLORADO", "CONNECTICUT","DELAWARE","DISTRICT OF COLUMBIA","FLORIDA","GEORGIA","HAWAII",
        "IDAHO","ILLINOIS","INDIANA","IOWA","KANSAS","KENTUCKY","LOUISIANA","MAINE","MARYLAND","MASSACHUSETTS","MICHIGAN","MINNESOTA","MISSISSIPPI","MISSOURI","MONTANA",
        "NEBRASKA","NEVADA","NEW HAMPSHIRE","NEW JERSEY","NEW MEXICO","NEW YORK","NORTH CAROLINA","NORTH DAKOTA","OHIO","OKLAHOMA","OREGON","PENNSYLVANIA","RHODE ISLAND",
        "SOUTH CAROLINA","SOUTH DAKOTA","TENNESSEE","TEXAS","UTAH","VERMONT","VIRGINIA ","WEST VIRGINIA",
        "WISCONSIN","WYOMING"};
    public Phone()
    {
        try
        {
            Start();
        }
        catch(Exception t){
        System.out.println(t.getMessage());}
    }
    public boolean toBoolean(String txt)
    {
        return ((txt!=null)&&txt.toLowerCase().equals("true"));
    }
    public void LoadInfo()
    {
        BufferedReader dis;
        try 
        { 
            dis = new BufferedReader(new InputStreamReader(new FileInputStream(new File("Data/prgmdata.dll"))));
            String Names = dis.readLine();
            StringTokenizer dt = new StringTokenizer(Names,":");
            locationx= Integer.parseInt(dt.nextToken());
            locationy=Integer.parseInt(dt.nextToken());
            sizex=Integer.parseInt(dt.nextToken());
            sizey=Integer.parseInt(dt.nextToken());
            String usr = dt.nextToken();
            if(toBoolean(usr)==true)
            {
                OPENUSER=true;
                user=dt.nextToken();
            }
            if(toBoolean(usr)==false)
                OPENUSER=false;
            dis.close();
        }
        catch(FileNotFoundException e)
        {
            newUser();
        }
        catch(IOException r){}
    }
    public void GetUserInfo() 
    {
        BufferedReader dis;
        if(OPENUSER==true)
        {
                try
                {
                    dis = new BufferedReader(new InputStreamReader( new FileInputStream(new File(user,"contacts.dat"))));
                    String Names = dis.readLine();
                    StringTokenizer dt = new StringTokenizer(Names,":");
                    EncrPass=dt.nextToken();
                    while (dt.hasMoreTokens())
                    {
                        Contacts.addElement(dt.nextToken());
                    }
                    dis.close();
                }
                catch(Exception flem)
                {   
                    JOptionPane.showMessageDialog(null, "Error", "Your File Was Not Found",JOptionPane.INFORMATION_MESSAGE);
                    JFileChooser file = new JFileChooser();
                    file.setFileFilter(new type());
                    file.setAcceptAllFileFilterUsed(false);
                    file.showOpenDialog(null);
                }
            }
        if(OPENUSER==false)
        {
            final JDialog uSeRs = new JDialog(fram,true);
            uSeRs.setUndecorated(true);
            uSeRs.getContentPane().setLayout(new GridLayout(4,1));
            Panel fields = new Panel();
            Panel Text = new Panel();
            Panel PaSs = new Panel();
            Panel butt = new Panel();
            Text.setLayout(new FlowLayout());
            PaSs.setLayout(new FlowLayout());
            fields.setLayout(new FlowLayout());
            butt.setLayout(new FlowLayout());
            enter = new TextField(10);
            final JPasswordField pass = new JPasswordField(7);
            Label info = new Label("Enter your username and password");
            Label name = new Label("Enter your name");
            Label PASs = new Label("Enter your password");
            final JButton ok = new JButton("Ok");
            pass.addKeyListener(new KeyAdapter() 
            {
                public void keyPressed(KeyEvent e)
                {
                    int key = e.getKeyCode();
                    if (key == KeyEvent.VK_ENTER)
                    {
                        ok.doClick(0);
                    }
                }
            });
            ok.requestFocusInWindow();
            JButton cAncel = new JButton("Cancel");
            ok.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent rew)
                {
                    if (enter.getText()!=""||(pass.getPassword()).toString()!="")
                    {
                        try
                        {
                            BufferedReader dis = new BufferedReader(new InputStreamReader( new FileInputStream(new File(enter.getText()+"/"+"contacts.dat"))));
                            StringTokenizer dt = new StringTokenizer(dis.readLine(),":");
                            String PASS = new String(dt.nextToken());
                            System.out.println(PASS+checkPass(pass.getPassword()));
                            if(PASS.equals(checkPass(pass.getPassword())))
                            {
                                while(dt.hasMoreTokens())
                                {
                                    Contacts.addElement(dt.nextToken());
                                }
                                user=enter.getText();
                                EncrPass=checkPass(pass.getPassword());
                                uSeRs.dispose();
                            }
                            else
                            {
                                JOptionPane.showMessageDialog(uSeRs, "Error", "Fill out all fields",JOptionPane.ERROR_MESSAGE);
                            }
                            dis.close();
                        }
                        catch(FileNotFoundException fnfe)
                        {}
                        catch(IOException ioe)
                        {
                            JOptionPane.showMessageDialog(uSeRs, "Error", "Cannot Write The File",JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    if(enter.getText()==""&&(pass.getPassword()).toString()=="")
                    {
                        final JWindow r = new JWindow();
                        r.add(new Label("Plese Enter All Information"));
                        Button ko = new Button("Ok");
                        ko.addActionListener(new ActionListener()
                        {
                            public void actionPerformed(ActionEvent pws)
                            {
                                r.dispose();
                            }
                        });
                        r.getContentPane().add(ko);
                        r.show();
                    }
                }
            });
            cAncel.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent trea)
                {
                    System.exit(0);
                }
            });
            fields.add(info);
            Text.add(name);
            Text.add(enter);
            PaSs.add(PASs);
            PaSs.add(pass);
            butt.add(ok);
            butt.add(cAncel);
            uSeRs.getContentPane().add(fields);
            uSeRs.getContentPane().add(Text);
            uSeRs.getContentPane().add(PaSs);
            uSeRs.getContentPane().add(butt);
            uSeRs.pack();
            int pos1 = (sizey+locationx);
            int pos2 = (sizex+locationy);
            uSeRs.setLocation(pos1-(sizey/2)-(uSeRs.getSize().width/2),pos2-(sizex/2)-(uSeRs.getSize().height/2));
            uSeRs.setVisible(true);
        }
    }
    public void newUser()
    {
        final JDialog usr = new JDialog();
        usr.getContentPane().setLayout(new GridLayout(4,1));
        Panel fields = new Panel();
        Panel Text = new Panel();
        Panel PaSs = new Panel();
        Panel butt = new Panel();
        Text.setLayout(new FlowLayout());
        PaSs.setLayout(new GridLayout(2,2));
        fields.setLayout(new FlowLayout());
        butt.setLayout(new FlowLayout());
        final TextField enter = new TextField(10);
        final JPasswordField pass = new JPasswordField(7);
        final JPasswordField pass1 = new JPasswordField(7);
        Label info = new Label("Enter your Desired username and password");
        Label name = new Label("Enter your name");
        Label PASs = new Label("Enter your password");
        Label PASs1 = new Label("Enter your password again");
        Button ok = new Button("Ok");
        Button cAncel = new Button("Cancel");
        ok.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent rew)
            {
                File f = new File(enter.getText()+"/"+"contacts.dat");
                if(f.exists())
                {
                    JOptionPane.showMessageDialog(usr, "Error", "User Exists",JOptionPane.ERROR_MESSAGE);
                }
                if(!f.exists())
                {
                    if(pass.getPassword()==pass1.getPassword());
                    {
                        try
                        {
                            File ut = new File(enter.getText()+"/");
                            ut.mkdirs();
                            PrintStream os = new PrintStream(new FileOutputStream(f));
                            os.println(checkPass(pass.getPassword())+":");
                            os.close();
                            usr.dispose();
                        }
                        catch(Exception hthds){System.out.println(hthds.getMessage());}
                    }
                }
             }
        });
        cAncel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent trea)
            {
                usr.dispose();
            }
        });
        fields.add(info);
        Text.add(name);
        Text.add(enter);
        PaSs.add(PASs);
        PaSs.add(pass);
        PaSs.add(PASs1);
        PaSs.add(pass1);
        butt.add(ok);
        butt.add(cAncel);
        usr.getContentPane().add(fields);
        usr.getContentPane().add(Text);
        usr.getContentPane().add(PaSs);
        usr.getContentPane().add(butt);
        usr.pack();
        int pos1 = (sizey+locationx);
        int pos2 = (sizex+locationy);
        usr.setLocation(pos1-(sizey/2)-(usr.getSize().width/2),pos2-(sizex/2)-(usr.getSize().height/2));
        usr.show();
    }
    public String checkPass(char[] o)
    {
        String pswrd = new String();
        String p = new String(o);
        for (int i=0;i<=p.length()-1;i++)
        {
            if((p.substring(i,i+1)).equals("a"))
                pswrd = pswrd.concat("0-+8?0?���?+b???=ASSm��-???w?�?>? ???��A??U8???8[Y�???D�???^Y?�?�???N");
            if((p.substring(i,i+1)).equals("b"))
                pswrd = pswrd.concat("???????k?[N?0???k?[N+_6???0-+8?0?���?+b?a??VN*�?????�!�??$?�???nN?��ɕz�?????????6??�");
            if((p.substring(i,i+1)).equals("c"))
                pswrd = pswrd.concat("(?���?+b?ak?[N?0???�!�+_0?���?+b?+8????????6??�??$?�???nN?��ɕz�?");
            if((p.substring(i,i+1)).equals("d"))
                pswrd = pswrd.concat("?????k?[??k?[??$????????k?[??$?�b?+8�!�+_0?���?+");
            if((p.substring(i,i+1)).equals("e"))
                pswrd = pswrd.concat("?????k?[N?0?���?+b?ak?[???????0???�!�+_0?���?+b?8?1?12?4462???????6??�");
            if((p.substring(i,i+1)).equals("f"))
                pswrd = pswrd.concat("�?K�am�????�7&(��?+b???=AS+_0??k?[N+_6???0-+8??N?�???nN?��ɕ�?>? ???��A??U8???8");
            if((p.substring(i,i+1)).equals("g"))
                pswrd = pswrd.concat("???b6�??�??�????�!�+_0??k?[N+_6???0-+8??N?�???nN?��ɕz�?c�?????????6??��?6N8??68DYA8bWV�z");
            if((p.substring(i,i+1)).equals("h"))
                pswrd = pswrd.concat("?-a�8YW2`X?N?0???�!�?b6�??�??_?=AS??U??8??0?���?+b?b???�?t?+8[Y�???Sm��-???w?�?>? ???��A");
            if((p.substring(i,i+1)).equals("i"))
                pswrd = pswrd.concat("a�8YW2`X?$?�???�!�??VN*2t?446284o?��A??U8?? ???��A??$?�???�!�?b6�??�?�!�???_?=[N?0?���???nN?���");
            if((p.substring(i,i+1)).equals("j"))
                pswrd = pswrd.concat("[??k?[�?????�!�??$?�???�!�??48�/0????8?8?1?12t?446284o?��A??U8?? ???��A??U8???????b???�?&?+8[Y�???Sm��-???");
            if((p.substring(i,i+1)).equals("k"))
                pswrd = pswrd.concat("k??k?[N?0???k?SN�!��????6???0-+8??��?+b???=AS�?>? ???��A??U8ak?[N?0?S??U???");
            if((p.substring(i,i+1)).equals("l"))
                pswrd = pswrd.concat("??�??A??-?�??��?+b???=ASSm��-???@???��A??U8??�-???@??��-a�8Y???=ASSm��-???��?+b???=ASSm�?@??��-a�8Y>? ???��A??U8???k?");
            if((p.substring(i,i+1)).equals("m"))
                pswrd = pswrd.concat("��?+b?��A??U8??�-??!�+_0?�AS??U??8??0?���?+b???�7&(��?+b???=ASSm��-???@??��{????�?>?+???");
            if((p.substring(i,i+1)).equals("n"))
                pswrd = pswrd.concat("??�!�?b6�???���?+Sm?0?���?+b?ak?[N?0???????�?>? ?��A??U8??�-?????��A??@??��{????�");
            if((p.substring(i,i+1)).equals("o"))
                pswrd = pswrd.concat("?b6�???��?+b?ak?[N?0???!��????6?�+_0?���?+b?U??8?0??�!�?b6�???���?+Sm?0?���?+b?ak?[N?0???????�?>? ?��?b6�??�??�????�!�+_0??k?[N+_6???0-+8??N?�???nN?��ɕz�?c�?????????6?A??U8??�-?????��A??@??��{??");
            if((p.substring(i,i+1)).equals("p"))
                pswrd = pswrd.concat("??�7&(��?+b???=ASSm��-???@??��{????�??6?�????6???0-+8?0??�");
            if((p.substring(i,i+1)).equals("q"))
                pswrd = pswrd.concat("???b6�??�??�?&??-a�8YW2`X?WA??-?�?????k?[N+_6???0�/0?8?8?1?12&?446284o1,9?512�47560?��A??U8??�-???@??��-a�8Y??551??48�/0?8?8?1?12&?446284o1,9?512�47560?5�?c�?????????6??��?6N8??68DYA8bWV�z");
            if((p.substring(i,i+1)).equals("r"))
                pswrd = pswrd.concat("?b6�??�??�?&??-a�8YW2`X?WA??-?�???��{????�?>? ???��A=ASSm��-???@??��8?");
            if((p.substring(i,i+1)).equals("s"))
                pswrd = pswrd.concat("�!�+_0??k?[N+N*�?????�!�??$?�???�!�?b6�??A??-?�??��?+b???=ASSm��-???@???��A??U8!�???_?0-+8??N?");
            if((p.substring(i,i+1)).equals("t"))
                pswrd = pswrd.concat("?0???�!�?b6�??�??_?=AS??U??�{????�??6?�????6?0?���?+b?b??");
            if((p.substring(i,i+1)).equals("u"))
                pswrd = pswrd.concat("???0-+{???ak?[N?08?{???ak?[N?0?S???????@??��{????�?>??@??��?�?>???U?");
            if((p.substring(i,i+1)).equals("v"))
                pswrd = pswrd.concat("?��{????�??8?8?1?12adsf#yth?44>?6???0?0?S??!�?b6+_0?���?+b?+8???????{????�?��????6?+8??��?");
            if((p.substring(i,i+1)).equals("w"))
                pswrd = pswrd.concat("�????��?????SSm��-???@�?(��?+b???=��8YW2`X?WA???=A???��A??U8?? ???��A??Sm��-?");
            if((p.substring(i,i+1)).equals("x"))
                pswrd = pswrd.concat("?[N?3240???!��2????62?�234+_0?���24?+b?U?4??��A??U8????���?+Sm?0?���?+b?ak?[N?0???????�?>? ?��?b6�??�??�????�!�+_0??k?�2????�?b");
            if((p.substring(i,i+1)).equals("y"))
                pswrd = pswrd.concat("?+b???=ASSm��-????8?b6�??U8?? ???��A?0??�!�?6284o?��A?U8?? ?????6?�????6A?0??�!�??���?+Smadsf#yth?44��{????�?�????");
            if((p.substring(i,i+1)).equals("z"))
                pswrd = pswrd.concat("??=ASak?[???��???��A??U80-+8??N?N?0???????�?>? ?A??U8?? ?????��A??@??��{??�?�????�!�-???@�???n???=ASSm��-??");
        }
        System.out.println(pswrd);
        return pswrd;
    }
    public void createCont()
    {
        inp = new JTextField[11];
        for(int i=1;i<=3;i++)
        {
            inp[i] = new JTextField(15);
        }
        inp[4] = new JTextField(20);
        inp[5] = new JTextField(5);
        inp[6] = new JTextField(10);
        inp[7] = new JTextField(10);
        inp[8] = new JTextField(10);
        inp[9] = new JTextField(10);
        inp[10] = new JTextField(10);
        save = new JButton("Save");
        save.setActionCommand("Save");
        save.addActionListener(this);
        cancel = new JButton("Cancel");
        cancel.setActionCommand("Cancel");
        cancel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent u)
            {
                try
                {
                    NContact.setClosed(true);
                }
                catch(Exception i){}
            }
        });
        ST = new JComboBox(States);
        NContact = new JInternalFrame("",true,true,false,true);
        NContact.getContentPane().setLayout(new BorderLayout());
        JPanel infoPane = new JPanel();
        infoPane.setLayout(new GridLayout(5,4));
        for (int i=1;i<=4;i++)
        {
            infoPane.add(label[i]);
            infoPane.add(inp[i]);
        }
        infoPane.add(label[5]);
        infoPane.add(ST);
        for(int i=6;i<=10;i++)
        {
            infoPane.add(label[i]);
            infoPane.add(inp[i]);
        }
        JPanel Bpanel = new JPanel();
        Bpanel.setLayout(new FlowLayout());
        Bpanel.add(save);
        Bpanel.add(cancel);
        NContact.getContentPane().add(infoPane,BorderLayout.NORTH);
        NContact.getContentPane().add(Bpanel,BorderLayout.SOUTH);
        NContact.setVisible(true);
        NContact.setLocation(132,1);
        NContact.setSize(600-140,565);
        Desktop.add(NContact);
        try
        {
            NContact.setSelected(true);
        }
        catch(java.beans.PropertyVetoException o){System.out.println(o.getMessage());}
        NContact.pack();
    }
    public void CreateLabels()
    {
        label = new JLabel[11];
        label[1] = new JLabel("First Name:");
        label[2] = new JLabel("Last Name:");
        label[3] = new JLabel("Address:");
        label[4] = new JLabel("City:");
        label[5] = new JLabel("State:");
        label[6] = new JLabel("Email:");
        label[7] = new JLabel("Zip Code:");
        label[8] = new JLabel("Home #:");
        label[9] = new JLabel("Work #:");
        label[10] = new JLabel("Cell #:");
    }
    public void openPrefs()
    {
        final JColorChooser dco = new JColorChooser();
        final Dialog f = new Dialog(fram,"Prefrences",true);
        Panel INFOPANE = new Panel();
        Panel DCOLOR = new Panel();
        Panel ButtonPanel = new Panel();
        Panel SavelastUser = new Panel();
        f.setLayout(new FlowLayout());
        DCOLOR.setLayout(new FlowLayout());
        ButtonPanel.setLayout(new FlowLayout());
        SavelastUser.setLayout(new FlowLayout());
        INFOPANE.setLayout(new GridLayout(6,1,5,5));
        JLabel dcolor = new JLabel("Desktop Color: Click To Change");
        final Canvas DColor = new Canvas();
        JButton ok = new JButton("Ok");
        final JButton cancel = new JButton("Cancel");
        final JButton apply = new JButton("Apply");
        final JCheckBox sghin = new JCheckBox("Sign In Automaticly",OPENUSER);
        DColor.setSize(30,15);
        DColor.setBackground(Color.blue);
        ok.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent rew)
            {
                OPENUSER=sghin.isSelected();
                if(apply.isEnabled())
                    apply.doClick();
                f.dispose();
            }
        });
        cancel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent yuir)
            {
                f.dispose();
            }
        });
        apply.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent tyye)
            {
                apply.setEnabled(false);
            }
        });
        DColor.addMouseListener(new MouseAdapter()
        {
            JDialog t = dco.createDialog(f, "Dialog Title",true,dco, null, null);
            public void mouseClicked(MouseEvent e)
            {
                t.setVisible(true);
                DColor.setBackground(dco.getColor());
                if(!apply.isEnabled())
                    apply.setEnabled(true);
            }
        });
        DCOLOR.add(dcolor);
        DCOLOR.add(DColor);
        INFOPANE.add(DCOLOR);
        INFOPANE.add(sghin);
        ButtonPanel.add(ok);
        ButtonPanel.add(cancel);
        ButtonPanel.add(apply);
        f.add(INFOPANE,BorderLayout.CENTER);
        f.add(ButtonPanel,BorderLayout.SOUTH);
        f.setVisible(true);
        f.pack();
        f.addWindowListener(new WindowAdapter()
        {
            public void windowClosing(WindowEvent e)
            {
                f.dispose();
                fram.repaint();
            }
        });
    }
    public void Start() throws IOException
    {
        LoadInfo();
        CreateLabels();
        GetUserInfo();
        final JPopupMenu popup = new JPopupMenu();
        JMenuItem i = new JMenuItem("Delete");
        i.addActionListener(this);
        i.setActionCommand("delete");
        JMenuItem opn = new JMenuItem("Open");
        opn.addActionListener(this);
        opn.setActionCommand("Open");
        JMenuItem edt = new JMenuItem("Edit");
        edt.addActionListener(this);
        edt.setActionCommand("Edit");
        JMenuItem New = new JMenuItem("New");
        New.addActionListener(this);
        New.setActionCommand("new");
        popup.add(opn);
        popup.add(edt);
        popup.add(New);
        popup.addSeparator();
        popup.add(i);
        final JPopupMenu Deskpopup = new JPopupMenu();
        JMenuItem pref = new JMenuItem("Prefrences");
        pref.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ew)
            {
                openPrefs();
            }
        });
        JMenuItem Vew = new JMenuItem("New user");
        Vew.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent ew)
            {
                newUser();
            }
        });
        Deskpopup.add(pref);
        Deskpopup.add(Vew);
        Desktop = new JDesktopPane();
        Desktop.putClientProperty("JDesktopPane.dragMode", "live");
        JPanel BPanel = new JPanel();
        selectNames = new JInternalFrame("Contacts",false,false,false,false);
        selectNames.getContentPane().setLayout(new BorderLayout());
        selectNames.setVisible(true);
        selectNames.setSize(130,300);
        setSize(sizey,sizex);
        setLocation(locationx,locationy);
        selectNames.setLocation(1,1);
        selectNames.getContentPane().add(BPanel, BorderLayout.SOUTH);
        this.setContentPane(Desktop);
        Desktop.add(selectNames);
        NameList = new JList(Contacts)
        {
            public int getScrollableUnitIncrement(Rectangle visibleRect,int orientation,int direction)
            {
                int row;
                if (orientation == SwingConstants.VERTICAL && direction < 0 && (row = getFirstVisibleIndex()) != -1)
                {
                    Rectangle r = getCellBounds(row, row);
                    if ((r.y == visibleRect.y) && (row != 0))
                    {
                        Point loc = r.getLocation();
                        loc.y--;
                        int prevIndex = locationToIndex(loc);
                        Rectangle prevR = getCellBounds(prevIndex, prevIndex);
                        if (prevR == null || prevR.y >= r.y)
                        {
                            return 0;
                        }
                        return prevR.height;
                    }
                }
                return super.getScrollableUnitIncrement(visibleRect, orientation, direction);
            }
        };
        NameList.addMouseListener(new MouseAdapter()
        {
            public void mouseClicked(MouseEvent e)
            {
                if (e.getClickCount() == 2)
                {
                    try{getContact();}
                    catch(Exception l){}
                }
            }
            public void mousePressed(MouseEvent e)
            {
                if ( SwingUtilities.isRightMouseButton(e) )
                {
                    try
                    {
                        Robot robot = new java.awt.Robot();
                        robot.mousePress(InputEvent.BUTTON1_MASK);
                        robot.mouseRelease(InputEvent.BUTTON1_MASK);
                    }
                    catch (AWTException ae) {}
                }
            }
 
            public void mouseReleased(MouseEvent e)
            {
                if ( SwingUtilities.isRightMouseButton(e) )
                {
                    popup.show(e.getComponent(), e.getX(), e.getY());
                }
            }

        });
        Desktop.addMouseListener(new MouseAdapter()
        {
            public void mousePressed(MouseEvent e)
            {
                if ( SwingUtilities.isRightMouseButton(e) )
                {
                    try
                    {
                        Robot robot = new java.awt.Robot();
                        robot.mousePress(InputEvent.BUTTON1_MASK);
                        robot.mouseRelease(InputEvent.BUTTON1_MASK);
                    }
                    catch (AWTException ae) {}
                }
            }
 
            public void mouseReleased(MouseEvent e)
            {
                if ( SwingUtilities.isRightMouseButton(e) )
                {
                    Deskpopup.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        JScrollPane lirst = new JScrollPane(NameList);
        selectNames.getContentPane().add(lirst, BorderLayout.CENTER);
        addWindowListener(
                             new WindowAdapter() {
                                public void windowClosing(WindowEvent e) {
                                    quit(getLocation().x,getLocation().y,getSize().height,getSize().width);
                                }});
    }
    int numOfTok;
    JLabel[] Labels = new JLabel[9];
    public void getContact() throws IOException
    {
        CreateLabels();
        inp = new JTextField[11];
        for(int i=1;i<=2;i++)
        {
            inp[i] = new JTextField(15);
        }
        inp[3] = new JTextField(20);
        inp[4] = new JTextField(5);
        inp[5] = new JTextField(10);
        inp[6] = new JTextField(10);
        inp[7] = new JTextField(10);
        inp[8] = new JTextField(10);
        inp[9] = new JTextField(10);
        inp[10] = new JTextField(10);
        BufferedReader dis;
        String FileName = (String)NameList.getSelectedValue();
        File f = new File(user,FileName+".cnt");
        try
        {
            FileInputStream fis = new FileInputStream(f);  
            InputStreamReader bis = new InputStreamReader(fis);  
            dis = new BufferedReader(bis);
            String Userdat = dis.readLine();
            StringTokenizer dt = new StringTokenizer(Userdat,":");
            int i=1;
            numOfTok = dt.countTokens();
            while(dt.hasMoreTokens())
            {
                inp[i].setText(dt.nextToken());
                inp[i].setEditable(false);
                i=i+1;
            }
            if(i<10)
            {
                while(i<=10)
                {
                    inp[i].setText("none");
                    inp[i].setEditable(false);
                    i++;
                }
            }
            dis.close();
            bis.close();
            fis.close();
        }
        catch(FileNotFoundException m)
        {
            JOptionPane.showMessageDialog(null, "Not Found", "Contact was not found",JOptionPane.ERROR_MESSAGE);
        }
        selcont = new JInternalFrame(FileName,false,true,false,true);
        JPanel j= new JPanel();
        selcont.getContentPane().setLayout(new BorderLayout());
        j.setLayout(new GridLayout(4,4));
        selcont.setVisible(true);
        selcont.setLocation(132,1);
        selcont.setSize(600-140,565);
        Desktop.add(selcont);
        for(int i=1;i<=10;i++)
        {
            j.add(label[i]);
            j.add(inp[i]);
        }
        selcont.getContentPane().add(j,BorderLayout.NORTH);
    }
    public void editContact()
    {
        CreateLabels();
        inp = new JTextField[11];
        for(int i=1;i<=3;i++)
        {
            inp[i] = new JTextField(15);
        }
        inp[4] = new JTextField(20);
        inp[5] = new JTextField(5);
        inp[6] = new JTextField(10);
        inp[7] = new JTextField(10);
        inp[8] = new JTextField(10);
        inp[9] = new JTextField(10);
        inp[10] = new JTextField(10);
        final JButton Cancel = new JButton("Cancel");
        JButton Save = new JButton("Save");
        Save.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent save)
            {
                if(NameList.getSelectedValue() != inp[1])
                {
                    Contacts.removeElement(NameList.getSelectedValue());
                    Contacts.addElement(inp[1].getText());
                }
                try
                {
                    File f = new File(user,inp[1].getText()+".cnt");
                    PrintStream os = new PrintStream(new FileOutputStream(f));
                    for(int i=1;i<=4;i++)
                    {
                        os.print(inp[i].getText() + ":");
                    }
                    os.print(ST.getSelectedItem() + ":");
                    for(int i=6;i<=10;i++) 
                    {
                        if(inp[i].getText()=="")
                        {
                            os.print("NONE"+":");
                        }
                        else
                            os.print(inp[i].getText()+":");
                    }
                    Cancel.doClick();
                    os.close();
                }
                catch(IOException q)
                {
                    
                }
            }
        });
        BufferedReader dis;
        String FileName = (String)NameList.getSelectedValue();
        try
        {
            dis = new BufferedReader(new InputStreamReader(new FileInputStream(new File(user,FileName+".cnt"))));
            String Userdat = dis.readLine();
            StringTokenizer dt = new StringTokenizer(Userdat,":");
            int i=1;
            numOfTok = dt.countTokens();
            while(dt.hasMoreTokens())
            {
                inp[i].setText(dt.nextToken());
                i=i+1;
            }
            dis.close();
        }
        catch(Exception h){}
        final JInternalFrame EDIT = new JInternalFrame(FileName,false,true,false,true);
        Cancel.addActionListener(new ActionListener()
        {
            public void actionPerformed(ActionEvent oiu)
            {
                try
                {
                    EDIT.setClosed(true);
                }
                catch(Exception k){}
            }
        });
        ST = new JComboBox(States);
        JPanel j= new JPanel();
        JPanel but = new JPanel();
        but.setLayout(new FlowLayout());
        EDIT.getContentPane().setLayout(new BorderLayout());
        j.setLayout(new GridLayout(5,4));
        EDIT.setVisible(true);
        EDIT.setLocation(132,1);
        EDIT.setSize(600-140,300);
        Desktop.add(EDIT);
        EDIT.getContentPane().add(j,BorderLayout.NORTH);
        for (int i=1;i<=4;i++)
        {
            j.add(label[i]);
            j.add(inp[i]);
        }
        j.add(label[5]);
        j.add(ST);
        ST.setSelectedItem(inp[5].getText());
        for(int i=6;i<=10;i++)
        {
            j.add(label[i]);
            j.add(inp[i]);
        }
        but.add(Save);
        but.add(Cancel);
        EDIT.getContentPane().add(but,BorderLayout.SOUTH);
    }
    public void actionPerformed(ActionEvent e)
    {
        if (e.getActionCommand()=="new")
        {
            createCont();
        }
        if (e.getActionCommand()=="delete")
        {
            String FileName = (String)NameList.getSelectedValue();
            Contacts.remove(NameList.getSelectedIndex());
            File t = new File(user,FileName+".cnt");
            boolean deleted = t.delete();
            if(deleted)
            {
            }
        }
        try
        {
            if(e.getActionCommand()=="Open")
            {
                getContact();
            }
        }
        catch (Exception n){System.out.println(n.getMessage());}
        if(e.getActionCommand()=="Edit")
        {
            editContact();
        }
        if(e.getActionCommand()=="Save")
        {
            Contacts.addElement(inp[1].getText());
            try
            {
                File f = new File(user,inp[1].getText()+".cnt");
                PrintStream os = new PrintStream(new FileOutputStream(f));
                for(int i=1;i<=4;i++)
                {
                    os.print(inp[i].getText() + ":");
                }
                os.print(ST.getSelectedItem() + ":");
                for(int i=5;i<=10;i++)
                {
                    if(inp[i].getText()=="")
                    {
                        os.print("null:");
                    }
                    else
                        os.print(inp[i].getText()+":");
                }
                cancel.doClick();
                os.close();
            }
            catch(IOException q){}
            try
            {
                PrintStream os = new PrintStream(new FileOutputStream(new File(user,"contacts.dat")));
                os.print(EncrPass+":");
                System.out.println(Contacts.getSize());
                for (int i=0;i<Contacts.getSize();i++)
                {
                    os.print((String)Contacts.get(i)+":");
                }
            }
            catch(Exception d)
            {
            }
        }
    }
    protected void quit(int locationx,int locationy,int sizex,int sizey)
    {
        File f = new File("Data/prgmdata.dll");
        try
        {
            PrintStream os = new PrintStream(new FileOutputStream(f));
            os.print(locationx+":");
            os.print(locationy+":");
            os.print(sizex+":");
            os.print(sizey+":");
            os.print(OPENUSER+":");
            os.print(user+":");
        }
        catch(Exception i)
        {
        }
        try
        {
            PrintStream os = new PrintStream(new FileOutputStream(new File(user,"contacts.dat")));
            os.print(EncrPass+":");
            for (int i=0;i<Contacts.getSize();i++)
            {
                os.print((String)Contacts.get(i)+":");
            }
            os.close();
        }
        catch(Exception d)
        {
        }
    }
    public static void main(String[] args)
    {
        LookAndFeel lf = UIManager.getLookAndFeel();
        try
        {
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        }
        catch (Exception b){}
        fram = new Phone();
        fram.setVisible(true);
    }
}