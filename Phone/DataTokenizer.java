   import java.util.StringTokenizer;
   class DataTokenizer extends StringTokenizer
   {
      DataTokenizer(String st)
      {
         super(st, "~");
      }
      public String nextToken()
      {
         String tmp = super.nextToken();
         if(tmp.equals("*")) return "";
         else return tmp.substring(1);
      }
   }