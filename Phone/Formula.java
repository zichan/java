import java.util.*;
/**
 * 
 * Formula contains the properties and behavior of meld 
 * formulas, specifically the transformation from Epistemological
 * Level to Heuristic Level.
 * 
 */

public class Formula
{
    private LO formula = JavaLisp.nil;
    private Queue formulaVariables = new Queue();
    private Queue universalVariables = new Queue();
    private Queue existentialVariables = new Queue();   
   
    public static Symbol cardinalitySymbol;
    public static Symbol greaterThanOrEqualToSymbol;
    public static Symbol cardinalitySetVariableSymbol;
    public static Symbol cardinalityCountVariableSymbol;
   
    // Construct a new formula instance with nil.
    public Formula()
    {
    }
   
    // Construct a new formula instance from the argument, a formula.
    // The second, third arguments are for skolemize.
    public Formula( LO aFormula, 
		    Queue someUniversalVariables,
		    Queue someExistentialVariables )
    {
	formula = aFormula;
	universalVariables = someUniversalVariables;
	existentialVariables = someExistentialVariables;
    }
   
    // Construct a new formula instance from the argument, a formula.
    // The second argument is for renameVariables.
    public Formula( LO aFormula, Queue someFormulaVariables )
    {
	formula = aFormula;
	formulaVariables = someFormulaVariables;
    }
   
    // Construct a new formula instance from the argument, a formula.
    public Formula( LO aFormula )
    {
	formula = aFormula;
    }
   
    public void formula( LO aFormula )
    {
	formula = aFormula;
    }
   
    public LO formula()
    {
	return formula;
    }
   
    public String toString()
    {
	return formula.toString();
    }
   
    public static void init()
	throws ErrorException,
	       JumpException
    {
	// Meld constants.
	cardinalitySymbol = ( Symbol ) Constant.create_constant( "#$cardinality" );
	greaterThanOrEqualToSymbol = ( Symbol ) Constant.create_constant( "#$greaterThanOrEqualTo" );
      
	// Meld variables
	cardinalitySetVariableSymbol = JavaLisp.makeConstantSymbol( "?SET" );
	cardinalityCountVariableSymbol = JavaLisp.makeConstantSymbol( "?COUNT" );
    }
   
    /**
     * 
     * convertToCnf
     * 
     * Return a Conjunctive Normal Form of the argument, a formula.  The result
     * is a list of clauses with an implied outermost #$and.
     * 
     * Per Artificial Intelligence, A Modern Approach, Russell & Norvig (1995)
     * 
     */
    public static LO convertToCnf( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).convertToCnf();
    }
   
    public LO convertToCnf()
	throws ErrorException,
	       JumpException
    {
	formula = eliminateThereExistExactly();   
	formula = eliminateThereExistAtMost();
	formula = eliminateThereExistAtLeast();
	//JavaLisp.pprint( formula );
	formula = eliminateImplies();
	formula = eliminateXor();
	formula = eliminateEquivalent();
	//JavaLisp.pprint( formula );
	formula = moveNotInwards();
	//JavaLisp.pprint( formula );
	formula = quantifyFreeVariables();
	//JavaLisp.pprint( formula );
	formula = renameVariables();
	// Disable below to improve skolemization.
	//formula = moveQuantifiersLeft();
	formula = skolemize();
	formula = dropUniversalQualifiers();
	formula = binaryAndOr();
	//JavaLisp.pprint( formula );
	formula = distributeOrOverAnd();
	formula = flattenNestedOr();
	formula = flattenNestedAnd();
	formula = ensureAnd();
	//JavaLisp.pprint( formula );
	verifyWellFormedCnf();
      
	return formula;
    }

    /**
     * 
     * eliminateThereExistExactly
     * 
     * Return a logical expression substituting for #$thereExistExactly,
     * using #$forAll, #$TheSetOf and #$cardinality.
     * 
     * 
     * (#$thereExistExactly 3 ?X (#$P ?X))
     * 
     * returned as
     * 
     * (#$thereExists ?S
     *  (#$forAll ?X
     *   (#$and
     *    (#$P ?X)
     *    (#$isa ?S #$SetOrCollection)
     *    (#$isa ?X #$S)
     *    (#$cardinality #$S 3))))
     * 
     */
    public static LO eliminateThereExistExactly( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateThereExistExactly();
    }
   
    public LO eliminateThereExistExactly()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalThereExistExactly ))
	    {
		LO number = Cons.second( formula );
		LO variable = Cons.third( formula );
		LO body = Cons.fourth( formula );
         
		formula = Cons.list( Clause.logicalThereExists,
				     cardinalitySetVariableSymbol,
				     Cons.list( Clause.logicalForAll,
						variable,
						Cons.list( Clause.logicalAnd,
							   Cons.list( body,
								      Cons.list( IsaSupport.isaSymbol,
										 cardinalitySetVariableSymbol,
										 ElSupport.setOrCollectionSymbol ),
								      Cons.list( IsaSupport.isaSymbol,
										 variable,
										 cardinalitySetVariableSymbol ),
								      Cons.list ( cardinalitySymbol,
										  cardinalitySetVariableSymbol,
										  number )))));
	    }
      
	// Recurse on the elements of formula.
	return Cons.cons( eliminateThereExistExactly( Cons.car( formula )),
			  eliminateThereExistExactly( Cons.cdr( formula )));
    }

    /**
     * 
     * eliminateThereExistAtMost
     * 
     * Return a logical expression substituting for #$thereExistAtMost,
     * using #$thereExists, #$forAll, #$TheSetOf and #$cardinality.
     * 
     * (#$thereExistAtMost 1 ?X (#$P ?X))
     * 
     * returned as
     * 
     * (#$thereExists ?S
     *  (#$thereExists ?C
     *   (#$forAll ?X
     *    (#$and
     *     (#$P ?X)
     *     (#$isa ?S #$SetOrCollection)
     *     (#$isa ?X #$S)
     *     (#$cardinality #$S ?C)
     *     (#$greaterThanOrEqualTo 1 ?C)))))
     * 
     */
    public static LO eliminateThereExistAtMost( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateThereExistAtMost();
    }
   
    public LO eliminateThereExistAtMost()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalThereExistAtMost ))
	    {
		LO number = Cons.second( formula );
		LO variable = Cons.third( formula );
		LO body = Cons.fourth( formula );

		formula = Cons.list( 
				    Clause.logicalThereExists,
				    cardinalitySetVariableSymbol,
				    Cons.list( 
					      Clause.logicalThereExists,
					      cardinalityCountVariableSymbol,
					      Cons.list (
							 Clause.logicalForAll,
							 variable,
							 Cons.list( 
								   Clause.logicalAnd,
								   body,
								   Cons.list( 
									     IsaSupport.isaSymbol,
									     cardinalitySetVariableSymbol,
									     ElSupport.setOrCollectionSymbol ),
								   Cons.list( 
									     IsaSupport.isaSymbol,
									     variable,
									     cardinalitySetVariableSymbol ),
								   Cons.list( 
									     cardinalitySymbol,
									     cardinalitySetVariableSymbol,
									     cardinalityCountVariableSymbol ),
								   Cons.list( 
									     greaterThanOrEqualToSymbol,
									     number,
									     cardinalityCountVariableSymbol )))));
	    }
      
	// Recurse on the elements of formula.
	return Cons.cons( eliminateThereExistAtMost( Cons.car( formula )),
			  eliminateThereExistAtMost( Cons.cdr( formula )));
    }

    /**
     * 
     * eliminateThereExistAtLeast
     * 
     * Return a logical expression substituting for #$thereExistAtLeast,
     * using #$thereExists, #$forAll, #$TheSetOf and #$cardinality.
     * 
     * (#$thereExistAtLeast 3 ?X (#$P ?X))
     * 
     * returned as
     * 
     * (#$thereExists ?S
     *  (#$thereExists ?C
     *   (#$forAll ?X
     *    (#$and
     *     (#$P ?X)
     *     (#$isa ?S #$SetOrCollection)
     *     (#$isa ?X #$S)
     *     (#$cardinality #$S ?C)
     *     (#$greaterThanOrEqualTo ?C 3)))))
     * 
     */
    public static LO eliminateThereExistAtLeast( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateThereExistAtLeast();
    }
   
    public LO eliminateThereExistAtLeast()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalThereExistAtLeast ))
	    {
		LO number = Cons.second( formula );
		LO variable = Cons.third( formula );
		LO body = Cons.fourth( formula );
         
		formula = Cons.list( 
				    Clause.logicalThereExists,
				    cardinalitySetVariableSymbol,
				    Cons.list( 
					      Clause.logicalThereExists,
					      cardinalityCountVariableSymbol,
					      Cons.list (
							 Clause.logicalForAll,
							 variable,
							 Cons.list( 
								   Clause.logicalAnd,
								   body,
								   Cons.list( 
									     IsaSupport.isaSymbol,
									     cardinalitySetVariableSymbol,
									     ElSupport.setOrCollectionSymbol ),
								   Cons.list( 
									     IsaSupport.isaSymbol,
									     variable,
									     cardinalitySetVariableSymbol ),
								   Cons.list( 
									     cardinalitySymbol,
									     cardinalitySetVariableSymbol,
									     cardinalityCountVariableSymbol ),
								   Cons.list( 
									     greaterThanOrEqualToSymbol,
									     cardinalityCountVariableSymbol,
									     number )))));
	    }
      
	// Recurse on the elements of formula.
	return Cons.cons( eliminateThereExistAtLeast( Cons.car( formula )),
			  eliminateThereExistAtLeast( Cons.cdr( formula )));
    }

    /**
     * 
     * eliminateImplies
     * 
     * Return a logical expression substituting for #$implies.
     * 
     * (#$implies (#$P ?X) (#$Q ?X))
     * 
     * returned as
     * 
     * (#$or 
     *  (#$not 
     *   (#$P ?X))
     *  (#$Q ?X))
     * 
     */
    public static LO eliminateImplies( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateImplies();
    }
   
    public LO eliminateImplies()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalImplies ))
	    {
		LO antecedent = Cons.second( formula );         
		LO consequent = Cons.third( formula );
         
		formula = Cons.list( Clause.logicalOr,
				     Cons.list( Clause.logicalNot,
						antecedent ),
				     consequent );
	    }
	return Cons.cons( eliminateImplies( Cons.car ( formula )),
			  eliminateImplies( Cons.cdr ( formula )));
    }

    /**
     * 
     * eliminateXor
     * 
     * Return a logical expression substituting for #$xor.
     * 
     * (#$xor p q)
     * 
     * returned as
     * 
     * (#$and 
     *  (#$or p q)
     *  (#$not (#$and p q)))
     * 
     */
    public static LO eliminateXor( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateXor();
    }
   
    public LO eliminateXor()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalXor ))
	    {
		LO formula1 = Cons.second( formula );         
		LO formula2 = Cons.third( formula );
         
		formula = Cons.list( Clause.logicalAnd,
				     Cons.list( Clause.logicalOr,
						formula1,
						formula2 ),
                                 
				     Cons.list( Clause.logicalNot,
						Cons.list( Clause.logicalAnd,
							   formula1,
							   formula2 )));
	    }
	return Cons.cons( eliminateXor( Cons.car( formula )),
			  eliminateXor( Cons.cdr( formula )));
    }

    /**
     * 
     * eliminateEquivalent
     * 
     * Return a logical expression substituting for #$equivalent.
     * 
     * (#$equivalent p q)
     * 
     * returned as
     * 
     * (#$and 
     *  (#$or (#$not p) q)
     *  (#$or p (#$not q)))
     * 
     */
    public static LO eliminateEquivalent( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).eliminateEquivalent();
    }
   
    public LO eliminateEquivalent()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalEquivalent ))
	    {
		LO formula1 = Cons.second( formula );         
		LO formula2 = Cons.third( formula );
         
		formula = Cons.list( Clause.logicalAnd,
				     Cons.list( Clause.logicalOr,
						Cons.list( Clause.logicalNot,
							   formula1 ),
						formula2 ),
				     Cons.list( Clause.logicalOr,
						formula1,
						Cons.list( Clause.logicalNot,
							   formula2 )));
                                 
	    }
	return Cons.cons( eliminateEquivalent( Cons.car( formula )),
			  eliminateEquivalent( Cons.cdr( formula )));
    }

    /**
     * 
     * moveNotInwards
     * 
     * Return a formula resulting from the movement of the logical not
     * connective inwards until it applies only to literals.
     * 
     */
    public static LO moveNotInwards( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).moveNotInwards();
    }
   
    public LO moveNotInwards()
	throws ErrorException,
	       JumpException
    {
	LO saveFormula = formula;
	while ( true )
	    {
		// Repeated applications of DeMorgan's laws.
		distributeNotOverOr();
		distributeNotOverAnd();
		distributeNotOverUniv();
		distributeNotOverExists();
		doubleNegation();
		//JavaLisp.pprint( saveFormula );
		//JavaLisp.pprint( formula );
		if ( JavaLisp.equal( saveFormula, formula ).toBoolean() )
		    // No further changes detected.
		    return formula;
		saveFormula = formula;
	    }
    }

    /**
     * 
     * distributeNotOverOr
     * 
     * Return a logical expression moving #$not inwards.
     * 
     * (#$not (#$or p q))
     * 
     * returned as
     * 
     * (#$and 
     *  (#$not p)
     *  (#$not q))
     * 
     */
    public static LO distributeNotOverOr( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).distributeNotOverOr();
    }
   
    public LO distributeNotOverOr()
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( formula );
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalNot ))
	    {
		// Found (#$not .
		LO formula1 = Cons.second( formula );
		if ( formula1.isCons() )
		    {
			// Found (#$not ( .
			LO g = Cons.first( formula1 );
			if ( g.equals( Clause.logicalOr ))
			    {
				// Found (#$not (#$or .
				formula = Cons.list( Clause.logicalAnd);
				Enumeration e = Cons.cdr( formula1 ).elements();
				while ( true )
				    {
					if ( ! e.hasMoreElements() )
					    break;
					LO formula2 = ( LO ) e.nextElement();
					formula = Cons.append( formula,
							       Cons.list( Cons.list( Clause.logicalNot,
										     formula2 )));
				    }
			    }
            
		    }
	    }
	formula = Cons.cons( distributeNotOverOr( Cons.car( formula )),
			     distributeNotOverOr( Cons.cdr( formula )));
	return formula;
    }

    /**
     * 
     * distributeNotOverAnd
     * 
     * Return a logical expression moving #$not inwards.
     * 
     * (#$not (#$and p q))
     * 
     * returned as
     * 
     * (#$or 
     *  (#$not p)
     *  (#$not q))
     * 
     */
    public static LO distributeNotOverAnd( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).distributeNotOverAnd();
    }
   
    public LO distributeNotOverAnd()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalNot ))
	    {
		// Found (#$not .
		LO formula1 = Cons.second( formula );   
		if ( formula1.isCons() )
		    {
			// Found (#$not ( .
			LO g = Cons.first( formula1 );
			if ( g.equals( Clause.logicalAnd ))
			    {
				// Found (#$not (#$and .
				formula = Cons.list( Clause.logicalOr);
				Enumeration e = Cons.cdr( formula1 ).elements();
				while ( true )
				    {
					if ( ! e.hasMoreElements() )
					    break;
					LO formula2 = ( LO ) e.nextElement();
					formula = Cons.append( formula,
							       Cons.list( Cons.list( Clause.logicalNot,
										     formula2 )));
				    }
			    }
			//JavaLisp.pprint( formula );                     
		    }
	    }
	formula = Cons.cons( distributeNotOverAnd( Cons.car( formula )),
			     distributeNotOverAnd( Cons.cdr( formula )));
	//JavaLisp.pprint( formula );
	return formula;
    }

    /**
     * 
     * distributeNotOverUniv
     * 
     * Return a logical expression moving #$not inwards.
     * 
     * (#$not (#$forAll ?X p))
     * 
     * returned as
     * 
     * (#$thereExists ?X (#$not p)) 
     * 
     */
    public static LO distributeNotOverUniv( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).distributeNotOverUniv();
    }
   
    public LO distributeNotOverUniv()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalNot ))
	    {
		// Found (#$not .
		LO formula1 = Cons.second( formula );
		if ( formula1.isCons() )
		    {
			// Found (#$not ( .
			LO g = Cons.first( formula1 );
			if ( g.equals( Clause.logicalForAll ))
			    {
				// Found (#$not (#$forAll .
               
				formula = Cons.list( Clause.logicalThereExists,
						     Cons.second( formula1 ),
						     Cons.list( Clause.logicalNot,
								Cons.third( formula1 )));
			    }            
		    }
	    }
	formula = Cons.cons( distributeNotOverUniv( Cons.car( formula )),
			     distributeNotOverUniv( Cons.cdr( formula )));
	return formula;
    }

    /**
     * 
     * distributeNotOverExists
     * 
     * Return a logical expression moving #$not inwards.
     * 
     * (#$not (#$thereExists ?X p))
     * 
     * returned as
     * 
     * (#$forAll ?X (#$not p)) 
     * 
     */
    public static LO distributeNotOverExists( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).distributeNotOverExists();
    }
   
    public LO distributeNotOverExists()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalNot ))
	    {
		// Found (#$not .
		LO formula1 = Cons.second( formula );   
		if ( formula1.isCons() )
		    {
			// Found (#$not ( .
			LO g = Cons.first( formula1 );
			if ( g.equals( Clause.logicalThereExists ))
			    {
				// Found (#$not (#$forAll .
               
				formula = Cons.list( Clause.logicalForAll,
						     Cons.second( formula1 ),
						     Cons.list( Clause.logicalNot,
								Cons.third( formula1 )));
			    }            
		    }
	    }
	formula = Cons.cons( distributeNotOverExists( Cons.car( formula )),
			     distributeNotOverExists( Cons.cdr( formula )));
	return formula;
    }

    /**
     * 
     * doubleNegation
     * 
     * Return a logical expression moving #$not inwards.
     * 
     * (#$not (#$not p))
     * 
     * returned as
     * 
     * (p) 
     * 
     */
    public static LO doubleNegation( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).doubleNegation();
    }
   
    public LO doubleNegation()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalNot ))
	    {
		// Found (#$not .
		LO formula1 = Cons.second( formula );   
		if ( formula1.isCons() )
		    {
			// Found (#$not ( .
			LO g = Cons.first( formula1 );
			if ( g.equals( Clause.logicalNot ))
			    {
				// Found (#$not (#$not .
               
				formula = Cons.second( formula1 );
			    }      
		    }
	    }
	formula = Cons.cons( doubleNegation( Cons.car( formula )),
			     doubleNegation( Cons.cdr( formula )));
	return formula;
    }

    /**
     * 
     * quantifyFreeVariables
     * 
     * Return a formula with free variables universally quantified.
     * 
     * (#$P ?X) 
     * 
     * returned as
     * 
     * (#$forAll ?X
     *  (#$P ?X))
     * 
     */
    public static LO quantifyFreeVariables( LO aFormula, 
					    Queue universalVariables,
					    Queue existentialVariables,
					    Queue freeVariables )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula, 
			      universalVariables,
			      existentialVariables )).quantifyFreeVariables( freeVariables );
    }
   
    public LO quantifyFreeVariables()
	throws ErrorException,
	       JumpException
    {
	Queue freeVariables = new Queue();
	formula = quantifyFreeVariables( freeVariables );
	if ( freeVariables.list().equals( JavaLisp.nil ))
	    // No free variables, return formula unchanged.
	    return formula;
      
	// Form (#$forAll ...
	Enumeration e = freeVariables.elements();
	while ( true )
	    {
		if ( ! e.hasMoreElements() )
		    break;
		LO freeVariable = ( LO ) e.nextElement();
         
		formula = Cons.list( Clause.logicalForAll,
				     freeVariable,
				     formula );
	    }
	return formula;
    }
   
    public LO quantifyFreeVariables( Queue freeVariables )
	throws ErrorException,
	       JumpException
    {
	if ( ElSupport.groundQ( formula ).equals( JavaLisp.t ))
	    // No variables within.
	    return formula;
      
	if ( formula.isAtomic() )
	    {
		// Found either a universal or existential variable.
		if ( universalVariables.member( formula ).equals( JavaLisp.t ))
		    // Found quantified universal variable.
		    return formula;
         
		if ( existentialVariables.member( formula ).equals( JavaLisp.t ))
		    // Found quantified existential variable.
		    return formula;
         
		if ( freeVariables.member( formula ).equals( JavaLisp.t ))
		    // Found previously recorded free variable.
		    return Variable.create_variable( formula.toString() );
         
		freeVariables.addElement( formula );
		return Variable.create_variable( formula.toString() );
	    }
         
	LO first = Cons.car( formula );
	// Parse #$thereExists recording existential variable.
	if ( first.equals( Clause.logicalThereExists ))
	    {
		LO existentialVariable = Cons.second( formula );
		existentialVariables.addElement( existentialVariable );
		return Cons.list( first,
				  existentialVariable,
				  quantifyFreeVariables ( Cons.third( formula ),
							  universalVariables,
							  existentialVariables,
							  freeVariables ));
	    }
      
	// Parse #$forAll, and record the universal variable.
	if ( first.equals( Clause.logicalForAll ))
	    {
		LO universalVariable = Cons.second( formula );
		universalVariables.addElement( Cons.second( formula ));
		return Cons.list( first,
				  universalVariable,
				  quantifyFreeVariables( Cons.third( formula ),
							 universalVariables,
							 existentialVariables,
							 freeVariables ));
	    }
      
	return Cons.cons( quantifyFreeVariables( first, 
						 universalVariables,
						 existentialVariables,
						 freeVariables ),
			  quantifyFreeVariables( Cons.cdr( formula ), 
						 universalVariables,
						 existentialVariables,
						 freeVariables ));

    }
    /**
     * 
     * renameVariables
     * 
     * Return a formula with each quantified variable having a unique Heuristic 
     * Level name.
     * 
     * (#$or 
     *  (#$forAll ?X (#$P ?X))
     *  (#$forAll ?X (#$Q ?X)))
     * 
     * returned as
     * 
     * (#$or 
     *  (#$forAll #$?X (#$P #$?X))
     *  (#$forAll #$?X-1 (#$Q #$?X-1)))
     * 
     */
    public static LO renameVariables( LO aFormula, Queue formulaVariables )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula, formulaVariables )).renameVariables();
    }
   
    public LO renameVariables()
	throws ErrorException,
	       JumpException
    {
	formulaVariables = new Queue();

	return renameVariablesWithRegardTo( formula, 
					    new Hashtable() );
    }

   
    /**
     * 
     * renameVariablesWithRegardTo
     * 
     * Helper function for renameVariables.  The instance variable named
     * variables contains the quantified variables parsed at a given point
     * in the recursive descent of the formula.
     * 
     */
    public LO renameVariablesWithRegardTo( LO aFormula, 
					   Hashtable variables )
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.princ( "" );
	LO isGround = ElSupport.groundQ( aFormula );
	if ( isGround.equals( JavaLisp.t ))
	    // No variables within.
	    return aFormula;
      
	if ( aFormula.isAtomic() )
	    {
		if ( variables.containsKey( aFormula ))
		    // Rename the variable.
		    return ( LO ) variables.get( aFormula );
         
		return aFormula;
	    }
      
	LO f = Cons.first( aFormula );
	if ( f.equals( Clause.logicalForAll ) ||
	     f.equals( Clause.logicalThereExists ))
	    {
		Hashtable newVariables = ( Hashtable ) variables.clone();
		LO variable = Cons.second( aFormula );         
		LO body = Cons.third( aFormula );
		if ( formulaVariables.member( variable ).equals( JavaLisp.t ))
		    {
			// Variable is already quantified, and must be renamed within
			// the scope of the quantifier.
			LO renamedVariable = JavaLisp.gensym((( Symbol ) variable).printName() );
			renamedVariable = Variable.create_variable( renamedVariable.toString() );
			newVariables.put( variable, renamedVariable );
			body = renameVariablesWithRegardTo( body, newVariables );
			//JavaLisp.print( body );
            
			return Cons.list( f,
					  renamedVariable,
					  body );
		    }
		else
		    {
			// Add to list of variables.
			formulaVariables.addElement( variable );
            
			// Associated variable with alias, initially the same name.
			LO renamedVariable = Variable.create_variable( variable.toString() );
			newVariables.put( variable, renamedVariable );
            
			body = renameVariablesWithRegardTo( body, newVariables );
			//JavaLisp.print( body );
            
			return Cons.list( f,
					  renamedVariable,
					  body );
		    }
	    }
	return Cons.cons( renameVariablesWithRegardTo( Cons.car( aFormula ), variables ),
			  renameVariablesWithRegardTo( Cons.cdr( aFormula ), variables ));

    }

    /**
     * 
     * moveQuantifiersLeft
     * 
     * Return a formula with quantifiers moved to the left, in the order in
     * which they appear.
     * 
     * (#$or 
     *  (#$forAll ?X (#$P ?X))
     *  (#$thereExists ?Y (#$Q ?Y)))
     * 
     * returned as
     * 
     * (#$forAll ?X 
     *  (#$thereExists ?Y
     *   (#$or
     *    (#$P ?X)
     *    (#$Q ?Y))))
     * 
     */
    public static LO moveQuantifiersLeft( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).moveQuantifiersLeft();
    }
   
    public LO moveQuantifiersLeft()
	throws ErrorException,
	       JumpException
    {
	if ( formula.isAtomic() )
	    return formula;
      
	LO saveFormula = formula;
	while ( true )
	    {
		LO f = Cons.first( formula );
		LO g = JavaLisp.nil;
		LO variable = JavaLisp.nil;
         
		if ( f.equals( Clause.logicalAnd ) ||
		     f.equals( Clause.logicalOr ))
		    {
			Queue operands = new Queue();
			Enumeration e = Cons.cdr( formula ).elements();
			boolean foundQuantifier = false;
			while ( true )
			    {
				if ( ! e.hasMoreElements() )
				    break;
               
				LO operand = ( LO ) e.nextElement();
				if ( operand.isAtomic() || foundQuantifier )
				    {
					// Recurse on the operand.
					operand = ( new Formula( operand, formulaVariables )).moveQuantifiersLeft();
					operands.addElement( operand );
					continue;
				    }
				g = Cons.first( operand );
				if ( g.equals( Clause.logicalForAll ) ||
				     g.equals( Clause.logicalThereExists ))
				    {
					foundQuantifier = true;
					variable = Cons.second( operand );
					LO body = Cons.third( operand );
					// Recurse on the body.
					body = ( new Formula( body, formulaVariables )).moveQuantifiersLeft();
					operands.addElement( body );
				    }
				else
				    operands.addElement( operand );
			    }
			if ( foundQuantifier )
			    formula = Cons.list( g,
						 variable,
						 Cons.cons( f,
							    operands.list() ));
			else
			    formula = Cons.cons( f,
						 operands.list() );
		    }
		else
		    formula = Cons.cons( moveQuantifiersLeft( Cons.car( formula )),
					 moveQuantifiersLeft( Cons.cdr( formula )));
		//JavaLisp.pprint( saveFormula );
		//JavaLisp.pprint( formula );
		if ( JavaLisp.equal( saveFormula, formula ).toBoolean() )
		    // No further changes detected.
		    return formula;
		saveFormula = formula;
	    }
    }

    /**
     * 
     * skolemize
     * 
     * Return a formula with existential quantifiers replaced by
     * skolem functions.
     * 
     * (#$or 
     *  (#$forAll ?X (#$P ?X))
     *  (#$thereExists ?Y (#$Q ?Y)))
     * 
     * returned as
     * 
     * (#$and
     *  (#$isa #$Skf-1 #$SkolemFunction)
     *  (#$arity #$Skf-1 1)
     *  (#$forAll ?X 
     *   (#$or
     *    (#$P ?X)
     *    (#$Q 
     *     (#$Skf-1 ?X))))))
     * 
     */
    public static LO skolemize( LO aFormula, 
				Queue universalVariables,
				Queue existentialVariables,
				Queue skolemAssertions,
				Hashtable skolemFunctions,
				Hashtable skolemUniversalVariables )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula, 
			      universalVariables,
			      existentialVariables )).skolemize( skolemAssertions, 
								 skolemFunctions,
								 skolemUniversalVariables );
    }
   
    public LO skolemize()
	throws ErrorException,
	       JumpException
    {
	Queue skolemAssertions = new Queue();
	Hashtable skolemFunctions = new Hashtable();
	Hashtable skolemUniversalVariables = new Hashtable();
	universalVariables = new Queue();
	existentialVariables = new Queue();   
	LO formula1 = skolemize( skolemAssertions,
				 skolemFunctions,
				 skolemUniversalVariables );
	if ( skolemAssertions.list().equals( JavaLisp.nil ))
	    // No existential variables, return formula unchanged.
	    return formula1;
      
	Queue result = new Queue();
	// Form (#$and ...
	result.addElement( Clause.logicalAnd );
	result.append( skolemAssertions );
	result.addElement( formula1 );
	return result.list();
    }
   
    public LO skolemize( Queue skolemAssertions, 
			 Hashtable skolemFunctions,
			 Hashtable skolemUniversalVariables )
	throws ErrorException,
	       JumpException
    {
	if ( Variable.fully_bound_p( formula ).equals( JavaLisp.t ))
	    // No variables within.
	    return formula;
      
	if ( formula.isAtomic() )
	    {
		// Found either a universal or existential variable.
		if ( universalVariables.member( formula ).equals( JavaLisp.t ))
		    // Skolemize applies only to existential variables.
		    return formula;
         
		if ( existentialVariables.member( formula ).equals( JavaLisp.t ))
		    {
			LO skolemFunction;
			// Retrieve the universal variables whose scope contains this
			// existential variable.
			Queue skolemUnivVars = ( Queue ) skolemUniversalVariables.get( formula );
            
			if ( ! skolemFunctions.containsKey( formula ))
			    {
				// Skolemize the existential variable.
				LO skolemFunctionName = JavaLisp.gensym( LispString.add( "Skf-" ));
				skolemFunction = Constant.create_constant( skolemFunctionName.toString());
               
				// Save skolemFunction in case it is used again.
				skolemFunctions.put( formula, skolemFunction );
				// Add Skolem Assertions.
				// (#$isa #$Skf-nnn #$SkolemFunction)
				skolemAssertions.addElement( Cons.list ( IsaSupport.isaSymbol,
									 skolemFunction,
									 ElSupport.skolemFunctionSymbol ));
				// (#$arity #$Skf-nnn N)
				skolemAssertions.addElement( Cons.list ( KbAccess.meldAritySymbol,
									 skolemFunction,
									 new LispInteger ( skolemUnivVars.size() )));
			    }
			else
			    skolemFunction = ( LO ) skolemFunctions.get( formula );
            
			// Return an expression of the form (Skf-nnnn #$u1 #$u2 ... )
			return Cons.append( Cons.list( skolemFunction ),
					    skolemUnivVars.list());
		    }
		throw new ErrorException( "skolemize: unqualified variable " + formula.toString() );
	    }
         
	LO first = Cons.car( formula );
	// Parse #$thereExists and remove it.
	if ( first.equals( Clause.logicalThereExists ))
	    {
		LO existentialVariable = Cons.second( formula );
		existentialVariables.addElement( existentialVariable );
		// Record (clone) the universal variables whose scope covers this
		// existential variable.
		skolemUniversalVariables.put( existentialVariable, universalVariables.clone() );
		return skolemize ( Cons.third( formula ),
				   universalVariables,
				   existentialVariables,
				   skolemAssertions,
				   skolemFunctions,
				   skolemUniversalVariables );
	    }
      
	// Parse #$forAll, and record the universal variable.
	if ( first.equals( Clause.logicalForAll ))
	    {
		LO universalVariable = Cons.second( formula );
		universalVariables.addElement( universalVariable );
		return Cons.list( first,
				  universalVariable,
				  skolemize ( Cons.third( formula ),
					      universalVariables,
					      existentialVariables,
					      skolemAssertions,
					      skolemFunctions,
					      skolemUniversalVariables ));
	    }
	// Clone the universal variables to reflect scope.  Discarding
	// universal variables parsed in sub forms.
	Queue saveUniversalVariables = ( Queue ) universalVariables.clone();
      
	// Recurse on the sub forms.
	return Cons.cons( skolemize( first, 
				     ( Queue ) saveUniversalVariables.clone(),
				     existentialVariables,
				     skolemAssertions,
				     skolemFunctions,
				     skolemUniversalVariables ),
			  skolemize( Cons.cdr( formula ), 
				     ( Queue ) saveUniversalVariables.clone(),
				     existentialVariables,
				     skolemAssertions,
				     skolemFunctions,
				     skolemUniversalVariables ));
    }

    /**
     * 
     * dropUniversalQualifiers
     * 
     * Return a formula with universal quantifiers removed.
     * 
     * (#$or 
     *  (#$forAll ?X (#$P ?X))
     *  (#$thereExists ?Y (#$Q ?Y)))
     * 
     * returned as
     * 
     * (#$and
     *  (#$isa #$Skf-1 #$SkolemFunction)
     *  (#$arity #$Skf-1 1)
     *   (#$or
     *    (#$P ?X)
     *    (#$Q 
     *     (#$Skf-1 ?X))))
     * 
     */
    public static LO dropUniversalQualifiers( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).dropUniversalQualifiers();
    }
   
    public LO dropUniversalQualifiers()
	throws ErrorException,
	       JumpException
    {
	if ( Variable.fully_bound_p( formula ).equals( JavaLisp.t ))
	    // No variables within.
	    return formula;
      
	if ( formula.isAtomic() )
	    // No #$forAll within.
	    return formula;
         
	LO first = Cons.car( formula );
	// Parse #$forAll and remove it.
	if ( first.equals( Clause.logicalForAll ))
	    return dropUniversalQualifiers ( Cons.third( formula ));
      
	// Recurse on formula components.
	return Cons.cons( dropUniversalQualifiers( first ),
			  dropUniversalQualifiers( Cons.cdr( formula )));
    }

    /**
     * 
     * binaryAndOr
     * 
     * Return a formula in which #$and, #$or are binary operators.
     * 
     * 
     * (#$or
     *  (#$P)
     *  (#$Q)
     *  (#$R)
     *  (#$S))
     *
     * returned as
     * 
     * (#$or
     *  (#$P)
     *  (#$or
     *   (#$Q)
     *   (#$or
     *    (#$R)
     *    (#$S))))
     * 
     */
    public static LO binaryAndOr( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).binaryAndOr();
    }
   
    public LO binaryAndOr()
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( formula );
	if ( formula.isAtomic() )
	    // No #$and nor #$or
	    return formula;
      
	LO first = Cons.first( formula );
	if ( first.equals( Clause.logicalOr ) ||
	     first.equals( Clause.logicalAnd ))
	    {
		LO operands = Cons.cdr( formula );
		if ( operands.equals( JavaLisp.nil ))
		    {
			if ( first.equals( Clause.logicalOr ))
			    // Return (#$or) as #$False.
			    return ElSupport.falseSymbol;
			else
			    // Return (#$and) as #$True.
			    return ElSupport.trueSymbol;
		    }
         
		if ((( Cons ) operands).size() == 1 )
		    // When (#and arg) return arg.
		    formula = Cons.second( formula );
		else if ((( Cons ) operands).size() == 2 )
		    // Binary already.
		    formula = formula;
		else
		    {
			LO remainingOperands = Cons.cdr( Cons.cdr ( formula ));
            
			// Return for example,
			// (#$and arg1 (#$and arg2 ... )).
			return  Cons.list( first,
					   Cons.second( formula ),
					   binaryAndOr(
						       Cons.cons( first,
								  remainingOperands )));
		    }
	    }
      
	// Recurse on formula components transforming embedded
	// #$and, #$or clauses.
	return Cons.cons( binaryAndOr( Cons.car( formula )),
			  binaryAndOr( Cons.cdr( formula )));
      
    }

    /**
     * 
     * distributeOrOverAnd
     * 
     * Return a formula with #$or distributed over #$and.
     * 
     * (#$or 
     *  (#$P)
     *  (#$and (#$Q) (#$R)))
     * 
     * returned as
     * 
     * (#$and
     *  (#$or (#$P) (#$Q))
     *  (#$or (#$P) (#$R)))
     * 
     * 
     */
    public static LO distributeOrOverAnd( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).distributeOrOverAnd();
    }
   
    public LO distributeOrOverAnd()
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( formula );
	if ( formula.isAtomic() )
	    // No #$and
	    return formula;
      
	// Recurse on formula components transforming embedded
	// #$and forms.
	formula = Cons.cons( distributeOrOverAnd( Cons.car( formula )),
			     distributeOrOverAnd( Cons.cdr( formula )));
      
	LO f = Cons.first( formula );
	if ( f.equals( Clause.logicalOr ))
	    {
		boolean foundAnd = false;
		LO orOperand1 = Cons.second( formula );
		LO orOperand2 = Cons.third( formula );
         
		if ( Cons.fourth( formula ).toBoolean() )
		    throw new ErrorException( "distributeOrOverAnd: ill-formed " +
					      formula.toString() );
         
		if ( orOperand1.isCons() )
		    {
			LO orOperand1First = Cons.car( orOperand1 );
			if ( orOperand1First.equals( Clause.logicalAnd ))
			    {
				// Found #$and
				foundAnd = true;
				LO andOperand1 = Cons.second( orOperand1 );
				LO andOperand2 = Cons.third( orOperand1 );
               
				if ( Cons.fourth( orOperand1 ).toBoolean() )
				    throw new ErrorException( "distributeOrOverAnd: ill-formed " +
							      orOperand1.toString() );
               
               
				formula = Cons.list( Clause.logicalAnd,
						     Cons.list( Clause.logicalOr,
								andOperand1,
								orOperand2 ),
						     Cons.list( Clause.logicalOr,
								andOperand2,
								orOperand2 ));
               
				// Second operand may qualify, so try again.
				return distributeOrOverAnd( formula );
			    }
               
		    }
		if ( orOperand2.isCons() )
		    {
			LO orOperand2First = Cons.car( orOperand2 );
			if ( orOperand2First.equals( Clause.logicalAnd ))
			    {
				// Found #$and
				foundAnd = true;
				LO andOperand1 = Cons.second( orOperand2 );
				LO andOperand2 = Cons.third( orOperand2 );
               
				if ( Cons.fourth( orOperand2 ).toBoolean() )
				    throw new ErrorException( "distributeOrOverAnd: ill-formed " +
							      orOperand1.toString() );
               
               
				return Cons.list( Clause.logicalAnd,
						  Cons.list( Clause.logicalOr,
							     orOperand1,
							     andOperand1 ),
						  Cons.list( Clause.logicalOr,
							     orOperand1,
							     andOperand2 ));
			    }
		    }
	    }
      
	return formula;
    }

    /**
     * 
     * flattenNestedAnd
     * 
     * Return a formula with #$and having the maximum number of
     * arguments.
     * 
     * (#$and (#$P) (#$and (#$Q) (#$R)))
     * 
     * returned as
     * 
     * (#$and (#$P) (#$Q) (#$R))
     * 
     */
    public static LO flattenNestedAnd( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).flattenNestedAnd();
    }
   
    public LO flattenNestedAnd()
	throws ErrorException,
	       JumpException
    {
	boolean flattenedAnd = false;
	//JavaLisp.pprint( formula );
	if ( formula.isAtomic() )
	    // No #$and
	    return formula;
      
	LO first = Cons.first( formula );
	if ( first.equals( Clause.logicalAnd ))
	    {
		Queue result = new Queue();
		result.addElement( first );
		Enumeration e = Cons.cdr( formula ).elements();
		while ( true )
		    {
			if ( ! e.hasMoreElements() )
			    break;
            
			LO operand = ( LO ) e.nextElement();
			if ( operand.isAtomic() )
			    {
				result.addElement( operand );
				continue;
			    }
            
			LO operandFirst = Cons.first( operand );
			if ( operandFirst.equals( Clause.logicalAnd ))
			    // Matching logical connective #$and.
			    {
				flattenedAnd = true;
				result.append( Cons.cdr( operand ));
			    }
			else
			    result.addElement( operand );
		    }
		formula = result.list();
	    }
      
	if ( flattenedAnd )
	    // Recurse on transformed formula.
	    return flattenNestedAnd ( formula );
	else
	    // Recurse on formula components transforming embedded
	    // #$or forms.
	    return Cons.cons( flattenNestedAnd( Cons.car( formula )),
			      flattenNestedAnd( Cons.cdr( formula )));
      
    }

    /**
     * 
     * flattenNestedOr
     * 
     * Return a formula with #$or having the maximum number of
     * arguments.
     * 
     * (#$or (#$P) (#$or (#$Q) (#$R)))
     * 
     * returned as
     * 
     * (#$or (#$P) (#$Q) (#$R))
     * 
     */
    public static LO flattenNestedOr( LO aFormula )
	throws ErrorException,
	       JumpException
    {
	return ( new Formula( aFormula )).flattenNestedOr();
    }
   
    public LO flattenNestedOr()
	throws ErrorException,
	       JumpException
    {
	boolean flattenedOr = false;
	//JavaLisp.pprint( formula );
	if ( formula.isAtomic() )
	    // No #$or
	    return formula;
      
	LO first = Cons.first( formula );
	if ( first.equals( Clause.logicalOr ))
	    {
		Queue result = new Queue();
		result.addElement( first );
		Enumeration e = Cons.cdr( formula ).elements();
		while ( true )
		    {
			if ( ! e.hasMoreElements() )
			    break;
            
			LO operand = ( LO ) e.nextElement();
			if ( operand.isAtomic() )
			    {
				result.addElement( operand );
				continue;
			    }
            
			LO operandFirst = Cons.first( operand );
			if ( operandFirst.equals( Clause.logicalOr ))
			    // Matching logical connective #$or.
			    {
				flattenedOr = true;
				result.append( Cons.cdr( operand ));
			    }
			else
			    result.addElement( operand );
		    }
		formula = result.list();
	    }
      
	if ( flattenedOr )
	    // Recurse on transformed formula.
	    return flattenNestedOr ( formula );
	else
	    // Recurse on formula components transforming embedded
	    // #$or forms.
	    return Cons.cons( flattenNestedOr( Cons.car( formula )),
			      flattenNestedOr( Cons.cdr( formula )));
      
    }

    /**
     * 
     * ensureAnd
     * 
     * Return a formula with #$and.
     * 
     * (#$P)
     * 
     * returned as
     * 
     * (#$and (#$P))
     * 
     */
    public LO ensureAnd()
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( formula );
	if ( formula.isAtomic() )
	    // No #$and, add it.
	    return Cons.list( Clause.logicalAnd,
			      formula );
      
	LO first = Cons.first( formula );
	if ( first.equals( Clause.logicalAnd ))
	    // #$and found.
	    return formula;
	else
	    // #$and not found, add it.
	    return Cons.list( Clause.logicalAnd,
			      formula );
    }

    /**
     * 
     * verifyWellFormedCnf
     * 
     * Throw an exception if the formula is not a well formed
     * Conjunctive Normal Form.
     * 
     * 
     */
    public void verifyWellFormedCnf()
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( formula );
	if ( formula.isAtomic() )
	    throw new ErrorException( "verifyWellFormedCnf: atomic " + formula.toString() );
      
	LO first = Cons.first( formula );
	if ( ! first.equals( Clause.logicalAnd ))
	    throw new ErrorException( "verifyWellFormedCnf: missing #$and " + formula.toString() );
      
	Enumeration eAnd = Cons.cdr( formula ).elements();
	while ( true )
	    {
		if ( ! eAnd.hasMoreElements() )
		    break;
         
		LO andOperand = ( LO ) eAnd.nextElement();
		LO andOperandFirst;
		if ( andOperand.isCons() )
		    {
			andOperandFirst = Cons.first( andOperand );
			// Only allow #$or as a logical connective, below #$and.
			if ( andOperandFirst.equals( Clause.logicalAnd ) ||
			     andOperandFirst.equals( Clause.logicalXor ) ||
			     andOperandFirst.equals( Clause.logicalEquivalent ) ||
			     andOperandFirst.equals( Clause.logicalForAll ) ||
			     andOperandFirst.equals( Clause.logicalImplies ) ||
			     andOperandFirst.equals( Clause.logicalThereExistAtLeast ) ||
			     andOperandFirst.equals( Clause.logicalThereExistAtMost ) ||
			     andOperandFirst.equals( Clause.logicalThereExistExactly ) ||
			     andOperandFirst.equals( Clause.logicalThereExists ))
			    throw new ErrorException( "verifyWellFormedCnf: invalid logical connective " + 
						      andOperandFirst.toString() );
            
			if ( andOperandFirst.equals( Clause.logicalOr ))
			    {
				Enumeration eOr = Cons.cdr( andOperand ).elements();
				while ( true )
				    {
					if ( ! eOr.hasMoreElements() )
					    break;
					LO orOperand = ( LO ) eOr.nextElement();
					if ( orOperand.isCons() )
					    {
						// Below the first #$or, only #$not allowed.
						LO orOperandFirst = Cons.first( orOperand );
						if ( orOperandFirst.equals( Clause.logicalAnd ) ||
						     orOperandFirst.equals( Clause.logicalOr ) ||
						     orOperandFirst.equals( Clause.logicalXor ) ||
						     orOperandFirst.equals( Clause.logicalEquivalent ) ||
						     orOperandFirst.equals( Clause.logicalForAll ) ||
						     orOperandFirst.equals( Clause.logicalImplies ) ||
						     orOperandFirst.equals( Clause.logicalThereExistAtLeast ) ||
						     orOperandFirst.equals( Clause.logicalThereExistAtMost ) ||
						     orOperandFirst.equals( Clause.logicalThereExistExactly ) ||
						     orOperandFirst.equals( Clause.logicalThereExists ))
						    throw new ErrorException( "verifyWellFormedCnf: invalid logical connective " + 
									      orOperandFirst.toString() );
                     
						verifyWellFormedLiteral( orOperand );
                     
					    }
					else if ( ! orOperand.equals( ElSupport.trueSymbol ) &&
						  ! orOperand.equals( ElSupport.falseSymbol ))
					    throw new ErrorException( "verifyWellFormedCnf: invalid atom " + 
								      orOperand.toString() );
				    }
			    }
			else
			    verifyWellFormedLiteral( andOperand );
		    }
		else if ( ! andOperand.equals( ElSupport.trueSymbol ) &&
			  ! andOperand.equals( ElSupport.falseSymbol ))
		    throw new ErrorException( "verifyWellFormedCnf: invalid atom " + 
					      andOperand.toString() );
	    }
      
    }
   
    /**
     * 
     * verifyWellFormedLiteral
     * 
     * Throw an exception if the formula is not a well formed literal
     * 
     */
    public static void verifyWellFormedLiteral( LO literal )
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( literal );
	if ( literal.isAtomic() )
	    throw new ErrorException( "verifyWellFormedLiteral: atomic " + literal.toString() );
      
	LO first = Cons.first( literal );
	if ( first.equals( Clause.logicalAnd ) ||
	     first.equals( Clause.logicalOr ) ||
	     first.equals( Clause.logicalXor ) ||
	     first.equals( Clause.logicalEquivalent ) ||
	     first.equals( Clause.logicalForAll ) ||
	     first.equals( Clause.logicalImplies ) ||
	     first.equals( Clause.logicalThereExistAtLeast ) ||
	     first.equals( Clause.logicalThereExistAtMost ) ||
	     first.equals( Clause.logicalThereExistExactly ) ||
	     first.equals( Clause.logicalThereExists ))
	    throw new ErrorException( "verifyWellFormedLiteral: invalid logical connective " + 
				      first.toString() );
      
	if ( first.equals( Clause.logicalNot ))
	    {
		LO notOperand = Cons.cdr( literal );
		if ( notOperand.isCons() )
		    verifyWellFormedAtomicFormula( Cons.cdr( literal ));
		else if ( Variable.variable_p( notOperand ).equals( JavaLisp.nil ))
		    throw new ErrorException( 
					     "verifyWellFormedLiteral: atomic operand for #$not is not a variable " + 
					     notOperand.toString() );
	    }
	else
	    verifyWellFormedAtomicFormula( literal );
    }

    /**
     * 
     * verifyWellFormedAtomicFormula
     * 
     * Throw an exception if the formula is not a well formed atomic formula.
     * 
     */
    public static void verifyWellFormedAtomicFormula( LO anAtomicFormula )
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( anAtomicFormula );
	if ( anAtomicFormula.isAtomic() )
	    throw new ErrorException( "verifyWellFormedAtomicFormula: atomic " + anAtomicFormula.toString() );
      
	LO first = Cons.first( anAtomicFormula );
	if ( first.equals( Clause.logicalAnd ) ||
	     first.equals( Clause.logicalNot ) ||
	     first.equals( Clause.logicalOr ) ||
	     first.equals( Clause.logicalXor ) ||
	     first.equals( Clause.logicalEquivalent ) ||
	     first.equals( Clause.logicalForAll ) ||
	     first.equals( Clause.logicalImplies ) ||
	     first.equals( Clause.logicalThereExistAtLeast ) ||
	     first.equals( Clause.logicalThereExistAtMost ) ||
	     first.equals( Clause.logicalThereExistExactly ) ||
	     first.equals( Clause.logicalThereExists ))
	    throw new ErrorException( "verifyWellFormedAtomicFormula: invalid logical connective " + 
				      first.toString() );
      
	verifyWffAtomicFormulaPart( Cons.car( anAtomicFormula ));
	verifyWffAtomicFormulaPart( Cons.cdr( anAtomicFormula ));
    }

    /**
     * 
     * verifyWffAtomicFormulaPart
     * 
     * Throw an exception if the formula is not a well formed atomic formula
     * part.
     * 
     */
    public static void verifyWffAtomicFormulaPart( LO anAtomicFormulaPart )
	throws ErrorException,
	       JumpException
    {
	//JavaLisp.print( anAtomicFormulaPart );
      
	if ( anAtomicFormulaPart.isAtomic() )
	    {
		if ( anAtomicFormulaPart.equals( Clause.logicalAnd ) ||
		     anAtomicFormulaPart.equals( Clause.logicalNot ) ||
		     anAtomicFormulaPart.equals( Clause.logicalOr ) ||
		     anAtomicFormulaPart.equals( Clause.logicalXor ) ||
		     anAtomicFormulaPart.equals( Clause.logicalEquivalent ) ||
		     anAtomicFormulaPart.equals( Clause.logicalForAll ) ||
		     anAtomicFormulaPart.equals( Clause.logicalImplies ) ||
		     anAtomicFormulaPart.equals( Clause.logicalThereExistAtLeast ) ||
		     anAtomicFormulaPart.equals( Clause.logicalThereExistAtMost ) ||
		     anAtomicFormulaPart.equals( Clause.logicalThereExistExactly ) ||
		     anAtomicFormulaPart.equals( Clause.logicalThereExists ))
		    throw new ErrorException( "verifyWffAtomicFormulaPart: invalid logical connective " + 
					      anAtomicFormulaPart.toString() );
         
		return;
	    }
      
	LO first = Cons.first( anAtomicFormulaPart );
	if ( first.equals( Clause.logicalAnd ) ||
	     first.equals( Clause.logicalNot ) ||
	     first.equals( Clause.logicalOr ) ||
	     first.equals( Clause.logicalXor ) ||
	     first.equals( Clause.logicalEquivalent ) ||
	     first.equals( Clause.logicalForAll ) ||
	     first.equals( Clause.logicalImplies ) ||
	     first.equals( Clause.logicalThereExistAtLeast ) ||
	     first.equals( Clause.logicalThereExistAtMost ) ||
	     first.equals( Clause.logicalThereExistExactly ) ||
	     first.equals( Clause.logicalThereExists ))
	    throw new ErrorException( "verifyWffAtomicFormulaPart: invalid logical connective " + 
				      first.toString() );
      
	verifyWffAtomicFormulaPart( Cons.car( anAtomicFormulaPart ));
	verifyWffAtomicFormulaPart( Cons.cdr( anAtomicFormulaPart ));
    }


}
