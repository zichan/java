
/**
 * Write a description of class classes here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
public class classes extends Frame implements ActionListener
{
    private Button keys[];
    private TextField lcd;
    public classes()
    {
        keys = new Button[10];
        keys[1] = new Button("Math");
        keys[2] = new Button("Science");
        keys[3] = new Button("Gym");
        keys[4] = new Button("Health");
        keys[5] = new Button("History");
        keys[6] = new Button("English");
        keys[7] = new Button("Spanish");
        keys[8] = new Button("German");
        keys[9] = new Button("French");
        setLayout(new BorderLayout());
        lcd = new TextField(20);
        for (int i=1;i<=9;i++)
            keys[i].addActionListener(this);
        Panel keypanel = new Panel();
        for (int i=1;i<=9;i++)
            keypanel.add(keys[i]);
        add(keypanel,BorderLayout.CENTER);
        add(lcd,BorderLayout.NORTH);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
    }
    public void actionPerformed(ActionEvent e)
    {
        for (int i=0; i<keys.length ; i++)
         {
             if(e.getSource() == keys[i])
             {
                 switch(i)
                 {
                     case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: 
                     case 9: case 10: case 11: case12:
                     lcd.setText(keys[i].getLabel());
                     break;
                 }
             }
         }
    }
    public static void main(String[] args)
    {
        classes f =new classes();
        f.setTitle(" Application");
        f.setBounds(200,200,300,300);
        f.setVisible(true);
        Image icon = Toolkit.getDefaultToolkit().getImage("cup.jpg");
        f.setIconImage(icon);
    }
}