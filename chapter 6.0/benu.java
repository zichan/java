
/**
 * Write a description of class Menu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
public class benu extends Frame implements ActionListener
{
    private TextField text;
    public benu()
    {
        MenuBar menu = new MenuBar();
        setMenuBar(menu);
        Menu Th = new Menu("Things", true);
        menu.add(Th);
            MenuItem Hello = new MenuItem("Hello");
            Th.add(Hello);
            MenuItem Hola = new MenuItem("Hola");
            Th.add(Hola);
            MenuItem disp = new MenuItem("Display");
            Th.add(disp);
            MenuItem Text = new MenuItem("Text");
            Th.add(Text);
            MenuItem Bowl = new MenuItem("Bowl");
            Th.add(Bowl);
        text = new TextField(20);
        setLayout(new BorderLayout());
        Hello.addActionListener(this);
        Hello.setActionCommand("Hello");
        Hola.addActionListener(this);
        Hola.setActionCommand("Hola");
        disp.addActionListener(this);
        disp.setActionCommand("Display");
        Text.addActionListener(this);
        Text.setActionCommand("Text");
        Bowl.addActionListener(this);
        Bowl.setActionCommand("Bowl");
        text.setEditable(false);
        add(text, BorderLayout.CENTER);
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}});
    }
    public void actionPerformed(ActionEvent e)
    {
        String arg = e.getActionCommand();
        if (arg=="Hello")
            text.setText(arg);
        if (arg=="Hola")
            text.setText(arg);
        if (arg=="Display")
            text.setText(arg);
        if (arg=="Text")
            text.setText(arg);
        if (arg=="Bowl")
            text.setText(arg);
     }
     public static void main(String args[])
    {
        benu f =new benu();
        f.setTitle(" Application");
        f.setBounds(200,200,300,300);
        f.setVisible(true);
        Image icon = Toolkit.getDefaultToolkit().getImage("cup.jpg");
        f.setIconImage(icon);
    }
}