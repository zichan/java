
import java.awt.*;
import javax.swing.*;
import java.lang.*;
import java.awt.*;
import java.applet.*;
import java.awt.event.*;
import java.util.*;
import java.math.*;
/**
 * Class NUM4 - write a description of the class here
 * 
 * @author (your name) 
 * @version (a version number)
 */
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
public class NUM4 extends Applet implements ActionListener
{
    private Button keys[];
    private TextField lcd;
    Canvas currtime;
    Thread timer;
    public void init()
    {
        lcd = new TextField(20);
        lcd.setEditable(false);
        Panel keypad = new Panel();
        keys = new Button[13];
        for (int i=0; i<=9; i++)
           keys[i] = new Button(String.valueOf(i));
                
        keys[10] = new Button("*");
        keys[11] = new Button("0");
        keys[12] = new Button("#");
        setLayout(new BorderLayout());
        keypad.setLayout(new GridLayout(4,4,10,10));
        for (int i=1; i<=12; i++)
            keypad.add(keys[i]);
      
        for (int i=0; i<keys.length; i++)
            keys[i].addActionListener(this);
        add(lcd, BorderLayout.NORTH);
        add(keypad, BorderLayout.CENTER);
        
     }
     public void actionPerformed(ActionEvent e)
     {
         for (int i=0; i<keys.length; i++)
         {
             if(e.getSource() == keys[i])
             {
                 switch(i)
                 {
                     case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: 
                     case 9: case 10: case 11: case 12:
                     lcd.setText(lcd.getText() + keys[i].getLabel());
                     break;
                 }
             }
         }
     }	
}