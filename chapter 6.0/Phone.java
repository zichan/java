
/**
 * Write a description of class Phone here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
import java.awt.datatransfer.*;
import java.text.DecimalFormat;
import javax.swing.JOptionPane;
import java.io.*;
public class Phone extends Frame implements ActionListener
{
    DataOutputStream output;
    private Button keys[];
    private Panel keypad;
    private TextField lcd;
    private double opl;
    private boolean first;
    private boolean foundKey;
    private int lastOp;
    public Phone()
    {
        MenuBar mnuBar = new MenuBar();
        setMenuBar(mnuBar);
        
        Menu mnuFile = new Menu("File", true);
        mnuBar.add(mnuFile);
            MenuItem mnuFileSave = new MenuItem("Save");
            mnuFile.add(mnuFileSave);
            MenuItem mnuFileOpen = new MenuItem("Open");
            mnuFile.add(mnuFileOpen);
        mnuFileSave.addActionListener(this);
        mnuFileSave.setActionCommand("Save");
        mnuFileOpen.addActionListener(this);
        mnuFileOpen.setActionCommand("Open");
        lcd = new TextField(20);
        lcd.setEditable(false);
        keypad = new Panel();
        keys = new Button[13];
        first = true;
        opl = 0.0;
        lastOp = 0;            
        for (int i=0; i<=9; i++)
           keys[i] = new Button(String.valueOf(i));
                
        keys[10] = new Button("*");
        keys[11] = new Button("0");
        keys[12] = new Button("#");
        setLayout(new BorderLayout());
        keypad.setLayout(new GridLayout(4,4,10,10));
        for (int i=1; i<=12; i++)
            keypad.add(keys[i]);
      
        for (int i=0; i<keys.length; i++)
            keys[i].addActionListener(this);
                
        add(lcd, BorderLayout.NORTH);
        add(keypad, BorderLayout.CENTER);
        
        addWindowListener(new WindowAdapter(){public void windowClosing(WindowEvent e){System.exit(0);}} );
     }
     public void actionPerformed(ActionEvent e)
     {
         String arg = e.getActionCommand();
                
         if (arg == "Save")
         {
             
             try
             {
                 String filename = JOptionPane.showInputDialog(null,"Enter the name of the person","Name");
                 filename = filename+".txt";
                 output = new DataOutputStream(new FileOutputStream(filename));
             }
             catch(IOException io)
             {
                 JOptionPane.showMessageDialog(null,"The save failed, please check Drive.","Error",JOptionPane.INFORMATION_MESSAGE);
             }
             try
             {
                 output.writeUTF(lcd.getText());
                 JOptionPane.showMessageDialog(null, "Saved succesusfuly","Saved",JOptionPane.INFORMATION_MESSAGE);
             }
             catch(IOException c)
             {
                 JOptionPane.showMessageDialog(null, "Diddent Save","Error",JOptionPane.INFORMATION_MESSAGE);
             }
         }
         if (arg=="Open")
         {
            }
         foundKey = false;                
         for (int i=0; i<keys.length && !foundKey; i++)
         {
             if(e.getSource() == keys[i])
             {
                 foundKey=true;
                 switch(i)
                 {
                     case 0: case 1: case 2: case 3: case 4: case 5: case 6: case 7: case 8: 
                     case 9: case 10: case 11: case12:
                     lcd.setText(lcd.getText() + keys[i].getLabel());
                     break;
                 }
             }
         }
    }         
    public static void main(String args[])
    {
        Phone f =new Phone();
        f.setTitle(" Application");
        f.setBounds(200,200,300,300);
        f.setVisible(true);
        f.setBackground(Color.red);
        Image icon = Toolkit.getDefaultToolkit().getImage("calcImage.gif");
        f.setIconImage(icon);
    }
}