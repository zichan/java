
/**
 * Write a description of class h here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.util.*;
class Clock extends Thread
{
	Canvas Time;
	private Date now;
	private String currentTime;
	public Clock(Canvas _Time)
	{
		Time = _Time;
		start();
	}
	public void run()
	{
		while (true)
		{
			try
			{
				draw_clock();
				sleep(1000);
			}
			catch (InterruptedException e)
			{
			    suspend();
			}
			catch (NullPointerException e)
			{
			    suspend();
			}
		}
	}
	public void draw_clock()
	{
		try
		{
			Graphics g = Time.getGraphics();
		
			g.setColor(Color.gray);
			g.setColor(Color.orange);
			get_the_time();
			g.drawString("Current Time - " + currentTime, 0, 17);		
		}
		catch (NullPointerException e) { suspend(); }
	}
	public void get_the_time()
	{
		now = new Date( );
		int a = now.getHours();
		int b = now.getMinutes();
		int c = now.getSeconds();
		
		if (a == 0) a = 12;
							
		if (a > 12) a = a -12;
		if ( a < 10)
			currentTime = "0" + a + ":" ;
		else
			currentTime = a +":";			
		
		if (b < 10)
			currentTime = currentTime + "0" + b + ":" ;
		else
			currentTime = currentTime + b + ":" ;
		
		if (c < 10)
			currentTime = currentTime + "0" + c ;
		else
			currentTime = currentTime + c;
	}
}