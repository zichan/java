
/**
 * Write a description of class Picture here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.awt.datatransfer.*;
public class Picture extends Applet implements ActionListener
{
    private Image cup;
    private int x=80;
    private int y=70;
    private Button keysArray[];
    private Panel keypad;
    public void init()
    {
        cup = getImage(getDocumentBase(), "cup.jpg");
        Canvas mine = new Canvas();
        Panel keypad = new Panel();
        keysArray = new Button[6];
        keysArray[1] = new Button("Up");
        keysArray[2] = new Button("Left");
        keysArray[3] = new Button("Center");
        keysArray[4] = new Button("Right");
        keysArray[5] = new Button("Down");
        setLayout(new BorderLayout());
        keypad.setLayout(new BorderLayout());
        keypad.add(keysArray[1], BorderLayout.NORTH);
        keypad.add(keysArray[2], BorderLayout.WEST);
        keypad.add(keysArray[3], BorderLayout.CENTER);
        keypad.add(keysArray[4], BorderLayout.EAST);
        keypad.add(keysArray[5], BorderLayout.SOUTH);
        keysArray[1].addActionListener(this);
        keysArray[2].addActionListener(this);
        keysArray[3].addActionListener(this);
        keysArray[4].addActionListener(this);
        keysArray[5].addActionListener(this);
        keysArray[1].setActionCommand("Up");
        keysArray[2].setActionCommand("Left");
        keysArray[3].setActionCommand("Center");
        keysArray[4].setActionCommand("Right");
        keysArray[5].setActionCommand("Down");
        add(keypad, BorderLayout.SOUTH);
        setBackground(Color.blue);
    }
    public void paint(Graphics g)
    {
        g.drawImage(cup, y, x, this);
    }
    public void actionPerformed(ActionEvent e)
    {
        String arg = e.getActionCommand();
        if (arg == "Up")
            x=x-10;
        if (arg == "Down")
            x=x+10;
        if (arg == "Left")
            y=y-10;
        if (arg == "Right")
            y=y+10;
        if (arg == "Center")
        {
            y=70;
            x=80;
        }
        repaint();
    }
}